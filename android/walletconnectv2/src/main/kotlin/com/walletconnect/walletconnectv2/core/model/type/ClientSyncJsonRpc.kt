package com.walletconnect.walletconnectv2.core.model.type

internal interface ClientSyncJsonRpc {
    val id: Long
}