package com.walletconnect.walletconnectv2.core.exceptions.peer

internal const val NO_SEQUENCE_CODE: Long = 1302
internal const val UNAUTHORIZED_PEER_CODE: Long = 3005