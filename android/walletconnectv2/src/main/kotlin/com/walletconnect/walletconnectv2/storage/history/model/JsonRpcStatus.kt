package com.walletconnect.walletconnectv2.storage.history.model

enum class JsonRpcStatus {
    PENDING, REQUEST_SUCCESS, RESPOND_SUCCESS, REQUEST_FAILURE, RESPOND_FAILURE
}