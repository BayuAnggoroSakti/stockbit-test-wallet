package com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal

internal data class PairingProposedPermissionsVO(val jsonRPC: JsonRPCVO)