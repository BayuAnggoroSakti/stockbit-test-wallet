package com.walletconnect.walletconnectv2.core.exceptions.peer

internal const val PEER_IS_ALSO_NON_CONTROLLER_MESSAGE: String = "Unauthorized: peer is also non-controller"
internal const val PEER_IS_ALSO_CONTROLLER_MESSAGE: String = "Unauthorized: peer is also controller"