package com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal

internal data class PairingProposerVO(val publicKey: String, val controller: Boolean)