package com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal

internal data class JsonRPCVO(val methods: List<String>)