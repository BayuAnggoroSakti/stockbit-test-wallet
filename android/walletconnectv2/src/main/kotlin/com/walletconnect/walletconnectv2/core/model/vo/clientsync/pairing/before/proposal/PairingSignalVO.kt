package com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal

internal data class PairingSignalVO(val type: String, val params: PairingSignalParamsVO)