-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request$Params
-keepnames class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request$Params
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request$Params
-keep class com.walletconnect.walletconnectv2.network.model.RelayDTO_Subscribe_Request_ParamsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
    private com.squareup.moshi.JsonAdapter topicVOAtQualifierAdapter;
}
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request$Params
-keep @interface com.walletconnect.walletconnectv2.core.adapters.TopicAdapter$Qualifier
