-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionDelete
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionDelete
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionDelete
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO_SessionDeleteJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionDelete
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionDelete
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionDelete {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$DeleteParams,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
