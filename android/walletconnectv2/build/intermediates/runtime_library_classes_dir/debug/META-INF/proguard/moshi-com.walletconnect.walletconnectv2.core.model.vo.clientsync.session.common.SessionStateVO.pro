-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
