-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params
-keepnames class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params
-keep class com.walletconnect.walletconnectv2.network.model.RelayDTO_Subscription_Request_ParamsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
    private com.squareup.moshi.JsonAdapter subscriptionIdVOAtQualifierAdapter;
}
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params
-keep @interface com.walletconnect.walletconnectv2.core.adapters.SubscriptionIdAdapter$Qualifier
