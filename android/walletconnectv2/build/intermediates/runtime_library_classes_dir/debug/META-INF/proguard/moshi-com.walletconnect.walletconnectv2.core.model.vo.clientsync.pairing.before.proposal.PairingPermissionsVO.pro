-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingPermissionsVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingPermissionsVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingPermissionsVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingPermissionsVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
