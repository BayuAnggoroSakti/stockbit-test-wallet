-if class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcError
-keepnames class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcError
-if class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcError
-keep class com.walletconnect.walletconnectv2.engine.model.EngineDO_JsonRpcResponse_JsonRpcErrorJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcError
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcError
-keepclassmembers class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcError {
    public synthetic <init>(long,java.lang.String,com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$Error,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
