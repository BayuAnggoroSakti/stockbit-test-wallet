-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO {
    public synthetic <init>(int,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
