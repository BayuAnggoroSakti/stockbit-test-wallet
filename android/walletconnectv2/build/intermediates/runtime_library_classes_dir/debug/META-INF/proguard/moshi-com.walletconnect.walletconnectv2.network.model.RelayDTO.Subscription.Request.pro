-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request
-keepnames class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request
-keep class com.walletconnect.walletconnectv2.network.model.RelayDTO_Subscription_RequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request
-keepclassmembers class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
