-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
