-if class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcError
-keepnames class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcError
-if class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcError
-keep class com.walletconnect.walletconnectv2.relay.model.RelayDO_JsonRpcResponse_JsonRpcErrorJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcError
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcError
-keepclassmembers class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcError {
    public synthetic <init>(long,java.lang.String,com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$Error,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
