-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params$SubscriptionData
-keepnames class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params$SubscriptionData
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params$SubscriptionData
-keep class com.walletconnect.walletconnectv2.network.model.RelayDTO_Subscription_Request_Params_SubscriptionDataJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
    private com.squareup.moshi.JsonAdapter topicVOAtQualifierAdapter;
}
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscription$Request$Params$SubscriptionData
-keep @interface com.walletconnect.walletconnectv2.core.adapters.TopicAdapter$Qualifier
