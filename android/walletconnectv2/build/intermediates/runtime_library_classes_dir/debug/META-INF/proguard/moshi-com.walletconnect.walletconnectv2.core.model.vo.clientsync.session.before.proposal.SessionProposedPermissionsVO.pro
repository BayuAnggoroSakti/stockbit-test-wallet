-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
