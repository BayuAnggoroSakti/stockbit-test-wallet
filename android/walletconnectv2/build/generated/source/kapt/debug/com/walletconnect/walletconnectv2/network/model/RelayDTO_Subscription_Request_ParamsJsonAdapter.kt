// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package com.walletconnect.walletconnectv2.network.model

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.`internal`.Util
import com.walletconnect.walletconnectv2.core.adapters.SubscriptionIdAdapter
import com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.emptySet
import kotlin.text.buildString

internal class RelayDTO_Subscription_Request_ParamsJsonAdapter(
  moshi: Moshi
) : JsonAdapter<RelayDTO.Subscription.Request.Params>() {
  private val options: JsonReader.Options = JsonReader.Options.of("id", "data")

  @field:SubscriptionIdAdapter.Qualifier
  private val subscriptionIdVOAtQualifierAdapter: JsonAdapter<SubscriptionIdVO> =
      moshi.adapter(SubscriptionIdVO::class.java, Types.getFieldJsonQualifierAnnotations(javaClass,
      "subscriptionIdVOAtQualifierAdapter"), "subscriptionId")

  private val subscriptionDataAdapter:
      JsonAdapter<RelayDTO.Subscription.Request.Params.SubscriptionData> =
      moshi.adapter(RelayDTO.Subscription.Request.Params.SubscriptionData::class.java, emptySet(),
      "subscriptionData")

  public override fun toString(): String = buildString(58) {
      append("GeneratedJsonAdapter(").append("RelayDTO.Subscription.Request.Params").append(')') }

  public override fun fromJson(reader: JsonReader): RelayDTO.Subscription.Request.Params {
    var subscriptionId: SubscriptionIdVO? = null
    var subscriptionData: RelayDTO.Subscription.Request.Params.SubscriptionData? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> subscriptionId = subscriptionIdVOAtQualifierAdapter.fromJson(reader) ?:
            throw Util.unexpectedNull("subscriptionId", "id", reader)
        1 -> subscriptionData = subscriptionDataAdapter.fromJson(reader) ?:
            throw Util.unexpectedNull("subscriptionData", "data", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return RelayDTO.Subscription.Request.Params(
        subscriptionId = subscriptionId ?: throw Util.missingProperty("subscriptionId", "id",
            reader),
        subscriptionData = subscriptionData ?: throw Util.missingProperty("subscriptionData",
            "data", reader)
    )
  }

  public override fun toJson(writer: JsonWriter, value_: RelayDTO.Subscription.Request.Params?):
      Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("id")
    subscriptionIdVOAtQualifierAdapter.toJson(writer, value_.subscriptionId)
    writer.name("data")
    subscriptionDataAdapter.toJson(writer, value_.subscriptionData)
    writer.endObject()
  }
}
