// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package com.walletconnect.walletconnectv2.network.model

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.`internal`.Util
import com.walletconnect.walletconnectv2.core.adapters.TopicAdapter
import com.walletconnect.walletconnectv2.core.model.vo.TopicVO
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.emptySet
import kotlin.text.buildString

internal class RelayDTO_Subscription_Request_Params_SubscriptionDataJsonAdapter(
  moshi: Moshi
) : JsonAdapter<RelayDTO.Subscription.Request.Params.SubscriptionData>() {
  private val options: JsonReader.Options = JsonReader.Options.of("topic", "message")

  @field:TopicAdapter.Qualifier
  private val topicVOAtQualifierAdapter: JsonAdapter<TopicVO> = moshi.adapter(TopicVO::class.java,
      Types.getFieldJsonQualifierAnnotations(javaClass, "topicVOAtQualifierAdapter"), "topic")

  private val stringAdapter: JsonAdapter<String> = moshi.adapter(String::class.java, emptySet(),
      "message")

  public override fun toString(): String = buildString(75) {
      append("GeneratedJsonAdapter(").append("RelayDTO.Subscription.Request.Params.SubscriptionData").append(')')
      }

  public override fun fromJson(reader: JsonReader):
      RelayDTO.Subscription.Request.Params.SubscriptionData {
    var topic: TopicVO? = null
    var message: String? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> topic = topicVOAtQualifierAdapter.fromJson(reader) ?:
            throw Util.unexpectedNull("topic", "topic", reader)
        1 -> message = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("message",
            "message", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return RelayDTO.Subscription.Request.Params.SubscriptionData(
        topic = topic ?: throw Util.missingProperty("topic", "topic", reader),
        message = message ?: throw Util.missingProperty("message", "message", reader)
    )
  }

  public override fun toJson(writer: JsonWriter,
      value_: RelayDTO.Subscription.Request.Params.SubscriptionData?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("topic")
    topicVOAtQualifierAdapter.toJson(writer, value_.topic)
    writer.name("message")
    stringAdapter.toJson(writer, value_.message)
    writer.endObject()
  }
}
