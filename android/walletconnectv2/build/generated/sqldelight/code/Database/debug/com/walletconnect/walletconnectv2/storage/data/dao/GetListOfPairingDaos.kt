package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus
import kotlin.Long
import kotlin.String
import kotlin.collections.List

public data class GetListOfPairingDaos(
  public val topic: String,
  public val expiry: Long,
  public val uri: String,
  public val status: SequenceStatus,
  public val controller_type: ControllerType,
  public val self_participant: String,
  public val peer_participant: String?,
  public val controller_key: String?,
  public val relay_protocol: String,
  public val permissions: List<String>?,
  public val _name: String?,
  public val description: String?,
  public val url: String?,
  public val icons: List<String>?
) {
  public override fun toString(): String = """
  |GetListOfPairingDaos [
  |  topic: $topic
  |  expiry: $expiry
  |  uri: $uri
  |  status: $status
  |  controller_type: $controller_type
  |  self_participant: $self_participant
  |  peer_participant: $peer_participant
  |  controller_key: $controller_key
  |  relay_protocol: $relay_protocol
  |  permissions: $permissions
  |  _name: $_name
  |  description: $description
  |  url: $url
  |  icons: $icons
  |]
  """.trimMargin()
}
