package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.squareup.sqldelight.Query
import com.squareup.sqldelight.Transacter
import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus
import kotlin.Any
import kotlin.Long
import kotlin.String
import kotlin.Unit
import kotlin.collections.List

public interface PairingDaoQueries : Transacter {
  public fun <T : Any> getListOfPairingDaos(mapper: (
    topic: String,
    expiry: Long,
    uri: String,
    status: SequenceStatus,
    controller_type: ControllerType,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    relay_protocol: String,
    permissions: List<String>?,
    _name: String?,
    description: String?,
    url: String?,
    icons: List<String>?
  ) -> T): Query<T>

  public fun getListOfPairingDaos(): Query<GetListOfPairingDaos>

  public fun <T : Any> getPairingByTopic(topic: String, mapper: (
    topic: String,
    expiry: Long,
    uri: String,
    status: SequenceStatus,
    controller_type: ControllerType,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    relay_protocol: String,
    permissions: List<String>?
  ) -> T): Query<T>

  public fun getPairingByTopic(topic: String): Query<GetPairingByTopic>

  public fun hasTopic(topic: String): Query<String>

  public fun getExpiry(topic: String): Query<Long>

  public fun insertPairing(
    topic: String,
    uri: String,
    expiry: Long,
    status: SequenceStatus,
    controller_type: ControllerType,
    self_participant: String,
    relay_protocol: String
  ): Unit

  public fun updatePendingPairingToPreSettled(
    topic: String,
    expiry: Long,
    status: SequenceStatus,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    permissions: List<String>?,
    topic_: String
  ): Unit

  public fun updatePreSettledPairingToAcknowledged(status: SequenceStatus, topic: String): Unit

  public fun updateProposedPairingToAcknowledged(
    topic: String,
    expiry: Long,
    status: SequenceStatus,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    permissions: List<String>?,
    relay_protocol: String,
    metadata_id: Long?,
    topic_: String
  ): Unit

  public fun deletePairing(topic: String): Unit

  public fun updateAcknowledgedPairingMetadata(metadata_id: Long?, topic: String): Unit
}
