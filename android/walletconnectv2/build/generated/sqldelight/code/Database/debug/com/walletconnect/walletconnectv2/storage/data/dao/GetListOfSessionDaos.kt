package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus
import kotlin.Long
import kotlin.String
import kotlin.collections.List

public data class GetListOfSessionDaos(
  public val topic: String,
  public val permissions_chains: List<String>,
  public val permissions_methods: List<String>,
  public val permissions_types: List<String>?,
  public val ttl_seconds: Long,
  public val accounts: List<String>?,
  public val expiry: Long,
  public val status: SequenceStatus,
  public val controller_type: ControllerType,
  public val _name: String?,
  public val description: String?,
  public val url: String?,
  public val icons: List<String>?,
  public val self_participant: String,
  public val peer_participant: String?,
  public val controller_key: String?,
  public val relay_protocol: String
) {
  public override fun toString(): String = """
  |GetListOfSessionDaos [
  |  topic: $topic
  |  permissions_chains: $permissions_chains
  |  permissions_methods: $permissions_methods
  |  permissions_types: $permissions_types
  |  ttl_seconds: $ttl_seconds
  |  accounts: $accounts
  |  expiry: $expiry
  |  status: $status
  |  controller_type: $controller_type
  |  _name: $_name
  |  description: $description
  |  url: $url
  |  icons: $icons
  |  self_participant: $self_participant
  |  peer_participant: $peer_participant
  |  controller_key: $controller_key
  |  relay_protocol: $relay_protocol
  |]
  """.trimMargin()
}
