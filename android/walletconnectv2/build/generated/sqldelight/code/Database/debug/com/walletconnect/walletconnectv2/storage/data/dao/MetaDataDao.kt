package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.squareup.sqldelight.ColumnAdapter
import kotlin.Long
import kotlin.String
import kotlin.collections.List

public data class MetaDataDao(
  public val id: Long,
  public val _name: String,
  public val description: String,
  public val url: String,
  public val icons: List<String>
) {
  public override fun toString(): String = """
  |MetaDataDao [
  |  id: $id
  |  _name: $_name
  |  description: $description
  |  url: $url
  |  icons: $icons
  |]
  """.trimMargin()

  public class Adapter(
    public val iconsAdapter: ColumnAdapter<List<String>, String>
  )
}
