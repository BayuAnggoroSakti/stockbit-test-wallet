package com.walletconnect.walletconnectv2.walletconnectv2

import com.squareup.sqldelight.Query
import com.squareup.sqldelight.TransacterImpl
import com.squareup.sqldelight.`internal`.copyOnWriteList
import com.squareup.sqldelight.db.SqlCursor
import com.squareup.sqldelight.db.SqlDriver
import com.walletconnect.walletconnectv2.Database
import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.`data`.dao.GetJsonRpcRequestsDaos
import com.walletconnect.walletconnectv2.storage.`data`.dao.GetJsonRpcRespondsDaos
import com.walletconnect.walletconnectv2.storage.`data`.dao.GetListOfPairingDaos
import com.walletconnect.walletconnectv2.storage.`data`.dao.GetListOfSessionDaos
import com.walletconnect.walletconnectv2.storage.`data`.dao.GetPairingByTopic
import com.walletconnect.walletconnectv2.storage.`data`.dao.GetPermissionsByTopic
import com.walletconnect.walletconnectv2.storage.`data`.dao.GetSessionByTopic
import com.walletconnect.walletconnectv2.storage.`data`.dao.JsonRpcHistoryDao
import com.walletconnect.walletconnectv2.storage.`data`.dao.JsonRpcHistoryQueries
import com.walletconnect.walletconnectv2.storage.`data`.dao.MetaDataDao
import com.walletconnect.walletconnectv2.storage.`data`.dao.MetaDataDaoQueries
import com.walletconnect.walletconnectv2.storage.`data`.dao.PairingDao
import com.walletconnect.walletconnectv2.storage.`data`.dao.PairingDaoQueries
import com.walletconnect.walletconnectv2.storage.`data`.dao.SessionDao
import com.walletconnect.walletconnectv2.storage.`data`.dao.SessionDaoQueries
import com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus
import com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus
import kotlin.Any
import kotlin.Boolean
import kotlin.Int
import kotlin.Long
import kotlin.String
import kotlin.Unit
import kotlin.collections.Collection
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.reflect.KClass

internal val KClass<Database>.schema: SqlDriver.Schema
  get() = DatabaseImpl.Schema

internal fun KClass<Database>.newInstance(
  driver: SqlDriver,
  JsonRpcHistoryDaoAdapter: JsonRpcHistoryDao.Adapter,
  MetaDataDaoAdapter: MetaDataDao.Adapter,
  PairingDaoAdapter: PairingDao.Adapter,
  SessionDaoAdapter: SessionDao.Adapter
): Database = DatabaseImpl(driver, JsonRpcHistoryDaoAdapter, MetaDataDaoAdapter, PairingDaoAdapter,
    SessionDaoAdapter)

private class DatabaseImpl(
  driver: SqlDriver,
  internal val JsonRpcHistoryDaoAdapter: JsonRpcHistoryDao.Adapter,
  internal val MetaDataDaoAdapter: MetaDataDao.Adapter,
  internal val PairingDaoAdapter: PairingDao.Adapter,
  internal val SessionDaoAdapter: SessionDao.Adapter
) : TransacterImpl(driver), Database {
  public override val jsonRpcHistoryQueries: JsonRpcHistoryQueriesImpl =
      JsonRpcHistoryQueriesImpl(this, driver)

  public override val metaDataDaoQueries: MetaDataDaoQueriesImpl = MetaDataDaoQueriesImpl(this,
      driver)

  public override val pairingDaoQueries: PairingDaoQueriesImpl = PairingDaoQueriesImpl(this, driver)

  public override val sessionDaoQueries: SessionDaoQueriesImpl = SessionDaoQueriesImpl(this, driver)

  public object Schema : SqlDriver.Schema {
    public override val version: Int
      get() = 1

    public override fun create(driver: SqlDriver): Unit {
      driver.execute(null, """
          |CREATE TABLE JsonRpcHistoryDao(
          |  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          |  request_id INTEGER UNIQUE NOT NULL,
          |  topic TEXT NOT NULL,
          |  method TEXT,
          |  body TEXT,
          |  status TEXT NOT NULL,
          |  controller_type TEXT NOT NULL
          |)
          """.trimMargin(), 0)
      driver.execute(null, """
          |CREATE TABLE MetaDataDao(
          |	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          |  	_name TEXT NOT NULL,
          |  	description TEXT NOT NULL,
          |  	url TEXT NOT NULL,
          |  	icons TEXT NOT NULL
          |)
          """.trimMargin(), 0)
      driver.execute(null, """
          |CREATE TABLE PairingDao (
          |    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          |    topic TEXT UNIQUE NOT NULL,
          |    uri TEXT NOT NULL,
          |    expiry INTEGER NOT NULL,
          |    status TEXT NOT NULL,
          |    controller_type TEXT NOT NULL,
          |    self_participant TEXT NOT NULL,
          |    peer_participant TEXT,
          |    controller_key TEXT,
          |    relay_protocol TEXT NOT NULL,
          |    permissions TEXT,
          |    metadata_id INTEGER
          |)
          """.trimMargin(), 0)
      driver.execute(null, """
          |CREATE TABLE SessionDao(
          |  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          |  topic TEXT UNIQUE NOT NULL,
          |  permissions_chains TEXT NOT NULL,
          |  permissions_methods TEXT NOT NULL,
          |  permissions_types TEXT,
          |  self_participant TEXT NOT NULL,
          |  peer_participant TEXT,
          |  controller_key TEXT,
          |  ttl_seconds INTEGER NOT NULL,
          |  accounts TEXT DEFAULT(NULL),
          |  expiry INTEGER NOT NULL,
          |  status TEXT NOT NULL,
          |  controller_type TEXT NOT NULL,
          |  metadata_id INTEGER,
          |  relay_protocol TEXT NOT NULL
          |)
          """.trimMargin(), 0)
    }

    public override fun migrate(
      driver: SqlDriver,
      oldVersion: Int,
      newVersion: Int
    ): Unit {
    }
  }
}

private class JsonRpcHistoryQueriesImpl(
  private val database: DatabaseImpl,
  private val driver: SqlDriver
) : TransacterImpl(driver), JsonRpcHistoryQueries {
  internal val doesJsonRpcNotExist: MutableList<Query<*>> = copyOnWriteList()

  internal val selectLastInsertedRowId: MutableList<Query<*>> = copyOnWriteList()

  internal val getJsonRpcRequestsDaos: MutableList<Query<*>> = copyOnWriteList()

  internal val getJsonRpcRespondsDaos: MutableList<Query<*>> = copyOnWriteList()

  public override fun doesJsonRpcNotExist(request_id: Long): Query<Boolean> =
      DoesJsonRpcNotExistQuery(request_id) { cursor ->
    cursor.getLong(0)!! == 1L
  }

  public override fun selectLastInsertedRowId(): Query<Long> = Query(-218067816,
      selectLastInsertedRowId, driver, "JsonRpcHistory.sq", "selectLastInsertedRowId",
      "SELECT last_insert_rowid()") { cursor ->
    cursor.getLong(0)!!
  }

  public override fun <T : Any> getJsonRpcRequestsDaos(
    topic: String,
    method: Collection<String?>,
    mapper: (
      request_id: Long,
      topic: String,
      method: String?,
      body: String?,
      status: JsonRpcStatus,
      controller_type: ControllerType
    ) -> T
  ): Query<T> = GetJsonRpcRequestsDaosQuery(topic, method) { cursor ->
    mapper(
      cursor.getLong(0)!!,
      cursor.getString(1)!!,
      cursor.getString(2),
      cursor.getString(3),
      database.JsonRpcHistoryDaoAdapter.statusAdapter.decode(cursor.getString(4)!!),
      database.JsonRpcHistoryDaoAdapter.controller_typeAdapter.decode(cursor.getString(5)!!)
    )
  }

  public override fun getJsonRpcRequestsDaos(topic: String, method: Collection<String?>):
      Query<GetJsonRpcRequestsDaos> = getJsonRpcRequestsDaos(topic, method) { request_id, topic_,
      method_, body, status, controller_type ->
    GetJsonRpcRequestsDaos(
      request_id,
      topic_,
      method_,
      body,
      status,
      controller_type
    )
  }

  public override fun <T : Any> getJsonRpcRespondsDaos(
    topic: String,
    method: Collection<String?>,
    mapper: (
      request_id: Long,
      topic: String,
      method: String?,
      body: String?,
      status: JsonRpcStatus,
      controller_type: ControllerType
    ) -> T
  ): Query<T> = GetJsonRpcRespondsDaosQuery(topic, method) { cursor ->
    mapper(
      cursor.getLong(0)!!,
      cursor.getString(1)!!,
      cursor.getString(2),
      cursor.getString(3),
      database.JsonRpcHistoryDaoAdapter.statusAdapter.decode(cursor.getString(4)!!),
      database.JsonRpcHistoryDaoAdapter.controller_typeAdapter.decode(cursor.getString(5)!!)
    )
  }

  public override fun getJsonRpcRespondsDaos(topic: String, method: Collection<String?>):
      Query<GetJsonRpcRespondsDaos> = getJsonRpcRespondsDaos(topic, method) { request_id, topic_,
      method_, body, status, controller_type ->
    GetJsonRpcRespondsDaos(
      request_id,
      topic_,
      method_,
      body,
      status,
      controller_type
    )
  }

  public override fun insertJsonRpcHistory(
    request_id: Long,
    topic: String,
    method: String?,
    body: String?,
    status: JsonRpcStatus,
    controller_type: ControllerType
  ): Unit {
    driver.execute(-79043229, """
    |INSERT OR IGNORE INTO JsonRpcHistoryDao (request_id, topic, method, body, status, controller_type)
    |VALUES (?, ?, ?, ?, ?, ?)
    """.trimMargin(), 6) {
      bindLong(1, request_id)
      bindString(2, topic)
      bindString(3, method)
      bindString(4, body)
      bindString(5, database.JsonRpcHistoryDaoAdapter.statusAdapter.encode(status))
      bindString(6,
          database.JsonRpcHistoryDaoAdapter.controller_typeAdapter.encode(controller_type))
    }
    notifyQueries(-79043229, {database.jsonRpcHistoryQueries.doesJsonRpcNotExist +
        database.jsonRpcHistoryQueries.getJsonRpcRespondsDaos +
        database.jsonRpcHistoryQueries.getJsonRpcRequestsDaos})
  }

  public override fun updateJsonRpcHistory(status: JsonRpcStatus, request_id: Long): Unit {
    driver.execute(709459827, """
    |UPDATE JsonRpcHistoryDao
    |SET status = ?
    |WHERE request_id = ?
    """.trimMargin(), 2) {
      bindString(1, database.JsonRpcHistoryDaoAdapter.statusAdapter.encode(status))
      bindLong(2, request_id)
    }
    notifyQueries(709459827, {database.jsonRpcHistoryQueries.doesJsonRpcNotExist +
        database.jsonRpcHistoryQueries.getJsonRpcRespondsDaos +
        database.jsonRpcHistoryQueries.getJsonRpcRequestsDaos})
  }

  public override fun deleteJsonRpcHistory(topic: String): Unit {
    driver.execute(1500420309, """
    |DELETE FROM JsonRpcHistoryDao
    |WHERE topic = ?
    """.trimMargin(), 1) {
      bindString(1, topic)
    }
    notifyQueries(1500420309, {database.jsonRpcHistoryQueries.doesJsonRpcNotExist +
        database.jsonRpcHistoryQueries.getJsonRpcRespondsDaos +
        database.jsonRpcHistoryQueries.getJsonRpcRequestsDaos})
  }

  private inner class DoesJsonRpcNotExistQuery<out T : Any>(
    public val request_id: Long,
    mapper: (SqlCursor) -> T
  ) : Query<T>(doesJsonRpcNotExist, mapper) {
    public override fun execute(): SqlCursor = driver.executeQuery(-851116939, """
    |SELECT NOT EXISTS (
    |    SELECT 1
    |    FROM JsonRpcHistoryDao
    |    WHERE request_id = ?
    |    LIMIT 1
    |)
    """.trimMargin(), 1) {
      bindLong(1, request_id)
    }

    public override fun toString(): String = "JsonRpcHistory.sq:doesJsonRpcNotExist"
  }

  private inner class GetJsonRpcRequestsDaosQuery<out T : Any>(
    public val topic: String,
    public val method: Collection<String?>,
    mapper: (SqlCursor) -> T
  ) : Query<T>(getJsonRpcRequestsDaos, mapper) {
    public override fun execute(): SqlCursor {
      val methodIndexes = createArguments(count = method.size)
      return driver.executeQuery(null, """
      |SELECT request_id, topic, method, body, status, controller_type
      |FROM JsonRpcHistoryDao
      |WHERE topic = ? AND method IN $methodIndexes
      """.trimMargin(), 1 + method.size) {
        bindString(1, topic)
        method.forEachIndexed { index, method_ ->
            bindString(index + 2, method_)
            }
      }
    }

    public override fun toString(): String = "JsonRpcHistory.sq:getJsonRpcRequestsDaos"
  }

  private inner class GetJsonRpcRespondsDaosQuery<out T : Any>(
    public val topic: String,
    public val method: Collection<String?>,
    mapper: (SqlCursor) -> T
  ) : Query<T>(getJsonRpcRespondsDaos, mapper) {
    public override fun execute(): SqlCursor {
      val methodIndexes = createArguments(count = method.size)
      return driver.executeQuery(null, """
      |SELECT request_id, topic, method, body, status, controller_type
      |FROM JsonRpcHistoryDao
      |WHERE topic = ? AND method NOT IN $methodIndexes
      """.trimMargin(), 1 + method.size) {
        bindString(1, topic)
        method.forEachIndexed { index, method_ ->
            bindString(index + 2, method_)
            }
      }
    }

    public override fun toString(): String = "JsonRpcHistory.sq:getJsonRpcRespondsDaos"
  }
}

private class MetaDataDaoQueriesImpl(
  private val database: DatabaseImpl,
  private val driver: SqlDriver
) : TransacterImpl(driver), MetaDataDaoQueries {
  internal val lastInsertedRowId: MutableList<Query<*>> = copyOnWriteList()

  internal val getMetaData: MutableList<Query<*>> = copyOnWriteList()

  public override fun lastInsertedRowId(): Query<Long> = Query(423205572, lastInsertedRowId, driver,
      "MetaDataDao.sq", "lastInsertedRowId", "SELECT last_insert_rowid()") { cursor ->
    cursor.getLong(0)!!
  }

  public override fun <T : Any> getMetaData(mapper: (
    id: Long,
    _name: String,
    description: String,
    url: String,
    icons: List<String>
  ) -> T): Query<T> = Query(-2123390494, getMetaData, driver, "MetaDataDao.sq", "getMetaData", """
  |SELECT id, _name, description, url, icons
  |FROM MetaDataDao
  |LIMIT 1
  """.trimMargin()) { cursor ->
    mapper(
      cursor.getLong(0)!!,
      cursor.getString(1)!!,
      cursor.getString(2)!!,
      cursor.getString(3)!!,
      database.MetaDataDaoAdapter.iconsAdapter.decode(cursor.getString(4)!!)
    )
  }

  public override fun getMetaData(): Query<MetaDataDao> = getMetaData { id, _name, description, url,
      icons ->
    MetaDataDao(
      id,
      _name,
      description,
      url,
      icons
    )
  }

  public override fun insertOrIgnoreMetaData(
    _name: String,
    description: String,
    url: String,
    icons: List<String>
  ): Unit {
    driver.execute(348605344, """
    |INSERT OR IGNORE INTO MetaDataDao(_name, description, url, icons)
    |VALUES (?, ?, ?, ?)
    """.trimMargin(), 4) {
      bindString(1, _name)
      bindString(2, description)
      bindString(3, url)
      bindString(4, database.MetaDataDaoAdapter.iconsAdapter.encode(icons))
    }
    notifyQueries(348605344, {database.metaDataDaoQueries.getMetaData +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic +
        database.pairingDaoQueries.getListOfPairingDaos})
  }

  public override fun deleteMetaDataFromTopic(topic: String): Unit {
    driver.execute(-589492152, """
    |DELETE FROM MetaDataDao
    |WHERE id = (
    |   SELECT metadata_id
    |   FROM SessionDao
    |   WHERE topic = ?
    |)
    """.trimMargin(), 1) {
      bindString(1, topic)
    }
    notifyQueries(-589492152, {database.metaDataDaoQueries.getMetaData +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic +
        database.pairingDaoQueries.getListOfPairingDaos})
  }
}

private class PairingDaoQueriesImpl(
  private val database: DatabaseImpl,
  private val driver: SqlDriver
) : TransacterImpl(driver), PairingDaoQueries {
  internal val getListOfPairingDaos: MutableList<Query<*>> = copyOnWriteList()

  internal val getPairingByTopic: MutableList<Query<*>> = copyOnWriteList()

  internal val hasTopic: MutableList<Query<*>> = copyOnWriteList()

  internal val getExpiry: MutableList<Query<*>> = copyOnWriteList()

  public override fun <T : Any> getListOfPairingDaos(mapper: (
    topic: String,
    expiry: Long,
    uri: String,
    status: SequenceStatus,
    controller_type: ControllerType,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    relay_protocol: String,
    permissions: List<String>?,
    _name: String?,
    description: String?,
    url: String?,
    icons: List<String>?
  ) -> T): Query<T> = Query(885037828, getListOfPairingDaos, driver, "PairingDao.sq",
      "getListOfPairingDaos", """
  |SELECT pd.topic, pd.expiry, pd.uri, pd.status, pd.controller_type, pd.self_participant, pd.peer_participant, pd.controller_key, pd.relay_protocol, pd.permissions, mdd._name, mdd.description, mdd.url, mdd.icons
  |FROM PairingDao pd
  |    LEFT JOIN MetaDataDao mdd ON pd.metadata_id = mdd.id
  """.trimMargin()) { cursor ->
    mapper(
      cursor.getString(0)!!,
      cursor.getLong(1)!!,
      cursor.getString(2)!!,
      database.PairingDaoAdapter.statusAdapter.decode(cursor.getString(3)!!),
      database.PairingDaoAdapter.controller_typeAdapter.decode(cursor.getString(4)!!),
      cursor.getString(5)!!,
      cursor.getString(6),
      cursor.getString(7),
      cursor.getString(8)!!,
      cursor.getString(9)?.let { database.PairingDaoAdapter.permissionsAdapter.decode(it) },
      cursor.getString(10),
      cursor.getString(11),
      cursor.getString(12),
      cursor.getString(13)?.let { database.MetaDataDaoAdapter.iconsAdapter.decode(it) }
    )
  }

  public override fun getListOfPairingDaos(): Query<GetListOfPairingDaos> = getListOfPairingDaos {
      topic, expiry, uri, status, controller_type, self_participant, peer_participant,
      controller_key, relay_protocol, permissions, _name, description, url, icons ->
    GetListOfPairingDaos(
      topic,
      expiry,
      uri,
      status,
      controller_type,
      self_participant,
      peer_participant,
      controller_key,
      relay_protocol,
      permissions,
      _name,
      description,
      url,
      icons
    )
  }

  public override fun <T : Any> getPairingByTopic(topic: String, mapper: (
    topic: String,
    expiry: Long,
    uri: String,
    status: SequenceStatus,
    controller_type: ControllerType,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    relay_protocol: String,
    permissions: List<String>?
  ) -> T): Query<T> = GetPairingByTopicQuery(topic) { cursor ->
    mapper(
      cursor.getString(0)!!,
      cursor.getLong(1)!!,
      cursor.getString(2)!!,
      database.PairingDaoAdapter.statusAdapter.decode(cursor.getString(3)!!),
      database.PairingDaoAdapter.controller_typeAdapter.decode(cursor.getString(4)!!),
      cursor.getString(5)!!,
      cursor.getString(6),
      cursor.getString(7),
      cursor.getString(8)!!,
      cursor.getString(9)?.let { database.PairingDaoAdapter.permissionsAdapter.decode(it) }
    )
  }

  public override fun getPairingByTopic(topic: String): Query<GetPairingByTopic> =
      getPairingByTopic(topic) { topic_, expiry, uri, status, controller_type, self_participant,
      peer_participant, controller_key, relay_protocol, permissions ->
    GetPairingByTopic(
      topic_,
      expiry,
      uri,
      status,
      controller_type,
      self_participant,
      peer_participant,
      controller_key,
      relay_protocol,
      permissions
    )
  }

  public override fun hasTopic(topic: String): Query<String> = HasTopicQuery(topic) { cursor ->
    cursor.getString(0)!!
  }

  public override fun getExpiry(topic: String): Query<Long> = GetExpiryQuery(topic) { cursor ->
    cursor.getLong(0)!!
  }

  public override fun insertPairing(
    topic: String,
    uri: String,
    expiry: Long,
    status: SequenceStatus,
    controller_type: ControllerType,
    self_participant: String,
    relay_protocol: String
  ): Unit {
    driver.execute(418608265, """
    |INSERT OR IGNORE INTO PairingDao(topic, uri, expiry, status, controller_type, self_participant, relay_protocol)
    |VALUES (?, ?, ?, ?, ?,    ?, ?)
    """.trimMargin(), 7) {
      bindString(1, topic)
      bindString(2, uri)
      bindLong(3, expiry)
      bindString(4, database.PairingDaoAdapter.statusAdapter.encode(status))
      bindString(5, database.PairingDaoAdapter.controller_typeAdapter.encode(controller_type))
      bindString(6, self_participant)
      bindString(7, relay_protocol)
    }
    notifyQueries(418608265, {database.pairingDaoQueries.getPairingByTopic +
        database.pairingDaoQueries.hasTopic + database.pairingDaoQueries.getListOfPairingDaos +
        database.pairingDaoQueries.getExpiry})
  }

  public override fun updatePendingPairingToPreSettled(
    topic: String,
    expiry: Long,
    status: SequenceStatus,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    permissions: List<String>?,
    topic_: String
  ): Unit {
    driver.execute(1880047537, """
    |UPDATE PairingDao
    |SET topic = ?, expiry = ?, status = ?, self_participant = ?, peer_participant = ?, controller_key = ?, permissions = ?
    |WHERE topic = ?
    """.trimMargin(), 8) {
      bindString(1, topic)
      bindLong(2, expiry)
      bindString(3, database.PairingDaoAdapter.statusAdapter.encode(status))
      bindString(4, self_participant)
      bindString(5, peer_participant)
      bindString(6, controller_key)
      bindString(7, permissions?.let { database.PairingDaoAdapter.permissionsAdapter.encode(it) })
      bindString(8, topic_)
    }
    notifyQueries(1880047537, {database.pairingDaoQueries.getPairingByTopic +
        database.pairingDaoQueries.hasTopic + database.pairingDaoQueries.getListOfPairingDaos +
        database.pairingDaoQueries.getExpiry})
  }

  public override fun updatePreSettledPairingToAcknowledged(status: SequenceStatus, topic: String):
      Unit {
    driver.execute(-404538298, """
    |UPDATE PairingDao
    |SET status = ?
    |WHERE topic = ?
    """.trimMargin(), 2) {
      bindString(1, database.PairingDaoAdapter.statusAdapter.encode(status))
      bindString(2, topic)
    }
    notifyQueries(-404538298, {database.pairingDaoQueries.getPairingByTopic +
        database.pairingDaoQueries.hasTopic + database.pairingDaoQueries.getListOfPairingDaos +
        database.pairingDaoQueries.getExpiry})
  }

  public override fun updateProposedPairingToAcknowledged(
    topic: String,
    expiry: Long,
    status: SequenceStatus,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    permissions: List<String>?,
    relay_protocol: String,
    metadata_id: Long?,
    topic_: String
  ): Unit {
    driver.execute(-83516554, """
    |UPDATE PairingDao
    |SET topic = ?, expiry = ?, status = ?, self_participant = ?, peer_participant = ?, controller_key = ?, permissions = ?, relay_protocol = ?, metadata_id = ?
    |WHERE topic = ?
    """.trimMargin(), 10) {
      bindString(1, topic)
      bindLong(2, expiry)
      bindString(3, database.PairingDaoAdapter.statusAdapter.encode(status))
      bindString(4, self_participant)
      bindString(5, peer_participant)
      bindString(6, controller_key)
      bindString(7, permissions?.let { database.PairingDaoAdapter.permissionsAdapter.encode(it) })
      bindString(8, relay_protocol)
      bindLong(9, metadata_id)
      bindString(10, topic_)
    }
    notifyQueries(-83516554, {database.pairingDaoQueries.getPairingByTopic +
        database.pairingDaoQueries.hasTopic + database.pairingDaoQueries.getListOfPairingDaos +
        database.pairingDaoQueries.getExpiry})
  }

  public override fun deletePairing(topic: String): Unit {
    driver.execute(725343575, """
    |DELETE FROM PairingDao
    |WHERE ? = topic
    """.trimMargin(), 1) {
      bindString(1, topic)
    }
    notifyQueries(725343575, {database.pairingDaoQueries.getPairingByTopic +
        database.pairingDaoQueries.hasTopic + database.pairingDaoQueries.getListOfPairingDaos +
        database.pairingDaoQueries.getExpiry})
  }

  public override fun updateAcknowledgedPairingMetadata(metadata_id: Long?, topic: String): Unit {
    driver.execute(-625709344, """
    |UPDATE PairingDao
    |SET metadata_id = ?
    |WHERE topic = ?
    """.trimMargin(), 2) {
      bindLong(1, metadata_id)
      bindString(2, topic)
    }
    notifyQueries(-625709344, {database.pairingDaoQueries.getPairingByTopic +
        database.pairingDaoQueries.hasTopic + database.pairingDaoQueries.getListOfPairingDaos +
        database.pairingDaoQueries.getExpiry})
  }

  private inner class GetPairingByTopicQuery<out T : Any>(
    public val topic: String,
    mapper: (SqlCursor) -> T
  ) : Query<T>(getPairingByTopic, mapper) {
    public override fun execute(): SqlCursor = driver.executeQuery(-1011676704, """
    |SELECT topic, expiry, uri, status, controller_type, self_participant, peer_participant, controller_key, relay_protocol, permissions
    |FROM PairingDao
    |WHERE ? = topic
    """.trimMargin(), 1) {
      bindString(1, topic)
    }

    public override fun toString(): String = "PairingDao.sq:getPairingByTopic"
  }

  private inner class HasTopicQuery<out T : Any>(
    public val topic: String,
    mapper: (SqlCursor) -> T
  ) : Query<T>(hasTopic, mapper) {
    public override fun execute(): SqlCursor = driver.executeQuery(-807654469, """
    |SELECT topic
    |FROM PairingDao
    |WHERE ? = topic
    """.trimMargin(), 1) {
      bindString(1, topic)
    }

    public override fun toString(): String = "PairingDao.sq:hasTopic"
  }

  private inner class GetExpiryQuery<out T : Any>(
    public val topic: String,
    mapper: (SqlCursor) -> T
  ) : Query<T>(getExpiry, mapper) {
    public override fun execute(): SqlCursor = driver.executeQuery(1387655139, """
    |SELECT expiry
    |FROM PairingDao
    |WHERE ? = topic
    """.trimMargin(), 1) {
      bindString(1, topic)
    }

    public override fun toString(): String = "PairingDao.sq:getExpiry"
  }
}

private class SessionDaoQueriesImpl(
  private val database: DatabaseImpl,
  private val driver: SqlDriver
) : TransacterImpl(driver), SessionDaoQueries {
  internal val getListOfSessionDaos: MutableList<Query<*>> = copyOnWriteList()

  internal val getPermissionsByTopic: MutableList<Query<*>> = copyOnWriteList()

  internal val getSessionByTopic: MutableList<Query<*>> = copyOnWriteList()

  internal val hasTopic: MutableList<Query<*>> = copyOnWriteList()

  internal val getExpiry: MutableList<Query<*>> = copyOnWriteList()

  public override fun <T : Any> getListOfSessionDaos(mapper: (
    topic: String,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    ttl_seconds: Long,
    accounts: List<String>?,
    expiry: Long,
    status: SequenceStatus,
    controller_type: ControllerType,
    _name: String?,
    description: String?,
    url: String?,
    icons: List<String>?,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    relay_protocol: String
  ) -> T): Query<T> = Query(-842394812, getListOfSessionDaos, driver, "SessionDao.sq",
      "getListOfSessionDaos", """
  |SELECT sd.topic, sd.permissions_chains, sd.permissions_methods, sd.permissions_types, sd.ttl_seconds, sd.accounts, sd.expiry, sd.status, sd.controller_type, mdd._name, mdd.description, mdd.url, mdd.icons, sd.self_participant, sd.peer_participant, sd.controller_key, sd.relay_protocol
  |FROM SessionDao sd
  |    LEFT JOIN MetaDataDao mdd ON sd.metadata_id = mdd.id
  """.trimMargin()) { cursor ->
    mapper(
      cursor.getString(0)!!,
      database.SessionDaoAdapter.permissions_chainsAdapter.decode(cursor.getString(1)!!),
      database.SessionDaoAdapter.permissions_methodsAdapter.decode(cursor.getString(2)!!),
      cursor.getString(3)?.let { database.SessionDaoAdapter.permissions_typesAdapter.decode(it) },
      cursor.getLong(4)!!,
      cursor.getString(5)?.let { database.SessionDaoAdapter.accountsAdapter.decode(it) },
      cursor.getLong(6)!!,
      database.SessionDaoAdapter.statusAdapter.decode(cursor.getString(7)!!),
      database.SessionDaoAdapter.controller_typeAdapter.decode(cursor.getString(8)!!),
      cursor.getString(9),
      cursor.getString(10),
      cursor.getString(11),
      cursor.getString(12)?.let { database.MetaDataDaoAdapter.iconsAdapter.decode(it) },
      cursor.getString(13)!!,
      cursor.getString(14),
      cursor.getString(15),
      cursor.getString(16)!!
    )
  }

  public override fun getListOfSessionDaos(): Query<GetListOfSessionDaos> = getListOfSessionDaos {
      topic, permissions_chains, permissions_methods, permissions_types, ttl_seconds, accounts,
      expiry, status, controller_type, _name, description, url, icons, self_participant,
      peer_participant, controller_key, relay_protocol ->
    GetListOfSessionDaos(
      topic,
      permissions_chains,
      permissions_methods,
      permissions_types,
      ttl_seconds,
      accounts,
      expiry,
      status,
      controller_type,
      _name,
      description,
      url,
      icons,
      self_participant,
      peer_participant,
      controller_key,
      relay_protocol
    )
  }

  public override fun <T : Any> getPermissionsByTopic(topic: String,
      mapper: (permissions_chains: List<String>, permissions_methods: List<String>) -> T): Query<T>
      = GetPermissionsByTopicQuery(topic) { cursor ->
    mapper(
      database.SessionDaoAdapter.permissions_chainsAdapter.decode(cursor.getString(0)!!),
      database.SessionDaoAdapter.permissions_methodsAdapter.decode(cursor.getString(1)!!)
    )
  }

  public override fun getPermissionsByTopic(topic: String): Query<GetPermissionsByTopic> =
      getPermissionsByTopic(topic) { permissions_chains, permissions_methods ->
    GetPermissionsByTopic(
      permissions_chains,
      permissions_methods
    )
  }

  public override fun <T : Any> getSessionByTopic(topic: String, mapper: (
    topic: String,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    ttl_seconds: Long,
    accounts: List<String>?,
    expiry: Long,
    status: SequenceStatus,
    controller_type: ControllerType,
    _name: String?,
    description: String?,
    url: String?,
    icons: List<String>?,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    relay_protocol: String
  ) -> T): Query<T> = GetSessionByTopicQuery(topic) { cursor ->
    mapper(
      cursor.getString(0)!!,
      database.SessionDaoAdapter.permissions_chainsAdapter.decode(cursor.getString(1)!!),
      database.SessionDaoAdapter.permissions_methodsAdapter.decode(cursor.getString(2)!!),
      cursor.getString(3)?.let { database.SessionDaoAdapter.permissions_typesAdapter.decode(it) },
      cursor.getLong(4)!!,
      cursor.getString(5)?.let { database.SessionDaoAdapter.accountsAdapter.decode(it) },
      cursor.getLong(6)!!,
      database.SessionDaoAdapter.statusAdapter.decode(cursor.getString(7)!!),
      database.SessionDaoAdapter.controller_typeAdapter.decode(cursor.getString(8)!!),
      cursor.getString(9),
      cursor.getString(10),
      cursor.getString(11),
      cursor.getString(12)?.let { database.MetaDataDaoAdapter.iconsAdapter.decode(it) },
      cursor.getString(13)!!,
      cursor.getString(14),
      cursor.getString(15),
      cursor.getString(16)!!
    )
  }

  public override fun getSessionByTopic(topic: String): Query<GetSessionByTopic> =
      getSessionByTopic(topic) { topic_, permissions_chains, permissions_methods, permissions_types,
      ttl_seconds, accounts, expiry, status, controller_type, _name, description, url, icons,
      self_participant, peer_participant, controller_key, relay_protocol ->
    GetSessionByTopic(
      topic_,
      permissions_chains,
      permissions_methods,
      permissions_types,
      ttl_seconds,
      accounts,
      expiry,
      status,
      controller_type,
      _name,
      description,
      url,
      icons,
      self_participant,
      peer_participant,
      controller_key,
      relay_protocol
    )
  }

  public override fun hasTopic(topic: String): Query<String> = HasTopicQuery(topic) { cursor ->
    cursor.getString(0)!!
  }

  public override fun getExpiry(topic: String): Query<Long> = GetExpiryQuery(topic) { cursor ->
    cursor.getLong(0)!!
  }

  public override fun insertSession(
    topic: String,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    self_participant: String,
    ttl_seconds: Long,
    expiry: Long,
    status: SequenceStatus,
    controller_type: ControllerType,
    metadata_id: Long?,
    relay_protocol: String
  ): Unit {
    driver.execute(1995991333, """
    |INSERT OR IGNORE INTO SessionDao(topic, permissions_chains, permissions_methods, permissions_types, self_participant, ttl_seconds, expiry,  status, controller_type, metadata_id, relay_protocol)
    |VALUES (?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?)
    """.trimMargin(), 11) {
      bindString(1, topic)
      bindString(2, database.SessionDaoAdapter.permissions_chainsAdapter.encode(permissions_chains))
      bindString(3,
          database.SessionDaoAdapter.permissions_methodsAdapter.encode(permissions_methods))
      bindString(4, permissions_types?.let {
          database.SessionDaoAdapter.permissions_typesAdapter.encode(it) })
      bindString(5, self_participant)
      bindLong(6, ttl_seconds)
      bindLong(7, expiry)
      bindString(8, database.SessionDaoAdapter.statusAdapter.encode(status))
      bindString(9, database.SessionDaoAdapter.controller_typeAdapter.encode(controller_type))
      bindLong(10, metadata_id)
      bindString(11, relay_protocol)
    }
    notifyQueries(1995991333, {database.sessionDaoQueries.getExpiry +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic + database.sessionDaoQueries.hasTopic})
  }

  public override fun updateProposedSessionToResponded(status: SequenceStatus, topic: String):
      Unit {
    driver.execute(-2002278294, """
    |UPDATE OR ABORT SessionDao
    |SET status = ?
    |WHERE topic = ?
    """.trimMargin(), 2) {
      bindString(1, database.SessionDaoAdapter.statusAdapter.encode(status))
      bindString(2, topic)
    }
    notifyQueries(-2002278294, {database.sessionDaoQueries.getExpiry +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic + database.sessionDaoQueries.hasTopic})
  }

  public override fun updateRespondedSessionToPresettled(
    topic: String,
    accounts: List<String>?,
    expiry: Long,
    status: SequenceStatus,
    self_participant: String,
    controller_key: String?,
    peer_participant: String?,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    ttl_seconds: Long,
    topic_: String
  ): Unit {
    driver.execute(-901611180, """
    |UPDATE OR ABORT SessionDao
    |SET topic = ?, accounts = ?, expiry = ?, status = ?, self_participant = ?, controller_key = ?, peer_participant = ?, permissions_chains = ?, permissions_methods = ?, permissions_types = ?, ttl_seconds = ?
    |WHERE topic = ?
    """.trimMargin(), 12) {
      bindString(1, topic)
      bindString(2, accounts?.let { database.SessionDaoAdapter.accountsAdapter.encode(it) })
      bindLong(3, expiry)
      bindString(4, database.SessionDaoAdapter.statusAdapter.encode(status))
      bindString(5, self_participant)
      bindString(6, controller_key)
      bindString(7, peer_participant)
      bindString(8, database.SessionDaoAdapter.permissions_chainsAdapter.encode(permissions_chains))
      bindString(9,
          database.SessionDaoAdapter.permissions_methodsAdapter.encode(permissions_methods))
      bindString(10, permissions_types?.let {
          database.SessionDaoAdapter.permissions_typesAdapter.encode(it) })
      bindLong(11, ttl_seconds)
      bindString(12, topic_)
    }
    notifyQueries(-901611180, {database.sessionDaoQueries.getExpiry +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic + database.sessionDaoQueries.hasTopic})
  }

  public override fun updatePreSettledSessionToAcknowledged(status: SequenceStatus, topic: String):
      Unit {
    driver.execute(-1974442398, """
    |UPDATE OR ABORT SessionDao
    |SET status = ?
    |WHERE topic = ?
    """.trimMargin(), 2) {
      bindString(1, database.SessionDaoAdapter.statusAdapter.encode(status))
      bindString(2, topic)
    }
    notifyQueries(-1974442398, {database.sessionDaoQueries.getExpiry +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic + database.sessionDaoQueries.hasTopic})
  }

  public override fun updateProposedSessionToAcknowledged(
    topic: String,
    accounts: List<String>?,
    expiry: Long,
    status: SequenceStatus,
    self_participant: String,
    controller_key: String?,
    peer_participant: String?,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    ttl_seconds: Long,
    relay_protocol: String,
    metadata_id: Long?,
    topic_: String
  ): Unit {
    driver.execute(551430418, """
    |UPDATE OR ABORT SessionDao
    |SET topic = ?, accounts = ?, expiry = ?, status = ?, self_participant = ?, controller_key = ?, peer_participant = ?, permissions_chains = ?, permissions_methods = ?, permissions_types = ?, ttl_seconds = ?, relay_protocol = ?, metadata_id =?
    |WHERE topic = ?
    """.trimMargin(), 14) {
      bindString(1, topic)
      bindString(2, accounts?.let { database.SessionDaoAdapter.accountsAdapter.encode(it) })
      bindLong(3, expiry)
      bindString(4, database.SessionDaoAdapter.statusAdapter.encode(status))
      bindString(5, self_participant)
      bindString(6, controller_key)
      bindString(7, peer_participant)
      bindString(8, database.SessionDaoAdapter.permissions_chainsAdapter.encode(permissions_chains))
      bindString(9,
          database.SessionDaoAdapter.permissions_methodsAdapter.encode(permissions_methods))
      bindString(10, permissions_types?.let {
          database.SessionDaoAdapter.permissions_typesAdapter.encode(it) })
      bindLong(11, ttl_seconds)
      bindString(12, relay_protocol)
      bindLong(13, metadata_id)
      bindString(14, topic_)
    }
    notifyQueries(551430418, {database.sessionDaoQueries.getExpiry +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic + database.sessionDaoQueries.hasTopic})
  }

  public override fun updateSessionWithPermissions(
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    topic: String
  ): Unit {
    driver.execute(1597611529, """
    |UPDATE OR ABORT SessionDao
    |SET permissions_chains = ?, permissions_methods = ?
    |WHERE topic = ?
    """.trimMargin(), 3) {
      bindString(1, database.SessionDaoAdapter.permissions_chainsAdapter.encode(permissions_chains))
      bindString(2,
          database.SessionDaoAdapter.permissions_methodsAdapter.encode(permissions_methods))
      bindString(3, topic)
    }
    notifyQueries(1597611529, {database.sessionDaoQueries.getExpiry +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic + database.sessionDaoQueries.hasTopic})
  }

  public override fun updateSessionWithAccounts(accounts: List<String>?, topic: String): Unit {
    driver.execute(1481246849, """
    |UPDATE OR ABORT SessionDao
    |SET accounts = ?
    |WHERE topic = ?
    """.trimMargin(), 2) {
      bindString(1, accounts?.let { database.SessionDaoAdapter.accountsAdapter.encode(it) })
      bindString(2, topic)
    }
    notifyQueries(1481246849, {database.sessionDaoQueries.getExpiry +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic + database.sessionDaoQueries.hasTopic})
  }

  public override fun deleteSession(topic: String): Unit {
    driver.execute(-1992240653, """
    |DELETE FROM SessionDao
    |WHERE topic = ?
    """.trimMargin(), 1) {
      bindString(1, topic)
    }
    notifyQueries(-1992240653, {database.sessionDaoQueries.getExpiry +
        database.sessionDaoQueries.getListOfSessionDaos +
        database.sessionDaoQueries.getPermissionsByTopic +
        database.sessionDaoQueries.getSessionByTopic + database.sessionDaoQueries.hasTopic})
  }

  private inner class GetPermissionsByTopicQuery<out T : Any>(
    public val topic: String,
    mapper: (SqlCursor) -> T
  ) : Query<T>(getPermissionsByTopic, mapper) {
    public override fun execute(): SqlCursor = driver.executeQuery(-620727438, """
    |SELECT sd.permissions_chains, sd.permissions_methods
    |FROM SessionDao sd
    |    LEFT JOIN MetaDataDao mdd ON sd.metadata_id = mdd.id
    |WHERE topic = ?
    """.trimMargin(), 1) {
      bindString(1, topic)
    }

    public override fun toString(): String = "SessionDao.sq:getPermissionsByTopic"
  }

  private inner class GetSessionByTopicQuery<out T : Any>(
    public val topic: String,
    mapper: (SqlCursor) -> T
  ) : Query<T>(getSessionByTopic, mapper) {
    public override fun execute(): SqlCursor = driver.executeQuery(-551371744, """
    |SELECT sd.topic, sd.permissions_chains, sd.permissions_methods, sd.permissions_types, sd.ttl_seconds, sd.accounts, sd.expiry, sd.status, sd.controller_type, mdd._name, mdd.description, mdd.url, mdd.icons, sd.self_participant, sd.peer_participant, sd.controller_key, sd.relay_protocol
    |FROM SessionDao sd
    |    LEFT JOIN MetaDataDao mdd ON sd.metadata_id = mdd.id
    |WHERE topic = ?
    """.trimMargin(), 1) {
      bindString(1, topic)
    }

    public override fun toString(): String = "SessionDao.sq:getSessionByTopic"
  }

  private inner class HasTopicQuery<out T : Any>(
    public val topic: String,
    mapper: (SqlCursor) -> T
  ) : Query<T>(hasTopic, mapper) {
    public override fun execute(): SqlCursor = driver.executeQuery(-344661459, """
    |SELECT topic
    |FROM SessionDao
    |WHERE ? = topic
    """.trimMargin(), 1) {
      bindString(1, topic)
    }

    public override fun toString(): String = "SessionDao.sq:hasTopic"
  }

  private inner class GetExpiryQuery<out T : Any>(
    public val topic: String,
    mapper: (SqlCursor) -> T
  ) : Query<T>(getExpiry, mapper) {
    public override fun execute(): SqlCursor = driver.executeQuery(-1439430735, """
    |SELECT expiry
    |FROM SessionDao
    |WHERE ? = topic
    """.trimMargin(), 1) {
      bindString(1, topic)
    }

    public override fun toString(): String = "SessionDao.sq:getExpiry"
  }
}
