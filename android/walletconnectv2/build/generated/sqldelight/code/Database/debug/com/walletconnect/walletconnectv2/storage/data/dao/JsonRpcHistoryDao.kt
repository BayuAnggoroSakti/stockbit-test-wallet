package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.squareup.sqldelight.ColumnAdapter
import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus
import kotlin.Long
import kotlin.String

public data class JsonRpcHistoryDao(
  public val id: Long,
  public val request_id: Long,
  public val topic: String,
  public val method: String?,
  public val body: String?,
  public val status: JsonRpcStatus,
  public val controller_type: ControllerType
) {
  public override fun toString(): String = """
  |JsonRpcHistoryDao [
  |  id: $id
  |  request_id: $request_id
  |  topic: $topic
  |  method: $method
  |  body: $body
  |  status: $status
  |  controller_type: $controller_type
  |]
  """.trimMargin()

  public class Adapter(
    public val statusAdapter: ColumnAdapter<JsonRpcStatus, String>,
    public val controller_typeAdapter: ColumnAdapter<ControllerType, String>
  )
}
