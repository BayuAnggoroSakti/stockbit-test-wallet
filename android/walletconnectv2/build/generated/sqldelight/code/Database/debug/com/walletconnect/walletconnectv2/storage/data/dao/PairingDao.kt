package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.squareup.sqldelight.ColumnAdapter
import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus
import kotlin.Long
import kotlin.String
import kotlin.collections.List

public data class PairingDao(
  public val id: Long,
  public val topic: String,
  public val uri: String,
  public val expiry: Long,
  public val status: SequenceStatus,
  public val controller_type: ControllerType,
  public val self_participant: String,
  public val peer_participant: String?,
  public val controller_key: String?,
  public val relay_protocol: String,
  public val permissions: List<String>?,
  public val metadata_id: Long?
) {
  public override fun toString(): String = """
  |PairingDao [
  |  id: $id
  |  topic: $topic
  |  uri: $uri
  |  expiry: $expiry
  |  status: $status
  |  controller_type: $controller_type
  |  self_participant: $self_participant
  |  peer_participant: $peer_participant
  |  controller_key: $controller_key
  |  relay_protocol: $relay_protocol
  |  permissions: $permissions
  |  metadata_id: $metadata_id
  |]
  """.trimMargin()

  public class Adapter(
    public val statusAdapter: ColumnAdapter<SequenceStatus, String>,
    public val controller_typeAdapter: ColumnAdapter<ControllerType, String>,
    public val permissionsAdapter: ColumnAdapter<List<String>, String>
  )
}
