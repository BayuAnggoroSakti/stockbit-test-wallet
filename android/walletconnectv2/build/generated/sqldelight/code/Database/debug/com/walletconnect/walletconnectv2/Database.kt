package com.walletconnect.walletconnectv2

import com.squareup.sqldelight.Transacter
import com.squareup.sqldelight.db.SqlDriver
import com.walletconnect.walletconnectv2.storage.`data`.dao.JsonRpcHistoryDao
import com.walletconnect.walletconnectv2.storage.`data`.dao.JsonRpcHistoryQueries
import com.walletconnect.walletconnectv2.storage.`data`.dao.MetaDataDao
import com.walletconnect.walletconnectv2.storage.`data`.dao.MetaDataDaoQueries
import com.walletconnect.walletconnectv2.storage.`data`.dao.PairingDao
import com.walletconnect.walletconnectv2.storage.`data`.dao.PairingDaoQueries
import com.walletconnect.walletconnectv2.storage.`data`.dao.SessionDao
import com.walletconnect.walletconnectv2.storage.`data`.dao.SessionDaoQueries
import com.walletconnect.walletconnectv2.walletconnectv2.newInstance
import com.walletconnect.walletconnectv2.walletconnectv2.schema

public interface Database : Transacter {
  public val jsonRpcHistoryQueries: JsonRpcHistoryQueries

  public val metaDataDaoQueries: MetaDataDaoQueries

  public val pairingDaoQueries: PairingDaoQueries

  public val sessionDaoQueries: SessionDaoQueries

  public companion object {
    public val Schema: SqlDriver.Schema
      get() = Database::class.schema

    public operator fun invoke(
      driver: SqlDriver,
      JsonRpcHistoryDaoAdapter: JsonRpcHistoryDao.Adapter,
      MetaDataDaoAdapter: MetaDataDao.Adapter,
      PairingDaoAdapter: PairingDao.Adapter,
      SessionDaoAdapter: SessionDao.Adapter
    ): Database = Database::class.newInstance(driver, JsonRpcHistoryDaoAdapter, MetaDataDaoAdapter,
        PairingDaoAdapter, SessionDaoAdapter)
  }
}
