package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus
import kotlin.Long
import kotlin.String

public data class GetJsonRpcRespondsDaos(
  public val request_id: Long,
  public val topic: String,
  public val method: String?,
  public val body: String?,
  public val status: JsonRpcStatus,
  public val controller_type: ControllerType
) {
  public override fun toString(): String = """
  |GetJsonRpcRespondsDaos [
  |  request_id: $request_id
  |  topic: $topic
  |  method: $method
  |  body: $body
  |  status: $status
  |  controller_type: $controller_type
  |]
  """.trimMargin()
}
