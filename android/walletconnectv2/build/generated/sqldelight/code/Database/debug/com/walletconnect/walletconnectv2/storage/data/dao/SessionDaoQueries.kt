package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.squareup.sqldelight.Query
import com.squareup.sqldelight.Transacter
import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus
import kotlin.Any
import kotlin.Long
import kotlin.String
import kotlin.Unit
import kotlin.collections.List

public interface SessionDaoQueries : Transacter {
  public fun <T : Any> getListOfSessionDaos(mapper: (
    topic: String,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    ttl_seconds: Long,
    accounts: List<String>?,
    expiry: Long,
    status: SequenceStatus,
    controller_type: ControllerType,
    _name: String?,
    description: String?,
    url: String?,
    icons: List<String>?,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    relay_protocol: String
  ) -> T): Query<T>

  public fun getListOfSessionDaos(): Query<GetListOfSessionDaos>

  public fun <T : Any> getPermissionsByTopic(topic: String,
      mapper: (permissions_chains: List<String>, permissions_methods: List<String>) -> T): Query<T>

  public fun getPermissionsByTopic(topic: String): Query<GetPermissionsByTopic>

  public fun <T : Any> getSessionByTopic(topic: String, mapper: (
    topic: String,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    ttl_seconds: Long,
    accounts: List<String>?,
    expiry: Long,
    status: SequenceStatus,
    controller_type: ControllerType,
    _name: String?,
    description: String?,
    url: String?,
    icons: List<String>?,
    self_participant: String,
    peer_participant: String?,
    controller_key: String?,
    relay_protocol: String
  ) -> T): Query<T>

  public fun getSessionByTopic(topic: String): Query<GetSessionByTopic>

  public fun hasTopic(topic: String): Query<String>

  public fun getExpiry(topic: String): Query<Long>

  public fun insertSession(
    topic: String,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    self_participant: String,
    ttl_seconds: Long,
    expiry: Long,
    status: SequenceStatus,
    controller_type: ControllerType,
    metadata_id: Long?,
    relay_protocol: String
  ): Unit

  public fun updateProposedSessionToResponded(status: SequenceStatus, topic: String): Unit

  public fun updateRespondedSessionToPresettled(
    topic: String,
    accounts: List<String>?,
    expiry: Long,
    status: SequenceStatus,
    self_participant: String,
    controller_key: String?,
    peer_participant: String?,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    ttl_seconds: Long,
    topic_: String
  ): Unit

  public fun updatePreSettledSessionToAcknowledged(status: SequenceStatus, topic: String): Unit

  public fun updateProposedSessionToAcknowledged(
    topic: String,
    accounts: List<String>?,
    expiry: Long,
    status: SequenceStatus,
    self_participant: String,
    controller_key: String?,
    peer_participant: String?,
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    permissions_types: List<String>?,
    ttl_seconds: Long,
    relay_protocol: String,
    metadata_id: Long?,
    topic_: String
  ): Unit

  public fun updateSessionWithPermissions(
    permissions_chains: List<String>,
    permissions_methods: List<String>,
    topic: String
  ): Unit

  public fun updateSessionWithAccounts(accounts: List<String>?, topic: String): Unit

  public fun deleteSession(topic: String): Unit
}
