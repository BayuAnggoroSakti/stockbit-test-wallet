package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.squareup.sqldelight.Query
import com.squareup.sqldelight.Transacter
import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus
import kotlin.Any
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.Unit
import kotlin.collections.Collection

public interface JsonRpcHistoryQueries : Transacter {
  public fun doesJsonRpcNotExist(request_id: Long): Query<Boolean>

  public fun selectLastInsertedRowId(): Query<Long>

  public fun <T : Any> getJsonRpcRequestsDaos(
    topic: String,
    method: Collection<String?>,
    mapper: (
      request_id: Long,
      topic: String,
      method: String?,
      body: String?,
      status: JsonRpcStatus,
      controller_type: ControllerType
    ) -> T
  ): Query<T>

  public fun getJsonRpcRequestsDaos(topic: String, method: Collection<String?>):
      Query<GetJsonRpcRequestsDaos>

  public fun <T : Any> getJsonRpcRespondsDaos(
    topic: String,
    method: Collection<String?>,
    mapper: (
      request_id: Long,
      topic: String,
      method: String?,
      body: String?,
      status: JsonRpcStatus,
      controller_type: ControllerType
    ) -> T
  ): Query<T>

  public fun getJsonRpcRespondsDaos(topic: String, method: Collection<String?>):
      Query<GetJsonRpcRespondsDaos>

  public fun insertJsonRpcHistory(
    request_id: Long,
    topic: String,
    method: String?,
    body: String?,
    status: JsonRpcStatus,
    controller_type: ControllerType
  ): Unit

  public fun updateJsonRpcHistory(status: JsonRpcStatus, request_id: Long): Unit

  public fun deleteJsonRpcHistory(topic: String): Unit
}
