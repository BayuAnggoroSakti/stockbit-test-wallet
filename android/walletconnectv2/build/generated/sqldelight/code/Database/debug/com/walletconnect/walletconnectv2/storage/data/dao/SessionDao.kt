package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.squareup.sqldelight.ColumnAdapter
import com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType
import com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus
import kotlin.Long
import kotlin.String
import kotlin.collections.List

public data class SessionDao(
  public val id: Long,
  public val topic: String,
  public val permissions_chains: List<String>,
  public val permissions_methods: List<String>,
  public val permissions_types: List<String>?,
  public val self_participant: String,
  public val peer_participant: String?,
  public val controller_key: String?,
  public val ttl_seconds: Long,
  public val accounts: List<String>?,
  public val expiry: Long,
  public val status: SequenceStatus,
  public val controller_type: ControllerType,
  public val metadata_id: Long?,
  public val relay_protocol: String
) {
  public override fun toString(): String = """
  |SessionDao [
  |  id: $id
  |  topic: $topic
  |  permissions_chains: $permissions_chains
  |  permissions_methods: $permissions_methods
  |  permissions_types: $permissions_types
  |  self_participant: $self_participant
  |  peer_participant: $peer_participant
  |  controller_key: $controller_key
  |  ttl_seconds: $ttl_seconds
  |  accounts: $accounts
  |  expiry: $expiry
  |  status: $status
  |  controller_type: $controller_type
  |  metadata_id: $metadata_id
  |  relay_protocol: $relay_protocol
  |]
  """.trimMargin()

  public class Adapter(
    public val permissions_chainsAdapter: ColumnAdapter<List<String>, String>,
    public val permissions_methodsAdapter: ColumnAdapter<List<String>, String>,
    public val permissions_typesAdapter: ColumnAdapter<List<String>, String>,
    public val accountsAdapter: ColumnAdapter<List<String>, String>,
    public val statusAdapter: ColumnAdapter<SequenceStatus, String>,
    public val controller_typeAdapter: ColumnAdapter<ControllerType, String>
  )
}
