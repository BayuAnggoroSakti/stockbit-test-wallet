package com.walletconnect.walletconnectv2.storage.`data`.dao

import com.squareup.sqldelight.Query
import com.squareup.sqldelight.Transacter
import kotlin.Any
import kotlin.Long
import kotlin.String
import kotlin.Unit
import kotlin.collections.List

public interface MetaDataDaoQueries : Transacter {
  public fun lastInsertedRowId(): Query<Long>

  public fun <T : Any> getMetaData(mapper: (
    id: Long,
    _name: String,
    description: String,
    url: String,
    icons: List<String>
  ) -> T): Query<T>

  public fun getMetaData(): Query<MetaDataDao>

  public fun insertOrIgnoreMetaData(
    _name: String,
    description: String,
    url: String,
    icons: List<String>
  ): Unit

  public fun deleteMetaDataFromTopic(topic: String): Unit
}
