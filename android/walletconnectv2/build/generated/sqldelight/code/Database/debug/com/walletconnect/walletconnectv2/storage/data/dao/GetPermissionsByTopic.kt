package com.walletconnect.walletconnectv2.storage.`data`.dao

import kotlin.String
import kotlin.collections.List

public data class GetPermissionsByTopic(
  public val permissions_chains: List<String>,
  public val permissions_methods: List<String>
) {
  public override fun toString(): String = """
  |GetPermissionsByTopic [
  |  permissions_chains: $permissions_chains
  |  permissions_methods: $permissions_methods
  |]
  """.trimMargin()
}
