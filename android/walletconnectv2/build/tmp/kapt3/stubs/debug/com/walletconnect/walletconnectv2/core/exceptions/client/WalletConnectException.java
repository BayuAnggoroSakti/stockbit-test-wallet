package com.walletconnect.walletconnectv2.core.exceptions.client;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00060\u0001j\u0002`\u0002:\u000e\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015B\u0011\b\u0004\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0005R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u0082\u0001\u000e\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#\u00a8\u0006$"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "CannotFindSequenceForTopic", "GenericException", "InvalidAccountsException", "InvalidNotificationException", "InvalidProjectIdException", "InvalidSessionPermissionsException", "InvalidSessionProposalException", "MalformedWalletConnectUri", "PairWithExistingPairingIsNotAllowed", "ProjectIdDoesNotExistException", "TimeoutException", "UnauthorizedChainIdException", "UnauthorizedNotificationException", "UnauthorizedPeerException", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$ProjectIdDoesNotExistException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidProjectIdException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$GenericException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$MalformedWalletConnectUri;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$UnauthorizedPeerException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidSessionPermissionsException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidSessionProposalException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidAccountsException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidNotificationException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$UnauthorizedNotificationException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$UnauthorizedChainIdException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$CannotFindSequenceForTopic;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$PairWithExistingPairingIsNotAllowed;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$TimeoutException;", "walletconnectv2_debug"})
public abstract class WalletConnectException extends java.lang.Exception {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String message = null;
    
    private WalletConnectException(java.lang.String message) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String getMessage() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$ProjectIdDoesNotExistException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class ProjectIdDoesNotExistException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public ProjectIdDoesNotExistException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidProjectIdException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class InvalidProjectIdException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public InvalidProjectIdException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$GenericException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class GenericException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public GenericException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$MalformedWalletConnectUri;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class MalformedWalletConnectUri extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public MalformedWalletConnectUri(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$UnauthorizedPeerException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class UnauthorizedPeerException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public UnauthorizedPeerException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidSessionPermissionsException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class InvalidSessionPermissionsException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public InvalidSessionPermissionsException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidSessionProposalException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class InvalidSessionProposalException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public InvalidSessionProposalException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidAccountsException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class InvalidAccountsException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public InvalidAccountsException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$InvalidNotificationException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class InvalidNotificationException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public InvalidNotificationException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$UnauthorizedNotificationException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class UnauthorizedNotificationException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public UnauthorizedNotificationException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$UnauthorizedChainIdException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class UnauthorizedChainIdException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public UnauthorizedChainIdException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$CannotFindSequenceForTopic;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class CannotFindSequenceForTopic extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public CannotFindSequenceForTopic(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$PairWithExistingPairingIsNotAllowed;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class PairWithExistingPairingIsNotAllowed extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public PairWithExistingPairingIsNotAllowed(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException$TimeoutException;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class TimeoutException extends com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String message = null;
        
        public TimeoutException(@org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.String getMessage() {
            return null;
        }
    }
}