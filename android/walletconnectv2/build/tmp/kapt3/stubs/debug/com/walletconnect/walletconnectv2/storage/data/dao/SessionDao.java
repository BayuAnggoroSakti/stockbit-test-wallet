package com.walletconnect.walletconnectv2.storage.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b+\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001DB\u009f\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007\u0012\u000e\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007\u0012\u0006\u0010\u000f\u001a\u00020\u0003\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0016J\t\u0010-\u001a\u00020\u0003H\u00c6\u0003J\u0011\u0010.\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007H\u00c6\u0003J\t\u0010/\u001a\u00020\u0003H\u00c6\u0003J\t\u00100\u001a\u00020\u0011H\u00c6\u0003J\t\u00101\u001a\u00020\u0013H\u00c6\u0003J\u0010\u00102\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010!J\t\u00103\u001a\u00020\u0005H\u00c6\u0003J\t\u00104\u001a\u00020\u0005H\u00c6\u0003J\u000f\u00105\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H\u00c6\u0003J\u000f\u00106\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H\u00c6\u0003J\u0011\u00107\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007H\u00c6\u0003J\t\u00108\u001a\u00020\u0005H\u00c6\u0003J\u000b\u00109\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010:\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010;\u001a\u00020\u0003H\u00c6\u0003J\u00c6\u0001\u0010<\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00072\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00072\u0010\b\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00072\b\b\u0002\u0010\n\u001a\u00020\u00052\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\r\u001a\u00020\u00032\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00072\b\b\u0002\u0010\u000f\u001a\u00020\u00032\b\b\u0002\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00132\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0015\u001a\u00020\u0005H\u00c6\u0001\u00a2\u0006\u0002\u0010=J\u0013\u0010>\u001a\u00020?2\b\u0010@\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010A\u001a\u00020BH\u00d6\u0001J\b\u0010C\u001a\u00020\u0005H\u0016R\u0019\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001eR\u0015\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\"\u001a\u0004\b \u0010!R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001aR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0018R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0018R\u0019\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0018R\u0011\u0010\u0015\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010\u001aR\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001aR\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010\u001aR\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010\u001e\u00a8\u0006E"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDao;", "", "id", "", "topic", "", "permissions_chains", "", "permissions_methods", "permissions_types", "self_participant", "peer_participant", "controller_key", "ttl_seconds", "accounts", "expiry", "status", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "controller_type", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "metadata_id", "relay_protocol", "(JLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/util/List;JLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Ljava/lang/Long;Ljava/lang/String;)V", "getAccounts", "()Ljava/util/List;", "getController_key", "()Ljava/lang/String;", "getController_type", "()Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "getExpiry", "()J", "getId", "getMetadata_id", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getPeer_participant", "getPermissions_chains", "getPermissions_methods", "getPermissions_types", "getRelay_protocol", "getSelf_participant", "getStatus", "()Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "getTopic", "getTtl_seconds", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(JLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/util/List;JLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Ljava/lang/Long;Ljava/lang/String;)Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDao;", "equals", "", "other", "hashCode", "", "toString", "Adapter", "walletconnectv2_debug"})
public final class SessionDao {
    private final long id = 0L;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String topic = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> permissions_chains = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> permissions_methods = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.String> permissions_types = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String self_participant = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String peer_participant = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String controller_key = null;
    private final long ttl_seconds = 0L;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.String> accounts = null;
    private final long expiry = 0L;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Long metadata_id = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String relay_protocol = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.data.dao.SessionDao copy(long id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions_types, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, long ttl_seconds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> accounts, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    public SessionDao(long id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions_types, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, long ttl_seconds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> accounts, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol) {
        super();
    }
    
    public final long component1() {
        return 0L;
    }
    
    public final long getId() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTopic() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getPermissions_chains() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getPermissions_methods() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getPermissions_types() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSelf_participant() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPeer_participant() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getController_key() {
        return null;
    }
    
    public final long component9() {
        return 0L;
    }
    
    public final long getTtl_seconds() {
        return 0L;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getAccounts() {
        return null;
    }
    
    public final long component11() {
        return 0L;
    }
    
    public final long getExpiry() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus component12() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus getStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType getController_type() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getMetadata_id() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRelay_protocol() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0095\u0001\u0012\u0018\u0010\u0002\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0018\u0010\u0006\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0018\u0010\u0007\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0018\u0010\b\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0002\u0010\rR#\u0010\b\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001d\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR#\u0010\u0002\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000fR#\u0010\u0006\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000fR#\u0010\u0007\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000fR\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000f\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDao$Adapter;", "", "permissions_chainsAdapter", "Lcom/squareup/sqldelight/ColumnAdapter;", "", "", "permissions_methodsAdapter", "permissions_typesAdapter", "accountsAdapter", "statusAdapter", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "controller_typeAdapter", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "(Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;)V", "getAccountsAdapter", "()Lcom/squareup/sqldelight/ColumnAdapter;", "getController_typeAdapter", "getPermissions_chainsAdapter", "getPermissions_methodsAdapter", "getPermissions_typesAdapter", "getStatusAdapter", "walletconnectv2_debug"})
    public static final class Adapter {
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> permissions_chainsAdapter = null;
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> permissions_methodsAdapter = null;
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> permissions_typesAdapter = null;
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> accountsAdapter = null;
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, java.lang.String> statusAdapter = null;
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> controller_typeAdapter = null;
        
        public Adapter(@org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> permissions_chainsAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> permissions_methodsAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> permissions_typesAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> accountsAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, java.lang.String> statusAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> controller_typeAdapter) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> getPermissions_chainsAdapter() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> getPermissions_methodsAdapter() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> getPermissions_typesAdapter() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> getAccountsAdapter() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, java.lang.String> getStatusAdapter() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> getController_typeAdapter() {
            return null;
        }
    }
}