package com.walletconnect.walletconnectv2.relay.data.serializer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u001d\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rH\u0000\u00a2\u0006\u0002\b\u000eJ\u001f\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\nH\u0000\u00a2\u0006\u0002\b\u0013J\u0010\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u001d\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\f\u001a\u00020\rH\u0000\u00a2\u0006\u0002\b\u0017J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u000b\u001a\u00020\nH\u0002J \u0010\u001a\u001a\u0004\u0018\u0001H\u001b\"\u0006\b\u0000\u0010\u001b\u0018\u00012\u0006\u0010\u0012\u001a\u00020\nH\u0086\b\u00a2\u0006\u0002\u0010\u001cJ\u001e\u0010\u001d\u001a\u00020\n\"\u0006\b\u0000\u0010\u001b\u0018\u00012\u0006\u0010\u001e\u001a\u0002H\u001bH\u0082\b\u00a2\u0006\u0002\u0010\u001fJ\f\u0010 \u001a\u00020\n*\u00020\nH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"}, d2 = {"Lcom/walletconnect/walletconnectv2/relay/data/serializer/JsonRpcSerializer;", "", "codec", "Lcom/walletconnect/walletconnectv2/relay/Codec;", "crypto", "Lcom/walletconnect/walletconnectv2/crypto/CryptoRepository;", "moshi", "Lcom/squareup/moshi/Moshi;", "(Lcom/walletconnect/walletconnectv2/relay/Codec;Lcom/walletconnect/walletconnectv2/crypto/CryptoRepository;Lcom/squareup/moshi/Moshi;)V", "decode", "", "message", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "decode$walletconnectv2_debug", "deserialize", "Lcom/walletconnect/walletconnectv2/core/model/type/ClientParams;", "method", "json", "deserialize$walletconnectv2_debug", "serialize", "payload", "Lcom/walletconnect/walletconnectv2/core/model/type/SerializableJsonRpc;", "serialize$walletconnectv2_debug", "toEncryptionPayload", "Lcom/walletconnect/walletconnectv2/core/model/vo/EncryptionPayloadVO;", "tryDeserialize", "T", "(Ljava/lang/String;)Ljava/lang/Object;", "trySerialize", "type", "(Ljava/lang/Object;)Ljava/lang/String;", "encode", "walletconnectv2_debug"})
public final class JsonRpcSerializer {
    private final com.walletconnect.walletconnectv2.relay.Codec codec = null;
    private final com.walletconnect.walletconnectv2.crypto.CryptoRepository crypto = null;
    private final com.squareup.moshi.Moshi moshi = null;
    
    public JsonRpcSerializer(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.relay.Codec codec, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.crypto.CryptoRepository crypto, @org.jetbrains.annotations.NotNull()
    com.squareup.moshi.Moshi moshi) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String serialize$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.SerializableJsonRpc payload, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String decode$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.walletconnect.walletconnectv2.core.model.type.ClientParams deserialize$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String method, @org.jetbrains.annotations.NotNull()
    java.lang.String json) {
        return null;
    }
    
    private final java.lang.String serialize(com.walletconnect.walletconnectv2.core.model.type.SerializableJsonRpc payload) {
        return null;
    }
    
    private final com.walletconnect.walletconnectv2.core.model.vo.EncryptionPayloadVO toEncryptionPayload(java.lang.String message) {
        return null;
    }
    
    private final java.lang.String encode(java.lang.String $this$encode) {
        return null;
    }
}