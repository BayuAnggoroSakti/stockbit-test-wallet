package com.walletconnect.walletconnectv2.storage.history.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007\u00a8\u0006\b"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "", "(Ljava/lang/String;I)V", "PENDING", "REQUEST_SUCCESS", "RESPOND_SUCCESS", "REQUEST_FAILURE", "RESPOND_FAILURE", "walletconnectv2_debug"})
public enum JsonRpcStatus {
    /*public static final*/ PENDING /* = new PENDING() */,
    /*public static final*/ REQUEST_SUCCESS /* = new REQUEST_SUCCESS() */,
    /*public static final*/ RESPOND_SUCCESS /* = new RESPOND_SUCCESS() */,
    /*public static final*/ REQUEST_FAILURE /* = new REQUEST_FAILURE() */,
    /*public static final*/ RESPOND_FAILURE /* = new RESPOND_FAILURE() */;
    
    JsonRpcStatus() {
    }
}