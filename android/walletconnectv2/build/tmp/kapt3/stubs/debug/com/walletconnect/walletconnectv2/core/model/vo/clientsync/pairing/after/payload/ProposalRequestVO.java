package com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.payload;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/after/payload/ProposalRequestVO;", "", "method", "", "params", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$ProposalParams;", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$ProposalParams;)V", "getMethod", "()Ljava/lang/String;", "getParams", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$ProposalParams;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ProposalRequestVO {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String method = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.ProposalParams params = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.payload.ProposalRequestVO copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "method")
    java.lang.String method, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "params")
    com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.ProposalParams params) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public ProposalRequestVO(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "method")
    java.lang.String method, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "params")
    com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.ProposalParams params) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMethod() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.ProposalParams component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.ProposalParams getParams() {
        return null;
    }
}