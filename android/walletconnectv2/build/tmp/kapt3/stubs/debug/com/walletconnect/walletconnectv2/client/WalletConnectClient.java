package com.walletconnect.walletconnectv2.client;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00e8\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002LMB\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bJ&\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\f\u001a\u00020\u000e2\u0014\b\u0002\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\b0\u0010J\u001a\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u00132\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0015J\u000e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\rJ\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aJ\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001aJ\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001aJ$\u0010 \u001a\u00020\b2\u0006\u0010!\u001a\u00020\"2\u0014\b\u0002\u0010#\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\b0\u0010J\u001a\u0010%\u001a\u00020\b2\u0006\u0010%\u001a\u00020&2\n\b\u0002\u0010\'\u001a\u0004\u0018\u00010(J\u001a\u0010)\u001a\u00020\b2\u0006\u0010)\u001a\u00020*2\n\b\u0002\u0010+\u001a\u0004\u0018\u00010,J\u001a\u0010-\u001a\u00020\b2\u0006\u0010-\u001a\u00020.2\n\b\u0002\u0010/\u001a\u0004\u0018\u000100J\u001a\u00101\u001a\u00020\b2\u0006\u00101\u001a\u0002022\n\b\u0002\u00103\u001a\u0004\u0018\u000104J\u001a\u00105\u001a\u00020\b2\u0006\u00105\u001a\u0002062\n\b\u0002\u00107\u001a\u0004\u0018\u000108J\u001a\u00109\u001a\u00020\b2\u0006\u0010:\u001a\u00020;2\n\b\u0002\u0010<\u001a\u0004\u0018\u00010=J\u000e\u0010>\u001a\u00020\b2\u0006\u0010?\u001a\u00020@J\u000e\u0010A\u001a\u00020\b2\u0006\u0010?\u001a\u00020BJ\u0006\u0010C\u001a\u00020\bJ\u001a\u0010D\u001a\u00020\b2\u0006\u0010D\u001a\u00020E2\n\b\u0002\u0010F\u001a\u0004\u0018\u00010GJ\u001a\u0010H\u001a\u00020\b2\u0006\u0010H\u001a\u00020I2\n\b\u0002\u0010J\u001a\u0004\u0018\u00010KR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006N"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnectClient;", "", "()V", "engineInteractor", "Lcom/walletconnect/walletconnectv2/engine/domain/EngineInteractor;", "wcKoinApp", "Lorg/koin/core/KoinApplication;", "approve", "", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Approve;", "sessionApprove", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionApprove;", "connect", "", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Connect;", "onFailure", "Lkotlin/Function1;", "", "disconnect", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Disconnect;", "listener", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionDelete;", "getJsonRpcHistory", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcHistory;", "topic", "getListOfPendingSession", "", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionProposal;", "getListOfSettledPairings", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledPairing;", "getListOfSettledSessions", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession;", "initialize", "initial", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Init;", "onError", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "notify", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Notify;", "notificationListener", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$Notification;", "pair", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Pair;", "pairing", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$Pairing;", "ping", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Ping;", "sessionPing", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionPing;", "reject", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Reject;", "sessionReject", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionReject;", "request", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Request;", "sessionRequest", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionRequest;", "respond", "response", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Response;", "sessionPayload", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionPayload;", "setDappDelegate", "delegate", "Lcom/walletconnect/walletconnectv2/client/WalletConnectClient$DappDelegate;", "setWalletDelegate", "Lcom/walletconnect/walletconnectv2/client/WalletConnectClient$WalletDelegate;", "shutdown", "update", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Update;", "sessionUpdate", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionUpdate;", "upgrade", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Upgrade;", "sessionUpgrade", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionUpgrade;", "DappDelegate", "WalletDelegate", "walletconnectv2_debug"})
public final class WalletConnectClient {
    @org.jetbrains.annotations.NotNull()
    public static final com.walletconnect.walletconnectv2.client.WalletConnectClient INSTANCE = null;
    private static final org.koin.core.KoinApplication wcKoinApp = null;
    private static com.walletconnect.walletconnectv2.engine.domain.EngineInteractor engineInteractor;
    
    private WalletConnectClient() {
        super();
    }
    
    public final void initialize(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Init initial, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException, kotlin.Unit> onError) {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class})
    public final void setWalletDelegate(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnectClient.WalletDelegate delegate) throws java.lang.IllegalStateException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class})
    public final void setDappDelegate(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnectClient.DappDelegate delegate) throws java.lang.IllegalStateException {
    }
    
    @org.jetbrains.annotations.Nullable()
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final java.lang.String connect(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Connect connect, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
        return null;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final void pair(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Pair pair, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.Pairing pairing) throws java.lang.IllegalStateException, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class})
    public final void approve(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Approve approve, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.SessionApprove sessionApprove) throws java.lang.IllegalStateException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class})
    public final void reject(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Reject reject, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.SessionReject sessionReject) throws java.lang.IllegalStateException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final void respond(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Response response, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.SessionPayload sessionPayload) throws java.lang.IllegalStateException, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final void request(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Request request, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.SessionRequest sessionRequest) throws java.lang.IllegalStateException, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final void upgrade(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Upgrade upgrade, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.SessionUpgrade sessionUpgrade) throws java.lang.IllegalStateException, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final void update(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Update update, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.SessionUpdate sessionUpdate) throws java.lang.IllegalStateException, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final void ping(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Ping ping, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.SessionPing sessionPing) throws java.lang.IllegalStateException, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final void notify(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Notify notify, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.Notification notificationListener) throws java.lang.IllegalStateException, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException.class})
    public final void disconnect(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.client.WalletConnect.Params.Disconnect disconnect, @org.jetbrains.annotations.Nullable()
    com.walletconnect.walletconnectv2.client.WalletConnect.Listeners.SessionDelete listener) throws java.lang.IllegalStateException, com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException {
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class})
    public final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession> getListOfSettledSessions() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class})
    public final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionProposal> getListOfPendingSession() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class})
    public final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledPairing> getListOfSettledPairings() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.IllegalStateException.class})
    public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory getJsonRpcHistory(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) throws java.lang.IllegalStateException {
        return null;
    }
    
    public final void shutdown() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000eH&\u00a8\u0006\u000f"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnectClient$WalletDelegate;", "", "onSessionDelete", "", "deletedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$DeletedSession;", "onSessionNotification", "sessionNotification", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionNotification;", "onSessionProposal", "sessionProposal", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionProposal;", "onSessionRequest", "sessionRequest", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionRequest;", "walletconnectv2_debug"})
    public static abstract interface WalletDelegate {
        
        public abstract void onSessionProposal(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionProposal sessionProposal);
        
        public abstract void onSessionRequest(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionRequest sessionRequest);
        
        public abstract void onSessionDelete(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.DeletedSession deletedSession);
        
        public abstract void onSessionNotification(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionNotification sessionNotification);
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0005H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\rH&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0010\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0016H&\u00a8\u0006\u0017"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnectClient$DappDelegate;", "", "onPairingSettled", "", "settledPairing", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledPairing;", "onPairingUpdated", "pairing", "onSessionApproved", "approvedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$ApprovedSession;", "onSessionDelete", "deletedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$DeletedSession;", "onSessionRejected", "rejectedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$RejectedSession;", "onSessionUpdate", "updatedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$UpdatedSession;", "onSessionUpgrade", "upgradedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$UpgradedSession;", "walletconnectv2_debug"})
    public static abstract interface DappDelegate {
        
        public abstract void onPairingSettled(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledPairing settledPairing);
        
        public abstract void onPairingUpdated(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledPairing pairing);
        
        public abstract void onSessionApproved(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.ApprovedSession approvedSession);
        
        public abstract void onSessionRejected(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.RejectedSession rejectedSession);
        
        public abstract void onSessionUpdate(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.UpdatedSession updatedSession);
        
        public abstract void onSessionUpgrade(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.UpgradedSession upgradedSession);
        
        public abstract void onSessionDelete(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.client.WalletConnect.Model.DeletedSession deletedSession);
    }
}