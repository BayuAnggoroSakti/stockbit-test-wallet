package com.walletconnect.walletconnectv2.walletconnectv2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0004BCDEB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00190\n2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u001a0\nH\u0016J\u00bb\u0003\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u001b0\n\"\b\b\u0000\u0010\u001b*\u00020\u001c2\u00a0\u0003\u0010\u001d\u001a\u009b\u0003\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\u0017\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00180!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\"\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00180!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(#\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0018\u0018\u00010!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b($\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(%\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0018\u0018\u00010!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(&\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\'\u0012\u0013\u0012\u00110(\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b()\u0012\u0013\u0012\u00110*\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(+\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(,\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(-\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(.\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0018\u0018\u00010!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(/\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(0\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(1\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(2\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(3\u0012\u0004\u0012\u0002H\u001b0\u001eH\u0016J\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002040\n2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016Jd\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u001b0\n\"\b\b\u0000\u0010\u001b*\u00020\u001c2\u0006\u0010\u0017\u001a\u00020\u00182B\u0010\u001d\u001a>\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00180!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\"\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00180!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(#\u0012\u0004\u0012\u0002H\u001b05H\u0016J\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002060\n2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u00c3\u0003\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\u001b0\n\"\b\b\u0000\u0010\u001b*\u00020\u001c2\u0006\u0010\u0017\u001a\u00020\u00182\u00a0\u0003\u0010\u001d\u001a\u009b\u0003\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\u0017\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00180!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\"\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00180!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(#\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0018\u0018\u00010!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b($\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(%\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0018\u0018\u00010!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(&\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\'\u0012\u0013\u0012\u00110(\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b()\u0012\u0013\u0012\u00110*\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(+\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(,\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(-\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(.\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0018\u0018\u00010!\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(/\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(0\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(1\u0012\u0015\u0012\u0013\u0018\u00010\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(2\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(3\u0012\u0004\u0012\u0002H\u001b0\u001eH\u0016J\u0016\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00180\n2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J{\u00107\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00180!2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00180!2\u000e\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010!2\u0006\u00100\u001a\u00020\u00182\u0006\u0010%\u001a\u00020\u00192\u0006\u0010\'\u001a\u00020\u00192\u0006\u0010)\u001a\u00020(2\u0006\u0010+\u001a\u00020*2\b\u00108\u001a\u0004\u0018\u00010\u00192\u0006\u00103\u001a\u00020\u0018H\u0016\u00a2\u0006\u0002\u00109J\u0018\u0010:\u001a\u00020\u00162\u0006\u0010)\u001a\u00020(2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u009f\u0001\u0010;\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u000e\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010!2\u0006\u0010\'\u001a\u00020\u00192\u0006\u0010)\u001a\u00020(2\u0006\u00100\u001a\u00020\u00182\b\u00102\u001a\u0004\u0018\u00010\u00182\b\u00101\u001a\u0004\u0018\u00010\u00182\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00180!2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00180!2\u000e\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010!2\u0006\u0010%\u001a\u00020\u00192\u0006\u00103\u001a\u00020\u00182\b\u00108\u001a\u0004\u0018\u00010\u00192\u0006\u0010<\u001a\u00020\u0018H\u0016\u00a2\u0006\u0002\u0010=J\u0018\u0010>\u001a\u00020\u00162\u0006\u0010)\u001a\u00020(2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0088\u0001\u0010?\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u000e\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010!2\u0006\u0010\'\u001a\u00020\u00192\u0006\u0010)\u001a\u00020(2\u0006\u00100\u001a\u00020\u00182\b\u00102\u001a\u0004\u0018\u00010\u00182\b\u00101\u001a\u0004\u0018\u00010\u00182\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00180!2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00180!2\u000e\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010!2\u0006\u0010%\u001a\u00020\u00192\u0006\u0010<\u001a\u00020\u0018H\u0016J \u0010@\u001a\u00020\u00162\u000e\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010!2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J,\u0010A\u001a\u00020\u00162\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00180!2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00180!2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\r\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u001e\u0010\u000f\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u001e\u0010\u0011\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\fR\u001e\u0010\u0013\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\f\u00a8\u0006F"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl;", "Lcom/squareup/sqldelight/TransacterImpl;", "Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDaoQueries;", "database", "Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;", "driver", "Lcom/squareup/sqldelight/db/SqlDriver;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;Lcom/squareup/sqldelight/db/SqlDriver;)V", "getExpiry", "", "Lcom/squareup/sqldelight/Query;", "getGetExpiry$walletconnectv2_debug", "()Ljava/util/List;", "getListOfSessionDaos", "getGetListOfSessionDaos$walletconnectv2_debug", "getPermissionsByTopic", "getGetPermissionsByTopic$walletconnectv2_debug", "getSessionByTopic", "getGetSessionByTopic$walletconnectv2_debug", "hasTopic", "getHasTopic$walletconnectv2_debug", "deleteSession", "", "topic", "", "", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetListOfSessionDaos;", "T", "", "mapper", "Lkotlin/Function17;", "Lkotlin/ParameterName;", "name", "", "permissions_chains", "permissions_methods", "permissions_types", "ttl_seconds", "accounts", "expiry", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "status", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "controller_type", "_name", "description", "url", "icons", "self_participant", "peer_participant", "controller_key", "relay_protocol", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetPermissionsByTopic;", "Lkotlin/Function2;", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetSessionByTopic;", "insertSession", "metadata_id", "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;JJLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Ljava/lang/Long;Ljava/lang/String;)V", "updatePreSettledSessionToAcknowledged", "updateProposedSessionToAcknowledged", "topic_", "(Ljava/lang/String;Ljava/util/List;JLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V", "updateProposedSessionToResponded", "updateRespondedSessionToPresettled", "updateSessionWithAccounts", "updateSessionWithPermissions", "GetExpiryQuery", "GetPermissionsByTopicQuery", "GetSessionByTopicQuery", "HasTopicQuery", "walletconnectv2_debug"})
final class SessionDaoQueriesImpl extends com.squareup.sqldelight.TransacterImpl implements com.walletconnect.walletconnectv2.storage.data.dao.SessionDaoQueries {
    private final com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl database = null;
    private final com.squareup.sqldelight.db.SqlDriver driver = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getListOfSessionDaos = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getPermissionsByTopic = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getSessionByTopic = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> hasTopic = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getExpiry = null;
    
    public SessionDaoQueriesImpl(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl database, @org.jetbrains.annotations.NotNull()
    com.squareup.sqldelight.db.SqlDriver driver) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetListOfSessionDaos$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetPermissionsByTopic$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetSessionByTopic$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getHasTopic$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetExpiry$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getListOfSessionDaos(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function17<? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? super java.lang.Long, ? super java.util.List<java.lang.String>, ? super java.lang.Long, ? super com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? extends T> mapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetListOfSessionDaos> getListOfSessionDaos() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getPermissionsByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? extends T> mapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetPermissionsByTopic> getPermissionsByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getSessionByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function17<? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? super java.lang.Long, ? super java.util.List<java.lang.String>, ? super java.lang.Long, ? super com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? extends T> mapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetSessionByTopic> getSessionByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<java.lang.String> hasTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<java.lang.Long> getExpiry(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
        return null;
    }
    
    @java.lang.Override()
    public void insertSession(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions_types, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, long ttl_seconds, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol) {
    }
    
    @java.lang.Override()
    public void updateProposedSessionToResponded(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @java.lang.Override()
    public void updateRespondedSessionToPresettled(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> accounts, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions_types, long ttl_seconds, @org.jetbrains.annotations.NotNull()
    java.lang.String topic_) {
    }
    
    @java.lang.Override()
    public void updatePreSettledSessionToAcknowledged(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @java.lang.Override()
    public void updateProposedSessionToAcknowledged(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> accounts, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions_types, long ttl_seconds, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic_) {
    }
    
    @java.lang.Override()
    public void updateSessionWithPermissions(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @java.lang.Override()
    public void updateSessionWithAccounts(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @java.lang.Override()
    public void deleteSession(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\bH\u0016J\b\u0010\r\u001a\u00020\u0005H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl$GetPermissionsByTopicQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class GetPermissionsByTopicQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        
        public GetPermissionsByTopicQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\bH\u0016J\b\u0010\r\u001a\u00020\u0005H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl$GetSessionByTopicQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class GetSessionByTopicQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        
        public GetSessionByTopicQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\bH\u0016J\b\u0010\r\u001a\u00020\u0005H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl$HasTopicQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class HasTopicQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        
        public HasTopicQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\bH\u0016J\b\u0010\r\u001a\u00020\u0005H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl$GetExpiryQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class GetExpiryQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        
        public GetExpiryQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
}