package com.walletconnect.walletconnectv2.engine.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00b4\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 \u0089\u00012\u00020\u0001:\u0002\u0089\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ=\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0002\b\u001fJ\u001e\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\u000b2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00170#H\u0002J\b\u0010$\u001a\u00020\u0017H\u0002J6\u0010%\u001a\u00020\u00172\u0006\u0010&\u001a\u00020\'2\u0018\u0010(\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020*0)\u0012\u0004\u0012\u00020\u00170\u001bH\u0082@\u00f8\u0001\u0000\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010+JY\u0010,\u001a\u00020\u00172\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020.2\u0006\u00100\u001a\u0002012\u001e\u0010\u001a\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020.02\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0002\b3J\b\u00104\u001a\u000205H\u0002J-\u00106\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020807\u0012\n\u0012\b\u0012\u0004\u0012\u00020807022\u0006\u0010-\u001a\u000205H\u0000\u00a2\u0006\u0002\b9J\u0013\u0010:\u001a\b\u0012\u0004\u0012\u00020\u001907H\u0000\u00a2\u0006\u0002\b;J\u0013\u0010<\u001a\b\u0012\u0004\u0012\u00020=07H\u0000\u00a2\u0006\u0002\b>J\u0013\u0010?\u001a\b\u0012\u0004\u0012\u00020\u001c07H\u0000\u00a2\u0006\u0002\b@J\u001a\u0010A\u001a\u00020\u00172\u0012\u0010B\u001a\u000e\u0012\u0004\u0012\u00020C\u0012\u0004\u0012\u00020\u00170\u001bJ\u0010\u0010D\u001a\u00020E2\u0006\u0010-\u001a\u000205H\u0002JE\u0010F\u001a\u00020\u00172\u0006\u0010-\u001a\u00020.2\u0006\u0010G\u001a\u00020H2\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0002\bIJ\u0018\u0010J\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020NH\u0002J\u0018\u0010O\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020PH\u0002J\u0018\u0010Q\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010R\u001a\u00020SH\u0002J,\u0010T\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020U2\u0006\u0010V\u001a\u00020W2\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020\u00170\u001bH\u0002J\u0018\u0010X\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020YH\u0002J\u0010\u0010Z\u001a\u00020\u00172\u0006\u0010K\u001a\u00020LH\u0002J\u0018\u0010[\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020\\H\u0002J\u0018\u0010]\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020^H\u0002J\u0018\u0010_\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020`H\u0002J\u0018\u0010a\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020bH\u0002J\u0018\u0010c\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020dH\u0002J\u0018\u0010e\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020fH\u0002J\u0018\u0010g\u001a\u00020\u00172\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020hH\u0002J=\u0010i\u001a\u00020\u00172\u0006\u0010j\u001a\u00020.2\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0002\bkJ\u0010\u0010l\u001a\u00020\u00172\u0006\u0010m\u001a\u00020WH\u0002J=\u0010n\u001a\u00020\u00172\u0006\u0010-\u001a\u00020.2\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0002\boJ\u0010\u0010p\u001a\u00020.2\u0006\u0010q\u001a\u00020\u0015H\u0002J5\u0010r\u001a\u0004\u0018\u00010.2\u0006\u0010q\u001a\u00020\u00152\b\u0010s\u001a\u0004\u0018\u00010.2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0002\btJ.\u0010u\u001a\u00020\u00172\u0006\u0010q\u001a\u00020\u00152\u0006\u0010s\u001a\u00020.2\u0014\b\u0002\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0002JQ\u0010v\u001a\u00020\u00172\u0006\u0010/\u001a\u00020.2\u0006\u0010-\u001a\u00020.2\u001e\u0010\u001a\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020.02\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0002\bwJ1\u0010x\u001a\u00020\u00172\u0006\u0010-\u001a\u00020.2\u0006\u0010y\u001a\u00020z2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0002\b{J\b\u0010|\u001a\u00020\u0017H\u0002J\b\u0010}\u001a\u00020\u0017H\u0002J?\u0010~\u001a\u00020\u00172\u0006\u0010K\u001a\u00020\u007f2\u0013\u0010\u001a\u001a\u000f\u0012\u0005\u0012\u00030\u0080\u0001\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0003\b\u0081\u0001J\t\u0010\u0082\u0001\u001a\u00020\u0017H\u0002J[\u0010\u0083\u0001\u001a\u00020\u00172\u0006\u0010-\u001a\u00020.2\b\u0010\u0084\u0001\u001a\u00030\u0085\u00012$\u0010\u001a\u001a \u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020.\u0012\n\u0012\b\u0012\u0004\u0012\u00020.0702\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0003\b\u0086\u0001JS\u0010\u0087\u0001\u001a\u00020\u00172\u0006\u0010-\u001a\u00020.2\u0006\u0010q\u001a\u00020\u00152\u001e\u0010\u001a\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020\u001502\u0012\u0004\u0012\u00020\u00170\u001b2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00170\u001bH\u0000\u00a2\u0006\u0003\b\u0088\u0001R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u008a\u0001"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/domain/EngineInteractor;", "", "relayer", "Lcom/walletconnect/walletconnectv2/relay/domain/WalletConnectRelayer;", "crypto", "Lcom/walletconnect/walletconnectv2/crypto/CryptoRepository;", "sequenceStorageRepository", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStorageRepository;", "metaData", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "controllerType", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "(Lcom/walletconnect/walletconnectv2/relay/domain/WalletConnectRelayer;Lcom/walletconnect/walletconnectv2/crypto/CryptoRepository;Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStorageRepository;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;)V", "_sequenceEvent", "Lkotlinx/coroutines/flow/MutableStateFlow;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "sequenceEvent", "Lkotlinx/coroutines/flow/StateFlow;", "getSequenceEvent", "()Lkotlinx/coroutines/flow/StateFlow;", "sessionPermissions", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionPermissions;", "approve", "", "proposal", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionProposal;", "onSuccess", "Lkotlin/Function1;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession;", "onFailure", "", "approve$walletconnectv2_debug", "checkPeer", "requiredPeer", "onUnauthorizedPeer", "Lkotlin/Function0;", "collectClientSyncJsonRpc", "collectResponse", "id", "", "onResponse", "Lkotlin/Result;", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$JsonRpcResult;", "(JLkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "disconnect", "topic", "", "reason", "code", "", "Lkotlin/Pair;", "disconnect$walletconnectv2_debug", "generateTopic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getListOfJsonRpcHistory", "", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcHistoryVO;", "getListOfJsonRpcHistory$walletconnectv2_debug", "getListOfPendingSessions", "getListOfPendingSessions$walletconnectv2_debug", "getListOfSettledPairings", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledPairing;", "getListOfSettledPairings$walletconnectv2_debug", "getListOfSettledSessions", "getListOfSettledSessions$walletconnectv2_debug", "handleInitializationErrors", "onError", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "isSequenceValid", "", "notify", "notification", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notification;", "notify$walletconnectv2_debug", "onPairingApprove", "request", "Lcom/walletconnect/walletconnectv2/core/model/vo/WCRequestVO;", "params", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$ApproveParams;", "onPairingDelete", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$DeleteParams;", "onPairingPayload", "payloadParams", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$PayloadParams;", "onPairingSuccess", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$Proposal;", "pairing", "Lcom/walletconnect/walletconnectv2/core/model/vo/sequence/PairingVO;", "onPairingUpdate", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$UpdateParams;", "onPing", "onSessionApprove", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$ApprovalParams;", "onSessionDelete", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$DeleteParams;", "onSessionNotification", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$NotificationParams;", "onSessionPayload", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$SessionPayloadParams;", "onSessionReject", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$RejectParams;", "onSessionUpdate", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$UpdateParams;", "onSessionUpgrade", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$UpgradeParams;", "pair", "uri", "pair$walletconnectv2_debug", "pairingUpdate", "settledSequence", "ping", "ping$walletconnectv2_debug", "proposePairing", "permissions", "proposeSequence", "pairingTopic", "proposeSequence$walletconnectv2_debug", "proposeSession", "reject", "reject$walletconnectv2_debug", "respondSessionPayload", "jsonRpcResponse", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO;", "respondSessionPayload$walletconnectv2_debug", "resubscribeToSettledPairings", "resubscribeToSettledSession", "sessionRequest", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Request;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$JsonRpcResult;", "sessionRequest$walletconnectv2_debug", "setupSequenceExpiration", "update", "state", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionState;", "update$walletconnectv2_debug", "upgrade", "upgrade$walletconnectv2_debug", "Companion", "walletconnectv2_debug"})
public final class EngineInteractor {
    private final com.walletconnect.walletconnectv2.relay.domain.WalletConnectRelayer relayer = null;
    private final com.walletconnect.walletconnectv2.crypto.CryptoRepository crypto = null;
    private final com.walletconnect.walletconnectv2.storage.sequence.SequenceStorageRepository sequenceStorageRepository = null;
    private final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData metaData = null;
    private com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType;
    private com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions sessionPermissions;
    private final kotlinx.coroutines.flow.MutableStateFlow<com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle> _sequenceEvent = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.StateFlow<com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle> sequenceEvent = null;
    @org.jetbrains.annotations.NotNull()
    private static final com.walletconnect.walletconnectv2.engine.domain.EngineInteractor.Companion Companion = null;
    @java.lang.Deprecated()
    public static final long THIRTY_SECONDS_TIMEOUT = 30000L;
    @java.lang.Deprecated()
    public static final long FIVE_MINUTES_TIMEOUT = 300000L;
    @java.lang.Deprecated()
    public static final boolean prompt = true;
    
    public EngineInteractor(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.relay.domain.WalletConnectRelayer relayer, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.crypto.CryptoRepository crypto, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStorageRepository sequenceStorageRepository, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData metaData, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.StateFlow<com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle> getSequenceEvent() {
        return null;
    }
    
    public final void handleInitializationErrors(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException, kotlin.Unit> onError) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String proposeSequence$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions permissions, @org.jetbrains.annotations.Nullable()
    java.lang.String pairingTopic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
        return null;
    }
    
    private final void proposeSession(com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions permissions, java.lang.String pairingTopic, kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    private final java.lang.String proposePairing(com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions permissions) {
        return null;
    }
    
    public final void pair$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String uri, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    private final void onPairingSuccess(com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.Proposal proposal, com.walletconnect.walletconnectv2.core.model.vo.sequence.PairingVO pairing, kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onSuccess) {
    }
    
    private final void pairingUpdate(com.walletconnect.walletconnectv2.core.model.vo.sequence.PairingVO settledSequence) {
    }
    
    public final void approve$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionProposal proposal, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void reject$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String reason, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Pair<java.lang.String, java.lang.String>, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void upgrade$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions permissions, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Pair<java.lang.String, com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions>, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void update$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionState state, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Pair<java.lang.String, ? extends java.util.List<java.lang.String>>, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void sessionRequest$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.Request request, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.JsonRpcResult, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void respondSessionPayload$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO jsonRpcResponse, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void ping$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void notify$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.Notification notification, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void disconnect$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.lang.String reason, int code, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Pair<java.lang.String, java.lang.String>, kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    private final java.lang.Object collectResponse(long id, kotlin.jvm.functions.Function1<? super kotlin.Result<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.JsonRpcResult>, kotlin.Unit> onResponse, kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionProposal> getListOfPendingSessions$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession> getListOfSettledSessions$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledPairing> getListOfSettledPairings$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.Pair<java.util.List<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcHistoryVO>, java.util.List<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcHistoryVO>> getListOfJsonRpcHistory$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
        return null;
    }
    
    private final void collectClientSyncJsonRpc() {
    }
    
    private final void onPairingPayload(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.PayloadParams payloadParams) {
    }
    
    private final void onPairingApprove(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.ApproveParams params) {
    }
    
    private final void onPairingDelete(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.DeleteParams params) {
    }
    
    private final void onPairingUpdate(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.UpdateParams params) {
    }
    
    private final void onSessionApprove(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.ApprovalParams params) {
    }
    
    private final void onSessionReject(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.RejectParams params) {
    }
    
    private final void onSessionDelete(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.DeleteParams params) {
    }
    
    private final void onSessionPayload(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.SessionPayloadParams params) {
    }
    
    private final void onSessionUpdate(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.UpdateParams params) {
    }
    
    private final void onSessionUpgrade(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.UpgradeParams params) {
    }
    
    private final void onSessionNotification(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.NotificationParams params) {
    }
    
    private final void onPing(com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request) {
    }
    
    private final void resubscribeToSettledPairings() {
    }
    
    private final void resubscribeToSettledSession() {
    }
    
    private final void checkPeer(com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType requiredPeer, kotlin.jvm.functions.Function0<kotlin.Unit> onUnauthorizedPeer) {
    }
    
    private final boolean isSequenceValid(com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
        return false;
    }
    
    private final void setupSequenceExpiration() {
    }
    
    private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO generateTopic() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\b\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/domain/EngineInteractor$Companion;", "", "()V", "FIVE_MINUTES_TIMEOUT", "", "THIRTY_SECONDS_TIMEOUT", "prompt", "", "walletconnectv2_debug"})
    static final class Companion {
        
        private Companion() {
            super();
        }
    }
}