package com.walletconnect.walletconnectv2.crypto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b`\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u0003H&\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0004\b\u0004\u0010\u0005J1\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u00072\u0006\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0003H&\u00f8\u0001\u0000\u00f8\u0001\u0002\u00a2\u0006\u0004\b\f\u0010\rJ\u001c\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u000f0\u00072\u0006\u0010\u0010\u001a\u00020\tH&J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H&J-\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\tH&\u00f8\u0001\u0000\u00f8\u0001\u0002\u00a2\u0006\u0004\b\u0018\u0010\u0019\u0082\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b\u00a1\u001e0\u0001\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/crypto/CryptoRepository;", "", "generateKeyPair", "Lcom/walletconnect/walletconnectv2/core/model/vo/PublicKey;", "generateKeyPair-oW5vskc", "()Ljava/lang/String;", "generateTopicAndSharedKey", "Lkotlin/Pair;", "Lcom/walletconnect/walletconnectv2/core/model/vo/SharedKey;", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "self", "peer", "generateTopicAndSharedKey-xakSQ8w", "(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Pair;", "getKeyAgreement", "Lcom/walletconnect/walletconnectv2/core/model/vo/Key;", "topic", "removeKeys", "", "tag", "", "setEncryptionKeys", "sharedKey", "publicKey", "setEncryptionKeys-SxomFNM", "(Ljava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;)V", "walletconnectv2_debug"})
public abstract interface CryptoRepository {
    
    @org.jetbrains.annotations.NotNull()
    public abstract kotlin.Pair<com.walletconnect.walletconnectv2.core.model.vo.Key, com.walletconnect.walletconnectv2.core.model.vo.Key> getKeyAgreement(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic);
    
    public abstract void removeKeys(@org.jetbrains.annotations.NotNull()
    java.lang.String tag);
}