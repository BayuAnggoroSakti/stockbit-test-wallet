package com.walletconnect.walletconnectv2.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\t\b\u0080\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t\u00a8\u0006\n"}, d2 = {"Lcom/walletconnect/walletconnectv2/di/DITags;", "", "(Ljava/lang/String;I)V", "KEY_STORE", "RPC_STORE", "RPC_STORE_ALIAS", "DB_ALIAS", "DB_KEY_STORAGE", "DB_SECRET_KEY", "DB_PASSPHRASE", "walletconnectv2_debug"})
public enum DITags {
    @kotlin.jvm.JvmSynthetic()
    /*public static final*/ KEY_STORE /* = new KEY_STORE() */,
    @kotlin.jvm.JvmSynthetic()
    /*public static final*/ RPC_STORE /* = new RPC_STORE() */,
    @kotlin.jvm.JvmSynthetic()
    /*public static final*/ RPC_STORE_ALIAS /* = new RPC_STORE_ALIAS() */,
    @kotlin.jvm.JvmSynthetic()
    /*public static final*/ DB_ALIAS /* = new DB_ALIAS() */,
    @kotlin.jvm.JvmSynthetic()
    /*public static final*/ DB_KEY_STORAGE /* = new DB_KEY_STORAGE() */,
    @kotlin.jvm.JvmSynthetic()
    /*public static final*/ DB_SECRET_KEY /* = new DB_SECRET_KEY() */,
    @kotlin.jvm.JvmSynthetic()
    /*public static final*/ DB_PASSPHRASE /* = new DB_PASSPHRASE() */;
    
    DITags() {
    }
}