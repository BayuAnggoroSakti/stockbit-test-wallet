package com.walletconnect.walletconnectv2.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u001a\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0001H\u0002\u001a\b\u0010\u0006\u001a\u00020\u0007H\u0001\u00a8\u0006\b"}, d2 = {"decode", "", "encodedData", "", "encode", "decodedData", "storageModule", "Lorg/koin/core/module/Module;", "walletconnectv2_debug"})
public final class StorageModuleKt {
    
    private static final java.lang.String encode(byte[] decodedData) {
        return null;
    }
    
    private static final byte[] decode(java.lang.String encodedData) {
        return null;
    }
}