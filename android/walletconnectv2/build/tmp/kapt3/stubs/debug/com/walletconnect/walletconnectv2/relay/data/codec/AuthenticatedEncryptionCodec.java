package com.walletconnect.walletconnectv2.relay.data.codec;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0005\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0006H\u0002J%\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016\u00f8\u0001\u0000\u00f8\u0001\u0001\u00a2\u0006\u0004\b\u000f\u0010\u0010J-\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\u0013H\u0016\u00f8\u0001\u0000\u00f8\u0001\u0001\u00a2\u0006\u0004\b\u0014\u0010\u0015J!\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00172\u0006\u0010\r\u001a\u00020\u0004H\u0000\u00a2\u0006\u0002\b\u0018\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b\u00a1\u001e0\u0001\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/relay/data/codec/AuthenticatedEncryptionCodec;", "Lcom/walletconnect/walletconnectv2/relay/Codec;", "()V", "computeHmac", "", "data", "", "iv", "authKey", "publicKey", "decrypt", "payload", "Lcom/walletconnect/walletconnectv2/core/model/vo/EncryptionPayloadVO;", "sharedKey", "Lcom/walletconnect/walletconnectv2/core/model/vo/SharedKey;", "decrypt-5kll5jI", "(Lcom/walletconnect/walletconnectv2/core/model/vo/EncryptionPayloadVO;Ljava/lang/String;)Ljava/lang/String;", "encrypt", "message", "Lcom/walletconnect/walletconnectv2/core/model/vo/PublicKey;", "encrypt-WkEBB2s", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", "getKeys", "Lkotlin/Pair;", "getKeys$walletconnectv2_debug", "Companion", "walletconnectv2_debug"})
public final class AuthenticatedEncryptionCodec implements com.walletconnect.walletconnectv2.relay.Codec {
    @org.jetbrains.annotations.NotNull()
    public static final com.walletconnect.walletconnectv2.relay.data.codec.AuthenticatedEncryptionCodec.Companion Companion = null;
    private static final java.lang.String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final java.lang.String MAC_ALGORITHM = "HmacSHA256";
    private static final java.lang.String HASH_ALGORITHM = "SHA-512";
    private static final java.lang.String AES_ALGORITHM = "AES";
    
    public AuthenticatedEncryptionCodec() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.Pair<byte[], byte[]> getKeys$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String sharedKey) {
        return null;
    }
    
    private final java.lang.String computeHmac(byte[] data, byte[] iv, byte[] authKey, byte[] publicKey) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/walletconnect/walletconnectv2/relay/data/codec/AuthenticatedEncryptionCodec$Companion;", "", "()V", "AES_ALGORITHM", "", "CIPHER_ALGORITHM", "HASH_ALGORITHM", "MAC_ALGORITHM", "walletconnectv2_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}