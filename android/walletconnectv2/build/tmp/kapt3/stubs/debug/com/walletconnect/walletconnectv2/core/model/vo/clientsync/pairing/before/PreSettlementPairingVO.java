package com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0011\u0012B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0003R\u0012\u0010\u0004\u001a\u00020\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0012\u0010\b\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0012\u0010\f\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000bR\u0012\u0010\u000e\u001a\u00020\u0002X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010\u0082\u0001\u0002\u0013\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/PreSettlementPairingVO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SettlementSequence;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "()V", "id", "", "getId", "()J", "jsonrpc", "", "getJsonrpc", "()Ljava/lang/String;", "method", "getMethod", "params", "getParams", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "Approve", "Reject", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/PreSettlementPairingVO$Approve;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/PreSettlementPairingVO$Reject;", "walletconnectv2_debug"})
public abstract class PreSettlementPairingVO implements com.walletconnect.walletconnectv2.core.model.type.SettlementSequence<com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO> {
    
    private PreSettlementPairingVO() {
        super();
    }
    
    @java.lang.Override()
    public abstract long getId();
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public abstract java.lang.String getMethod();
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public abstract java.lang.String getJsonrpc();
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public abstract com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO getParams();
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B-\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\bH\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00052\b\b\u0003\u0010\u0007\u001a\u00020\bH\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u0006\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/PreSettlementPairingVO$Approve;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/PreSettlementPairingVO;", "id", "", "jsonrpc", "", "method", "params", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$ApproveParams;", "(JLjava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$ApproveParams;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getMethod", "getParams", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$ApproveParams;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class Approve extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.PreSettlementPairingVO {
        private final long id = 0L;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String jsonrpc = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String method = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.ApproveParams params = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.PreSettlementPairingVO.Approve copy(@com.squareup.moshi.Json(name = "id")
        long id, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "jsonrpc")
        java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "method")
        java.lang.String method, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "params")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.ApproveParams params) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Approve(@com.squareup.moshi.Json(name = "id")
        long id, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "jsonrpc")
        java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "method")
        java.lang.String method, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "params")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.ApproveParams params) {
            super();
        }
        
        public final long component1() {
            return 0L;
        }
        
        @java.lang.Override()
        public long getId() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String getJsonrpc() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String getMethod() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.ApproveParams component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.ApproveParams getParams() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\bH\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\bH\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u0006\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/PreSettlementPairingVO$Reject;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/PreSettlementPairingVO;", "id", "", "jsonrpc", "", "method", "params", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$RejectParams;", "(JLjava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$RejectParams;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getMethod", "getParams", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$RejectParams;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class Reject extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.PreSettlementPairingVO {
        private final long id = 0L;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String jsonrpc = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String method = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.RejectParams params = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.PreSettlementPairingVO.Reject copy(long id, @org.jetbrains.annotations.NotNull()
        java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
        java.lang.String method, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.RejectParams params) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Reject(long id, @org.jetbrains.annotations.NotNull()
        java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
        java.lang.String method, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.RejectParams params) {
            super();
        }
        
        public final long component1() {
            return 0L;
        }
        
        @java.lang.Override()
        public long getId() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String getJsonrpc() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String getMethod() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.RejectParams component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.RejectParams getParams() {
            return null;
        }
    }
}