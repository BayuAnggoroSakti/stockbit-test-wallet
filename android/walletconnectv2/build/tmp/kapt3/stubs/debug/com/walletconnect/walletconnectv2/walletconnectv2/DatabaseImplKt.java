package com.walletconnect.walletconnectv2.walletconnectv2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u00000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a:\u0010\u0006\u001a\u00020\u0003*\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0000\"\u001e\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u00a8\u0006\u0011"}, d2 = {"schema", "Lcom/squareup/sqldelight/db/SqlDriver$Schema;", "Lkotlin/reflect/KClass;", "Lcom/walletconnect/walletconnectv2/Database;", "getSchema", "(Lkotlin/reflect/KClass;)Lcom/squareup/sqldelight/db/SqlDriver$Schema;", "newInstance", "driver", "Lcom/squareup/sqldelight/db/SqlDriver;", "JsonRpcHistoryDaoAdapter", "Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryDao$Adapter;", "MetaDataDaoAdapter", "Lcom/walletconnect/walletconnectv2/storage/data/dao/MetaDataDao$Adapter;", "PairingDaoAdapter", "Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDao$Adapter;", "SessionDaoAdapter", "Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDao$Adapter;", "walletconnectv2_debug"})
public final class DatabaseImplKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.squareup.sqldelight.db.SqlDriver.Schema getSchema(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<com.walletconnect.walletconnectv2.Database> $this$schema) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.walletconnect.walletconnectv2.Database newInstance(@org.jetbrains.annotations.NotNull()
    kotlin.reflect.KClass<com.walletconnect.walletconnectv2.Database> $this$newInstance, @org.jetbrains.annotations.NotNull()
    com.squareup.sqldelight.db.SqlDriver driver, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.JsonRpcHistoryDao.Adapter JsonRpcHistoryDaoAdapter, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.MetaDataDao.Adapter MetaDataDaoAdapter, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.PairingDao.Adapter PairingDaoAdapter, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.SessionDao.Adapter SessionDaoAdapter) {
        return null;
    }
}