package com.walletconnect.walletconnectv2.storage.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0007H&J\u00e8\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0007\"\b\b\u0000\u0010\u000b*\u00020\f2\u00cd\u0002\u0010\r\u001a\u00c8\u0002\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00110\b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0016\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0017\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0018\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0019\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001a\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001c\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001d\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001e\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001f\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b( \u0012\u0004\u0012\u0002H\u000b0\u000eH&J\u0016\u0010!\u001a\b\u0012\u0004\u0012\u00020\"0\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J\u008e\u0002\u0010!\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0007\"\b\b\u0000\u0010\u000b*\u00020\f2\u0006\u0010\u0004\u001a\u00020\u00052\u00eb\u0001\u0010\r\u001a\u00e6\u0001\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0004\u0012\u0013\u0012\u00110\b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0013\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0016\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0017\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0018\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0019\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001a\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001c\u0012\u0004\u0012\u0002H\u000b0#H&J\u0016\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00050\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J@\u0010%\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u001a\u001a\u00020\u0005H&J\u001f\u0010&\u001a\u00020\u00032\b\u0010\'\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a2\u0006\u0002\u0010(JT\u0010)\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u00052\b\u0010\u0018\u001a\u0004\u0018\u00010\u00052\b\u0010\u0019\u001a\u0004\u0018\u00010\u00052\u000e\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001b2\u0006\u0010*\u001a\u00020\u0005H&J\u0018\u0010+\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0004\u001a\u00020\u0005H&Jk\u0010,\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u00052\b\u0010\u0018\u001a\u0004\u0018\u00010\u00052\b\u0010\u0019\u001a\u0004\u0018\u00010\u00052\u000e\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001b2\u0006\u0010\u001a\u001a\u00020\u00052\b\u0010\'\u001a\u0004\u0018\u00010\b2\u0006\u0010*\u001a\u00020\u0005H&\u00a2\u0006\u0002\u0010-\u00a8\u0006."}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDaoQueries;", "Lcom/squareup/sqldelight/Transacter;", "deletePairing", "", "topic", "", "getExpiry", "Lcom/squareup/sqldelight/Query;", "", "getListOfPairingDaos", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetListOfPairingDaos;", "T", "", "mapper", "Lkotlin/Function14;", "Lkotlin/ParameterName;", "name", "expiry", "uri", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "status", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "controller_type", "self_participant", "peer_participant", "controller_key", "relay_protocol", "", "permissions", "_name", "description", "url", "icons", "getPairingByTopic", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetPairingByTopic;", "Lkotlin/Function10;", "hasTopic", "insertPairing", "updateAcknowledgedPairingMetadata", "metadata_id", "(Ljava/lang/Long;Ljava/lang/String;)V", "updatePendingPairingToPreSettled", "topic_", "updatePreSettledPairingToAcknowledged", "updateProposedPairingToAcknowledged", "(Ljava/lang/String;JLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V", "walletconnectv2_debug"})
public abstract interface PairingDaoQueries extends com.squareup.sqldelight.Transacter {
    
    @org.jetbrains.annotations.NotNull()
    public abstract <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getListOfPairingDaos(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function14<? super java.lang.String, ? super java.lang.Long, ? super java.lang.String, ? super com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? extends T> mapper);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetListOfPairingDaos> getListOfPairingDaos();
    
    @org.jetbrains.annotations.NotNull()
    public abstract <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getPairingByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function10<? super java.lang.String, ? super java.lang.Long, ? super java.lang.String, ? super com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? extends T> mapper);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetPairingByTopic> getPairingByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<java.lang.String> hasTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<java.lang.Long> getExpiry(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    public abstract void insertPairing(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.lang.String uri, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol);
    
    public abstract void updatePendingPairingToPreSettled(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.NotNull()
    java.lang.String topic_);
    
    public abstract void updatePreSettledPairingToAcknowledged(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    public abstract void updateProposedPairingToAcknowledged(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic_);
    
    public abstract void deletePairing(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    public abstract void updateAcknowledgedPairingMetadata(@org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic);
}