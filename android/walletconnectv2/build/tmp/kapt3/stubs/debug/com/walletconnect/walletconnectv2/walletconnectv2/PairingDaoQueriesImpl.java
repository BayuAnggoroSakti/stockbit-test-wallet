package com.walletconnect.walletconnectv2.walletconnectv2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\r\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0003:;<B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00170\n2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00180\nH\u0016J\u00e8\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u00190\n\"\b\b\u0000\u0010\u0019*\u00020\u001a2\u00cd\u0002\u0010\u001b\u001a\u00c8\u0002\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\u0017\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u001f\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b( \u0012\u0013\u0012\u00110!\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\"\u0012\u0013\u0012\u00110#\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b($\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(%\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(&\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\'\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b((\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0016\u0018\u00010)\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(*\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(+\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(,\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(-\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0016\u0018\u00010)\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(.\u0012\u0004\u0012\u0002H\u00190\u001cH\u0016J\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020/0\n2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u008e\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00190\n\"\b\b\u0000\u0010\u0019*\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u00162\u00eb\u0001\u0010\u001b\u001a\u00e6\u0001\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\u0017\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u001f\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b( \u0012\u0013\u0012\u00110!\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\"\u0012\u0013\u0012\u00110#\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b($\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(%\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(&\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\'\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b((\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0016\u0018\u00010)\u00a2\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(*\u0012\u0004\u0012\u0002H\u001900H\u0016J\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00160\n2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J@\u00101\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010 \u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u00172\u0006\u0010\"\u001a\u00020!2\u0006\u0010$\u001a\u00020#2\u0006\u0010%\u001a\u00020\u00162\u0006\u0010(\u001a\u00020\u0016H\u0016J\u001f\u00102\u001a\u00020\u00142\b\u00103\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0015\u001a\u00020\u0016H\u0016\u00a2\u0006\u0002\u00104JT\u00105\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u00172\u0006\u0010\"\u001a\u00020!2\u0006\u0010%\u001a\u00020\u00162\b\u0010&\u001a\u0004\u0018\u00010\u00162\b\u0010\'\u001a\u0004\u0018\u00010\u00162\u000e\u0010*\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010)2\u0006\u00106\u001a\u00020\u0016H\u0016J\u0018\u00107\u001a\u00020\u00142\u0006\u0010\"\u001a\u00020!2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016Jk\u00108\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u00172\u0006\u0010\"\u001a\u00020!2\u0006\u0010%\u001a\u00020\u00162\b\u0010&\u001a\u0004\u0018\u00010\u00162\b\u0010\'\u001a\u0004\u0018\u00010\u00162\u000e\u0010*\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010)2\u0006\u0010(\u001a\u00020\u00162\b\u00103\u001a\u0004\u0018\u00010\u00172\u0006\u00106\u001a\u00020\u0016H\u0016\u00a2\u0006\u0002\u00109R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\r\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u001e\u0010\u000f\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u001e\u0010\u0011\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\f\u00a8\u0006="}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl;", "Lcom/squareup/sqldelight/TransacterImpl;", "Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDaoQueries;", "database", "Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;", "driver", "Lcom/squareup/sqldelight/db/SqlDriver;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;Lcom/squareup/sqldelight/db/SqlDriver;)V", "getExpiry", "", "Lcom/squareup/sqldelight/Query;", "getGetExpiry$walletconnectv2_debug", "()Ljava/util/List;", "getListOfPairingDaos", "getGetListOfPairingDaos$walletconnectv2_debug", "getPairingByTopic", "getGetPairingByTopic$walletconnectv2_debug", "hasTopic", "getHasTopic$walletconnectv2_debug", "deletePairing", "", "topic", "", "", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetListOfPairingDaos;", "T", "", "mapper", "Lkotlin/Function14;", "Lkotlin/ParameterName;", "name", "expiry", "uri", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "status", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "controller_type", "self_participant", "peer_participant", "controller_key", "relay_protocol", "", "permissions", "_name", "description", "url", "icons", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetPairingByTopic;", "Lkotlin/Function10;", "insertPairing", "updateAcknowledgedPairingMetadata", "metadata_id", "(Ljava/lang/Long;Ljava/lang/String;)V", "updatePendingPairingToPreSettled", "topic_", "updatePreSettledPairingToAcknowledged", "updateProposedPairingToAcknowledged", "(Ljava/lang/String;JLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V", "GetExpiryQuery", "GetPairingByTopicQuery", "HasTopicQuery", "walletconnectv2_debug"})
final class PairingDaoQueriesImpl extends com.squareup.sqldelight.TransacterImpl implements com.walletconnect.walletconnectv2.storage.data.dao.PairingDaoQueries {
    private final com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl database = null;
    private final com.squareup.sqldelight.db.SqlDriver driver = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getListOfPairingDaos = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getPairingByTopic = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> hasTopic = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getExpiry = null;
    
    public PairingDaoQueriesImpl(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl database, @org.jetbrains.annotations.NotNull()
    com.squareup.sqldelight.db.SqlDriver driver) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetListOfPairingDaos$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetPairingByTopic$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getHasTopic$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetExpiry$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getListOfPairingDaos(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function14<? super java.lang.String, ? super java.lang.Long, ? super java.lang.String, ? super com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? extends T> mapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetListOfPairingDaos> getListOfPairingDaos() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getPairingByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function10<? super java.lang.String, ? super java.lang.Long, ? super java.lang.String, ? super com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? extends T> mapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetPairingByTopic> getPairingByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<java.lang.String> hasTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<java.lang.Long> getExpiry(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
        return null;
    }
    
    @java.lang.Override()
    public void insertPairing(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.lang.String uri, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol) {
    }
    
    @java.lang.Override()
    public void updatePendingPairingToPreSettled(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.NotNull()
    java.lang.String topic_) {
    }
    
    @java.lang.Override()
    public void updatePreSettledPairingToAcknowledged(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @java.lang.Override()
    public void updateProposedPairingToAcknowledged(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic_) {
    }
    
    @java.lang.Override()
    public void deletePairing(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @java.lang.Override()
    public void updateAcknowledgedPairingMetadata(@org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\bH\u0016J\b\u0010\r\u001a\u00020\u0005H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl$GetPairingByTopicQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class GetPairingByTopicQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        
        public GetPairingByTopicQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\bH\u0016J\b\u0010\r\u001a\u00020\u0005H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl$HasTopicQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class HasTopicQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        
        public HasTopicQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\bH\u0016J\b\u0010\r\u001a\u00020\u0005H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl$GetExpiryQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class GetExpiryQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        
        public GetExpiryQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
}