package com.walletconnect.walletconnectv2.core.model.vo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081@\u0018\u00002\u00020\u0001B\u0012\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00f8\u0001\u0000\u00a2\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u0003H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u0088\u0001\u0002\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000f"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/PublicKey;", "Lcom/walletconnect/walletconnectv2/core/model/vo/Key;", "keyAsHex", "", "constructor-impl", "(Ljava/lang/String;)Ljava/lang/String;", "getKeyAsHex", "()Ljava/lang/String;", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
@kotlin.jvm.JvmInline()
public final class PublicKey implements com.walletconnect.walletconnectv2.core.model.vo.Key {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String keyAsHex = null;
    
    @java.lang.Override()
    public boolean equals(java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getKeyAsHex() {
        return null;
    }
}