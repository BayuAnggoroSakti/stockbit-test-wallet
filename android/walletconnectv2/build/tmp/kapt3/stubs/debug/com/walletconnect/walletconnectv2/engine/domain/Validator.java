package com.walletconnect.walletconnectv2.engine.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001b\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u0000\u00a2\u0006\u0002\b\u000bJ)\u0010\f\u001a\u00020\b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u0000\u00a2\u0006\u0002\b\u000fJ\u0015\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u0012H\u0000\u00a2\u0006\u0002\b\u0013J\u0015\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u0004H\u0000\u00a2\u0006\u0002\b\u0016J\u0015\u0010\u0017\u001a\u00020\b2\u0006\u0010\u0018\u001a\u00020\u0019H\u0000\u00a2\u0006\u0002\b\u001aJ\u0015\u0010\u001b\u001a\u00020\b2\u0006\u0010\u001c\u001a\u00020\u0004H\u0000\u00a2\u0006\u0002\b\u001dJ\u0015\u0010\u001e\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020 H\u0000\u00a2\u0006\u0002\b!J(\u0010\"\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040#2\f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u0002J/\u0010%\u001a\u00020&2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0012\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020&0(H\u0000\u00a2\u0006\u0002\b)J9\u0010*\u001a\u00020&2\b\u0010\u001c\u001a\u0004\u0018\u00010\u00042\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020&0(H\u0000\u00a2\u0006\u0002\b,J=\u0010-\u001a\u00020&2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0012\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020&0(H\u0000\u00a2\u0006\u0002\b.J)\u0010/\u001a\u00020&2\u0006\u0010\u0011\u001a\u0002002\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020&0(H\u0000\u00a2\u0006\u0002\b2J1\u00103\u001a\u00020&2\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\u00042\u0012\u00107\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020&0(H\u0000\u00a2\u0006\u0002\b8J)\u00109\u001a\u00020&2\u0006\u0010:\u001a\u00020;2\u0012\u0010<\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020&0(H\u0000\u00a2\u0006\u0002\b=J)\u0010>\u001a\u00020&2\u0006\u0010?\u001a\u00020@2\u0012\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020&0(H\u0000\u00a2\u0006\u0002\bBJ\u0017\u0010C\u001a\u0004\u0018\u00010D2\u0006\u0010E\u001a\u00020\u0004H\u0000\u00a2\u0006\u0002\bFR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006G"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/domain/Validator;", "", "()V", "ACCOUNT_ADDRESS_REGEX", "", "NAMESPACE_REGEX", "REFERENCE_REGEX", "areAccountsNotEmpty", "", "accounts", "", "areAccountsNotEmpty$walletconnectv2_debug", "areChainIdsIncludedInPermissions", "accountIds", "chains", "areChainIdsIncludedInPermissions$walletconnectv2_debug", "areNotificationTypesValid", "notification", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notifications;", "areNotificationTypesValid$walletconnectv2_debug", "isAccountIdValid", "accountId", "isAccountIdValid$walletconnectv2_debug", "isBlockchainValid", "blockchain", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Blockchain;", "isBlockchainValid$walletconnectv2_debug", "isChainIdValid", "chainId", "isChainIdValid$walletconnectv2_debug", "isJsonRpcValid", "jsonRpc", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpc;", "isJsonRpcValid$walletconnectv2_debug", "splitAccountId", "Lkotlin/Triple;", "elements", "validateCAIP10", "", "onInvalidAccounts", "Lkotlin/Function1;", "validateCAIP10$walletconnectv2_debug", "validateChainIdAuthorization", "onInvalidChainId", "validateChainIdAuthorization$walletconnectv2_debug", "validateIfChainIdsIncludedInPermission", "validateIfChainIdsIncludedInPermission$walletconnectv2_debug", "validateNotification", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notification;", "onInvalidNotification", "validateNotification$walletconnectv2_debug", "validateNotificationAuthorization", "session", "Lcom/walletconnect/walletconnectv2/core/model/vo/sequence/SessionVO;", "type", "onUnauthorizedNotification", "validateNotificationAuthorization$walletconnectv2_debug", "validateProposalFields", "sessionProposal", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionProposal;", "onInvalidProposal", "validateProposalFields$walletconnectv2_debug", "validateSessionPermissions", "permissions", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionPermissions;", "onInvalidPermissions", "validateSessionPermissions$walletconnectv2_debug", "validateWCUri", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$WalletConnectUri;", "uri", "validateWCUri$walletconnectv2_debug", "walletconnectv2_debug"})
public final class Validator {
    @org.jetbrains.annotations.NotNull()
    public static final com.walletconnect.walletconnectv2.engine.domain.Validator INSTANCE = null;
    private static final java.lang.String NAMESPACE_REGEX = "^[-a-z0-9]{3,8}$";
    private static final java.lang.String REFERENCE_REGEX = "^[-a-zA-Z0-9]{1,32}$";
    private static final java.lang.String ACCOUNT_ADDRESS_REGEX = "^[a-zA-Z0-9]{1,64}$";
    
    private Validator() {
        super();
    }
    
    public final void validateSessionPermissions$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions permissions, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onInvalidPermissions) {
    }
    
    public final void validateIfChainIdsIncludedInPermission$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> chains, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onInvalidAccounts) {
    }
    
    public final void validateCAIP10$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onInvalidAccounts) {
    }
    
    public final void validateNotification$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.Notification notification, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onInvalidNotification) {
    }
    
    public final void validateNotificationAuthorization$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.sequence.SessionVO session, @org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onUnauthorizedNotification) {
    }
    
    public final void validateChainIdAuthorization$walletconnectv2_debug(@org.jetbrains.annotations.Nullable()
    java.lang.String chainId, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> chains, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onInvalidChainId) {
    }
    
    public final void validateProposalFields$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionProposal sessionProposal, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onInvalidProposal) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.walletconnect.walletconnectv2.engine.model.EngineDO.WalletConnectUri validateWCUri$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String uri) {
        return null;
    }
    
    public final boolean isJsonRpcValid$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpc jsonRpc) {
        return false;
    }
    
    public final boolean isBlockchainValid$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.Blockchain blockchain) {
        return false;
    }
    
    public final boolean areNotificationTypesValid$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.engine.model.EngineDO.Notifications notification) {
        return false;
    }
    
    public final boolean isChainIdValid$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String chainId) {
        return false;
    }
    
    public final boolean areAccountsNotEmpty$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> accounts) {
        return false;
    }
    
    public final boolean isAccountIdValid$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String accountId) {
        return false;
    }
    
    public final boolean areChainIdsIncludedInPermissions$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> accountIds, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> chains) {
        return false;
    }
    
    private final kotlin.Triple<java.lang.String, java.lang.String, java.lang.String> splitAccountId(java.util.List<java.lang.String> elements) {
        return null;
    }
}