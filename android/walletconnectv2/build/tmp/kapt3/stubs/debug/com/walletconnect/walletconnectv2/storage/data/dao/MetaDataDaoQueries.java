package com.walletconnect.walletconnectv2.storage.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H&J\u0095\u0001\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\t0\u0007\"\b\b\u0000\u0010\t*\u00020\n2{\u0010\u000b\u001aw\u0012\u0013\u0012\u00110\r\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0010\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0011\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0012\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0013\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0014\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0015\u0012\u0004\u0012\u0002H\t0\fH&J.\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00052\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00050\u0014H&J\u000e\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\u0007H&\u00a8\u0006\u0018"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/MetaDataDaoQueries;", "Lcom/squareup/sqldelight/Transacter;", "deleteMetaDataFromTopic", "", "topic", "", "getMetaData", "Lcom/squareup/sqldelight/Query;", "Lcom/walletconnect/walletconnectv2/storage/data/dao/MetaDataDao;", "T", "", "mapper", "Lkotlin/Function5;", "", "Lkotlin/ParameterName;", "name", "id", "_name", "description", "url", "", "icons", "insertOrIgnoreMetaData", "lastInsertedRowId", "walletconnectv2_debug"})
public abstract interface MetaDataDaoQueries extends com.squareup.sqldelight.Transacter {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<java.lang.Long> lastInsertedRowId();
    
    @org.jetbrains.annotations.NotNull()
    public abstract <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getMetaData(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function5<? super java.lang.Long, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? extends T> mapper);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.MetaDataDao> getMetaData();
    
    public abstract void insertOrIgnoreMetaData(@org.jetbrains.annotations.NotNull()
    java.lang.String _name, @org.jetbrains.annotations.NotNull()
    java.lang.String description, @org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> icons);
    
    public abstract void deleteMetaDataFromTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
}