package com.walletconnect.walletconnectv2.core.exceptions.client;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0013\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\f\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"EMPTY_ACCOUNT_LIST_MESSAGE", "", "EMPTY_CHAIN_LIST_MESSAGE", "EMPTY_RPC_METHODS_LIST_MESSAGE", "INVALID_NOTIFICATIONS_TYPES_MESSAGE", "INVALID_NOTIFICATION_MESSAGE", "INVALID_SESSION_PROPOSAL_MESSAGE", "MALFORMED_PAIRING_URI_MESSAGE", "NO_SEQUENCE_FOR_TOPIC_MESSAGE", "PAIRING_NOW_ALLOWED_MESSAGE", "UNAUTHORIZED_APPROVE_MESSAGE", "UNAUTHORIZED_CHAIN_ID_MESSAGE", "UNAUTHORIZED_CONNECT_MESSAGE", "UNAUTHORIZED_NOTIFICATION_TYPE_MESSAGE", "UNAUTHORIZED_PAIR_MESSAGE", "UNAUTHORIZED_REJECT_MESSAGE", "UNAUTHORIZED_UPDATE_MESSAGE", "UNAUTHORIZED_UPGRADE_MESSAGE", "WRONG_ACCOUNT_ID_FORMAT_MESSAGE", "WRONG_CHAIN_ID_FORMAT_MESSAGE", "walletconnectv2_debug"})
public final class ClientMessagesKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NO_SEQUENCE_FOR_TOPIC_MESSAGE = "Cannot find sequence for given topic: ";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PAIRING_NOW_ALLOWED_MESSAGE = "Pair with existing pairing is not allowed";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNAUTHORIZED_CONNECT_MESSAGE = "The connect() was called by the unauthorized peer. Initialize SDK with isController = false.";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNAUTHORIZED_PAIR_MESSAGE = "The pair() was called by the unauthorized peer. Initialize SDK with isController = true.";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNAUTHORIZED_APPROVE_MESSAGE = "The approve() was called by the unauthorized peer. Initialize SDK with isController = true.";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNAUTHORIZED_REJECT_MESSAGE = "The reject() was called by the unauthorized peer. Initialize SDK with isController = true.";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNAUTHORIZED_UPDATE_MESSAGE = "The update() was called by the unauthorized peer. Initialize SDK with isController = true";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNAUTHORIZED_UPGRADE_MESSAGE = "The upgrade() was called by the unauthorized peer. Initialize SDK with isController = true";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMPTY_CHAIN_LIST_MESSAGE = "List of chains in session permissions cannot be empty";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMPTY_ACCOUNT_LIST_MESSAGE = "List of accounts cannot be empty";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMPTY_RPC_METHODS_LIST_MESSAGE = "List of rpc methods in session permissions cannot be empty";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String INVALID_NOTIFICATIONS_TYPES_MESSAGE = "Invalid notification types";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNAUTHORIZED_NOTIFICATION_TYPE_MESSAGE = "Unauthorized Notification Type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WRONG_CHAIN_ID_FORMAT_MESSAGE = "Blockchain chaiId does not follow the CAIP2 semantics. See: https://github.com/ChainAgnostic/CAIPs/blob/master/CAIPs/caip-2.md";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WRONG_ACCOUNT_ID_FORMAT_MESSAGE = "AccountIds does not follow the CAIP10 semantics. See: https://github.com/ChainAgnostic/CAIPs/blob/master/CAIPs/caip-10.md";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNAUTHORIZED_CHAIN_ID_MESSAGE = "Unauthorized chain id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String INVALID_NOTIFICATION_MESSAGE = "Notification type and data fields cannot be empty";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String INVALID_SESSION_PROPOSAL_MESSAGE = "None of the session proposal fields cannot be empty";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MALFORMED_PAIRING_URI_MESSAGE = "Pairing URI string is invalid.";
}