package com.walletconnect.walletconnectv2.core.exceptions.peer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0010\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0002\u001a\u00020\u0015H\u0002J\t\u0010\u0016\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\fH\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0018"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/exceptions/peer/PeerError;", "", "error", "Lcom/walletconnect/walletconnectv2/core/exceptions/peer/Error;", "(Lcom/walletconnect/walletconnectv2/core/exceptions/peer/Error;)V", "code", "", "getCode", "()I", "getError", "()Lcom/walletconnect/walletconnectv2/core/exceptions/peer/Error;", "message", "", "getMessage", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "getPeerType", "Lcom/walletconnect/walletconnectv2/core/exceptions/peer/Error$UnauthorizedMatchingController;", "hashCode", "toString", "walletconnectv2_debug"})
public final class PeerError {
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.exceptions.peer.Error error = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String message = null;
    private final int code = 0;
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.exceptions.peer.PeerError copy(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.exceptions.peer.Error error) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public PeerError(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.exceptions.peer.Error error) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.exceptions.peer.Error component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.exceptions.peer.Error getError() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final int getCode() {
        return 0;
    }
    
    private final java.lang.String getPeerType(com.walletconnect.walletconnectv2.core.exceptions.peer.Error.UnauthorizedMatchingController error) {
        return null;
    }
}