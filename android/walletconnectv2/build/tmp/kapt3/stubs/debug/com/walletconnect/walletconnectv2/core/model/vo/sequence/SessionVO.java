package com.walletconnect.walletconnectv2.core.model.vo.sequence;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\"\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u00a0\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\t\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u0012\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u000e\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0019J\t\u00104\u001a\u00020\u0003H\u00c6\u0003J\t\u00105\u001a\u00020\u0012H\u00c6\u0003J\u000f\u00106\u001a\b\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u000b\u00107\u001a\u0004\u0018\u00010\u0015H\u00c6\u0003J\t\u00108\u001a\u00020\u0017H\u00c6\u0003J\t\u00109\u001a\u00020\u000eH\u00c6\u0003J\t\u0010:\u001a\u00020\u0005H\u00c6\u0003J\t\u0010;\u001a\u00020\u0007H\u00c6\u0003J\u0019\u0010<\u001a\u00020\tH\u00c6\u0003\u00f8\u0001\u0000\u00f8\u0001\u0002\u00f8\u0001\u0001\u00a2\u0006\u0004\b=\u0010 J\u001b\u0010>\u001a\u0004\u0018\u00010\tH\u00c6\u0003\u00f8\u0001\u0000\u00f8\u0001\u0002\u00f8\u0001\u0001\u00a2\u0006\u0004\b?\u0010 J\u001b\u0010@\u001a\u0004\u0018\u00010\tH\u00c6\u0003\u00f8\u0001\u0000\u00f8\u0001\u0002\u00f8\u0001\u0001\u00a2\u0006\u0004\bA\u0010 J\u000f\u0010B\u001a\b\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u000f\u0010C\u001a\b\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u0011\u0010D\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rH\u00c6\u0003J\u00c2\u0001\u0010E\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\t2\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\b\b\u0002\u0010\u0011\u001a\u00020\u00122\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00152\b\b\u0002\u0010\u0016\u001a\u00020\u00172\b\b\u0002\u0010\u0018\u001a\u00020\u000eH\u00c6\u0001\u00f8\u0001\u0000\u00f8\u0001\u0001\u00a2\u0006\u0004\bF\u0010GJ\u0013\u0010H\u001a\u00020\'2\b\u0010I\u001a\u0004\u0018\u00010JH\u00d6\u0003J\t\u0010K\u001a\u00020LH\u00d6\u0001J\t\u0010M\u001a\u00020\u000eH\u00d6\u0001R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001bR\u001e\u0010\u000b\u001a\u0004\u0018\u00010\t\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010!\u001a\u0004\b\u001f\u0010 R\u0011\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010&\u001a\u00020\'\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010(R\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001bR\u001e\u0010\n\u001a\u0004\u0018\u00010\t\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010!\u001a\u0004\b*\u0010 R\u0011\u0010\u0018\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010 R\u001c\u0010\b\u001a\u00020\t\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010!\u001a\u0004\b,\u0010 R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010.R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u00100R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u00102R\u0019\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010\u001b\u0082\u0002\u000f\n\u0002\b\u0019\n\u0005\b\u00a1\u001e0\u0001\n\u0002\b!\u00a8\u0006N"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/sequence/SessionVO;", "Lcom/walletconnect/walletconnectv2/core/model/type/Sequence;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "expiry", "Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "status", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "selfParticipant", "Lcom/walletconnect/walletconnectv2/core/model/vo/PublicKey;", "peerParticipant", "controllerKey", "chains", "", "", "methods", "types", "ttl", "Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "accounts", "appMetaData", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/AppMetaDataVO;", "controllerType", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "relayProtocol", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;Ljava/util/List;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/AppMetaDataVO;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V", "getAccounts", "()Ljava/util/List;", "getAppMetaData", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/AppMetaDataVO;", "getChains", "getControllerKey-cNg-jV0", "()Ljava/lang/String;", "Ljava/lang/String;", "getControllerType", "()Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "getExpiry", "()Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "isPeerController", "", "()Z", "getMethods", "getPeerParticipant-cNg-jV0", "getRelayProtocol", "getSelfParticipant-oW5vskc", "getStatus", "()Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getTtl", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "getTypes", "component1", "component10", "component11", "component12", "component13", "component14", "component2", "component3", "component4", "component4-oW5vskc", "component5", "component5-cNg-jV0", "component6", "component6-cNg-jV0", "component7", "component8", "component9", "copy", "copy-ak1HopU", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;Ljava/util/List;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/AppMetaDataVO;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Ljava/lang/String;)Lcom/walletconnect/walletconnectv2/core/model/vo/sequence/SessionVO;", "equals", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
public final class SessionVO implements com.walletconnect.walletconnectv2.core.model.type.Sequence {
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String selfParticipant = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String peerParticipant = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String controllerKey = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> chains = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> methods = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.String> types = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> accounts = null;
    @org.jetbrains.annotations.Nullable()
    private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.AppMetaDataVO appMetaData = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String relayProtocol = null;
    private final boolean isPeerController = false;
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    private SessionVO(com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry, com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, java.lang.String selfParticipant, java.lang.String peerParticipant, java.lang.String controllerKey, java.util.List<java.lang.String> chains, java.util.List<java.lang.String> methods, java.util.List<java.lang.String> types, com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl, java.util.List<java.lang.String> accounts, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.AppMetaDataVO appMetaData, com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType, java.lang.String relayProtocol) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO getExpiry() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus getStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getChains() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getMethods() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getTypes() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.vo.TtlVO component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.vo.TtlVO getTtl() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> component11() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getAccounts() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.AppMetaDataVO component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.AppMetaDataVO getAppMetaData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType getControllerType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRelayProtocol() {
        return null;
    }
    
    public final boolean isPeerController() {
        return false;
    }
}