package com.walletconnect.walletconnectv2.storage.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\t\u001a\u00020\nH&J&\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u00072\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\r\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u000eH&J\u00c2\u0001\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u000f0\u0007\"\b\b\u0000\u0010\u000f*\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\r\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u000e2\u008f\u0001\u0010\u0011\u001a\u008a\u0001\u0012\u0013\u0012\u00110\n\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\t\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0004\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\r\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0017\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0019\u0012\u0004\u0012\u0002H\u000f0\u0012H&J&\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001b0\u00072\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\r\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u000eH&J\u00c2\u0001\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\u000f0\u0007\"\b\b\u0000\u0010\u000f*\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\r\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u000e2\u008f\u0001\u0010\u0011\u001a\u008a\u0001\u0012\u0013\u0012\u00110\n\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\t\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0004\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\r\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0017\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0019\u0012\u0004\u0012\u0002H\u000f0\u0012H&J<\u0010\u001c\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0004\u001a\u00020\u00052\b\u0010\r\u001a\u0004\u0018\u00010\u00052\b\u0010\u0015\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0018H&J\u000e\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\u0007H&J\u0018\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\t\u001a\u00020\nH&\u00a8\u0006\u001f"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryQueries;", "Lcom/squareup/sqldelight/Transacter;", "deleteJsonRpcHistory", "", "topic", "", "doesJsonRpcNotExist", "Lcom/squareup/sqldelight/Query;", "", "request_id", "", "getJsonRpcRequestsDaos", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetJsonRpcRequestsDaos;", "method", "", "T", "", "mapper", "Lkotlin/Function6;", "Lkotlin/ParameterName;", "name", "body", "Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "status", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "controller_type", "getJsonRpcRespondsDaos", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetJsonRpcRespondsDaos;", "insertJsonRpcHistory", "selectLastInsertedRowId", "updateJsonRpcHistory", "walletconnectv2_debug"})
public abstract interface JsonRpcHistoryQueries extends com.squareup.sqldelight.Transacter {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<java.lang.Boolean> doesJsonRpcNotExist(long request_id);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<java.lang.Long> selectLastInsertedRowId();
    
    @org.jetbrains.annotations.NotNull()
    public abstract <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getJsonRpcRequestsDaos(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.String> method, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function6<? super java.lang.Long, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? extends T> mapper);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetJsonRpcRequestsDaos> getJsonRpcRequestsDaos(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.String> method);
    
    @org.jetbrains.annotations.NotNull()
    public abstract <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getJsonRpcRespondsDaos(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.String> method, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function6<? super java.lang.Long, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? extends T> mapper);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetJsonRpcRespondsDaos> getJsonRpcRespondsDaos(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.String> method);
    
    public abstract void insertJsonRpcHistory(long request_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.lang.String method, @org.jetbrains.annotations.Nullable()
    java.lang.String body, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type);
    
    public abstract void updateJsonRpcHistory(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus status, long request_id);
    
    public abstract void deleteJsonRpcHistory(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
}