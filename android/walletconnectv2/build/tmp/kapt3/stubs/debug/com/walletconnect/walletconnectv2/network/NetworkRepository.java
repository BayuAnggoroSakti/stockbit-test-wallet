package com.walletconnect.walletconnectv2.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b`\u0018\u00002\u00020\u0001JA\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00132\u001a\b\u0002\u0010\u0014\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\u0016\u0012\u0004\u0012\u00020\r0\u0015H&\u00f8\u0001\u0000J-\u0010\u0018\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0018\u0010\u0014\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u0016\u0012\u0004\u0012\u00020\r0\u0015H&\u00f8\u0001\u0000J5\u0010\u001a\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u001c2\u0018\u0010\u0014\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001d0\u0016\u0012\u0004\u0012\u00020\r0\u0015H&\u00f8\u0001\u0000R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000b\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001e"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/NetworkRepository;", "", "eventsFlow", "Lkotlinx/coroutines/flow/SharedFlow;", "Lcom/tinder/scarlet/WebSocket$Event;", "getEventsFlow", "()Lkotlinx/coroutines/flow/SharedFlow;", "subscriptionRequest", "Lkotlinx/coroutines/flow/Flow;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request;", "getSubscriptionRequest", "()Lkotlinx/coroutines/flow/Flow;", "publish", "", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "message", "", "prompt", "", "onResult", "Lkotlin/Function1;", "Lkotlin/Result;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Acknowledgement;", "subscribe", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Acknowledgement;", "unsubscribe", "subscriptionId", "Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Acknowledgement;", "walletconnectv2_debug"})
public abstract interface NetworkRepository {
    
    @org.jetbrains.annotations.NotNull()
    public abstract kotlinx.coroutines.flow.SharedFlow<com.tinder.scarlet.WebSocket.Event> getEventsFlow();
    
    @org.jetbrains.annotations.NotNull()
    public abstract kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request> getSubscriptionRequest();
    
    public abstract void publish(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean prompt, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Acknowledgement>, kotlin.Unit> onResult);
    
    public abstract void subscribe(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Acknowledgement>, kotlin.Unit> onResult);
    
    public abstract void unsubscribe(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO subscriptionId, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Acknowledgement>, kotlin.Unit> onResult);
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 3)
    public final class DefaultImpls {
    }
}