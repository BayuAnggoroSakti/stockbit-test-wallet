package com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\b\u0003\u0004\u0005\u0006\u0007\b\t\nB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\b\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "Lcom/walletconnect/walletconnectv2/core/model/type/ClientParams;", "()V", "ApproveParams", "DeleteParams", "NotificationParams", "PayloadParams", "PingParams", "Proposal", "RejectParams", "UpdateParams", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$Proposal;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$ApproveParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$RejectParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$PayloadParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$DeleteParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$PingParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$NotificationParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$UpdateParams;", "walletconnectv2_debug"})
public abstract class PairingParamsVO implements com.walletconnect.walletconnectv2.core.model.type.ClientParams {
    
    private PairingParamsVO() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\t\u0010 \u001a\u00020\rH\u00c6\u0003JI\u0010!\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\f\u001a\u00020\rH\u00c6\u0001J\u0013\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%H\u00d6\u0003J\t\u0010&\u001a\u00020\'H\u00d6\u0001J\t\u0010(\u001a\u00020)H\u00d6\u0001R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0013\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\f\u001a\u00020\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001a\u00a8\u0006*"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$Proposal;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "relay", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "proposer", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingProposerVO;", "signal", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingSignalVO;", "permissions", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingProposedPermissionsVO;", "ttl", "Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingProposerVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingSignalVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingProposedPermissionsVO;Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;)V", "getPermissions", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingProposedPermissionsVO;", "getProposer", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingProposerVO;", "getRelay", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "getSignal", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/proposal/PairingSignalVO;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getTtl", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    public static final class Proposal extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposerVO proposer = null;
        @org.jetbrains.annotations.Nullable()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingSignalVO signal = null;
        @org.jetbrains.annotations.Nullable()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposedPermissionsVO permissions = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.Proposal copy(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposerVO proposer, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingSignalVO signal, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposedPermissionsVO permissions, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Proposal(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposerVO proposer, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingSignalVO signal, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposedPermissionsVO permissions, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO getRelay() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposerVO component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposerVO getProposer() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingSignalVO component4() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingSignalVO getSignal() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposedPermissionsVO component5() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.proposal.PairingProposedPermissionsVO getPermissions() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TtlVO component6() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TtlVO getTtl() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0081\b\u0018\u00002\u00020\u0001B/\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tH\u00c6\u0003J3\u0010\u0017\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00072\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006 "}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$ApproveParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "relay", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "responder", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingParticipantVO;", "expiry", "Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "state", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingStateVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingParticipantVO;Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingStateVO;)V", "getExpiry", "()Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "getRelay", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "getResponder", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingParticipantVO;", "getState", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingStateVO;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class ApproveParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO responder = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry = null;
        @org.jetbrains.annotations.Nullable()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO state = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.ApproveParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "relay")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "responder")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO responder, @org.jetbrains.annotations.NotNull()
        @com.walletconnect.walletconnectv2.core.adapters.ExpiryAdapter.Qualifier()
        @com.squareup.moshi.Json(name = "expiry")
        com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry, @org.jetbrains.annotations.Nullable()
        @com.squareup.moshi.Json(name = "state")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO state) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public ApproveParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "relay")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "responder")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO responder, @org.jetbrains.annotations.NotNull()
        @com.walletconnect.walletconnectv2.core.adapters.ExpiryAdapter.Qualifier()
        @com.squareup.moshi.Json(name = "expiry")
        com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry, @org.jetbrains.annotations.Nullable()
        @com.squareup.moshi.Json(name = "state")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO state) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO getRelay() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO getResponder() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO getExpiry() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO component4() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO getState() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$RejectParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "reason", "", "(Ljava/lang/String;)V", "getReason", "()Ljava/lang/String;", "walletconnectv2_debug"})
    public static final class RejectParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String reason = null;
        
        public RejectParams(@org.jetbrains.annotations.NotNull()
        java.lang.String reason) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getReason() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0081\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$PayloadParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "request", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/after/payload/ProposalRequestVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/after/payload/ProposalRequestVO;)V", "getRequest", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/after/payload/ProposalRequestVO;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class PayloadParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.payload.ProposalRequestVO request = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.PayloadParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "request")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.payload.ProposalRequestVO request) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public PayloadParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "request")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.payload.ProposalRequestVO request) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.payload.ProposalRequestVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.payload.ProposalRequestVO getRequest() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0001\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$DeleteParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "reason", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;)V", "getReason", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class DeleteParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO reason = null;
        
        public DeleteParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "reason")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO reason) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO getReason() {
            return null;
        }
    }
    
    @kotlin.Suppress(names = {"CanSealedSubClassBeObject"})
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$PingParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "()V", "walletconnectv2_debug"})
    public static final class PingParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO {
        
        public PingParams() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$NotificationParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "type", "", "data", "", "(Ljava/lang/String;Ljava/lang/Object;)V", "getData", "()Ljava/lang/Object;", "getType", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class NotificationParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String type = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.Object data = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.NotificationParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "type")
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "data")
        java.lang.Object data) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public NotificationParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "type")
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "data")
        java.lang.Object data) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getType() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object getData() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0081\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO$UpdateParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/PairingParamsVO;", "state", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingStateVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingStateVO;)V", "getState", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/pairing/before/success/PairingStateVO;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class UpdateParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO state = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO.UpdateParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "state")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO state) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public UpdateParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "state")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO state) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO getState() {
            return null;
        }
    }
}