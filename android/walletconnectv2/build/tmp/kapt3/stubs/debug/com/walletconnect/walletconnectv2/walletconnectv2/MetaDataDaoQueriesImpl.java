package com.walletconnect.walletconnectv2.walletconnectv2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00130\nH\u0016J\u0095\u0001\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00140\n\"\b\b\u0000\u0010\u0014*\u00020\u00152{\u0010\u0016\u001aw\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u0019\u0012\b\b\u001a\u0012\u0004\b\b(\u001b\u0012\u0013\u0012\u00110\u0012\u00a2\u0006\f\b\u0019\u0012\b\b\u001a\u0012\u0004\b\b(\u001c\u0012\u0013\u0012\u00110\u0012\u00a2\u0006\f\b\u0019\u0012\b\b\u001a\u0012\u0004\b\b(\u001d\u0012\u0013\u0012\u00110\u0012\u00a2\u0006\f\b\u0019\u0012\b\b\u001a\u0012\u0004\b\b(\u001e\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00120\u001f\u00a2\u0006\f\b\u0019\u0012\b\b\u001a\u0012\u0004\b\b( \u0012\u0004\u0012\u0002H\u00140\u0017H\u0016J.\u0010!\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\u00122\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00120\u001fH\u0016J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00180\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\r\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f\u00a8\u0006\""}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/MetaDataDaoQueriesImpl;", "Lcom/squareup/sqldelight/TransacterImpl;", "Lcom/walletconnect/walletconnectv2/storage/data/dao/MetaDataDaoQueries;", "database", "Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;", "driver", "Lcom/squareup/sqldelight/db/SqlDriver;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;Lcom/squareup/sqldelight/db/SqlDriver;)V", "getMetaData", "", "Lcom/squareup/sqldelight/Query;", "getGetMetaData$walletconnectv2_debug", "()Ljava/util/List;", "lastInsertedRowId", "getLastInsertedRowId$walletconnectv2_debug", "deleteMetaDataFromTopic", "", "topic", "", "Lcom/walletconnect/walletconnectv2/storage/data/dao/MetaDataDao;", "T", "", "mapper", "Lkotlin/Function5;", "", "Lkotlin/ParameterName;", "name", "id", "_name", "description", "url", "", "icons", "insertOrIgnoreMetaData", "walletconnectv2_debug"})
final class MetaDataDaoQueriesImpl extends com.squareup.sqldelight.TransacterImpl implements com.walletconnect.walletconnectv2.storage.data.dao.MetaDataDaoQueries {
    private final com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl database = null;
    private final com.squareup.sqldelight.db.SqlDriver driver = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> lastInsertedRowId = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getMetaData = null;
    
    public MetaDataDaoQueriesImpl(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl database, @org.jetbrains.annotations.NotNull()
    com.squareup.sqldelight.db.SqlDriver driver) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getLastInsertedRowId$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetMetaData$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<java.lang.Long> lastInsertedRowId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getMetaData(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function5<? super java.lang.Long, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? extends T> mapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.MetaDataDao> getMetaData() {
        return null;
    }
    
    @java.lang.Override()
    public void insertOrIgnoreMetaData(@org.jetbrains.annotations.NotNull()
    java.lang.String _name, @org.jetbrains.annotations.NotNull()
    java.lang.String description, @org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> icons) {
    }
    
    @java.lang.Override()
    public void deleteMetaDataFromTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
}