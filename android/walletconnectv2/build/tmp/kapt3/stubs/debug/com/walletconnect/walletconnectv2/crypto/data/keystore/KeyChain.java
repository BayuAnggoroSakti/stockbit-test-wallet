package com.walletconnect.walletconnectv2.crypto.data.keystore;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0002J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0006H\u0016J\u001c\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u000e2\u0006\u0010\f\u001a\u00020\u0006H\u0016J \u0010\u000f\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\bH\u0016J\u001c\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u000e2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/crypto/data/keystore/KeyChain;", "Lcom/walletconnect/walletconnectv2/crypto/KeyStore;", "sharedPreferences", "Landroid/content/SharedPreferences;", "(Landroid/content/SharedPreferences;)V", "concatKeys", "", "keyA", "Lcom/walletconnect/walletconnectv2/core/model/vo/Key;", "keyB", "deleteKeys", "", "tag", "getKeys", "Lkotlin/Pair;", "setKey", "key1", "key2", "splitKeys", "walletconnectv2_debug"})
public final class KeyChain implements com.walletconnect.walletconnectv2.crypto.KeyStore {
    private final android.content.SharedPreferences sharedPreferences = null;
    
    public KeyChain(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences sharedPreferences) {
        super();
    }
    
    @java.lang.Override()
    public void setKey(@org.jetbrains.annotations.NotNull()
    java.lang.String tag, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.Key key1, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.Key key2) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kotlin.Pair<java.lang.String, java.lang.String> getKeys(@org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
        return null;
    }
    
    @java.lang.Override()
    public void deleteKeys(@org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
    }
    
    private final java.lang.String concatKeys(com.walletconnect.walletconnectv2.core.model.vo.Key keyA, com.walletconnect.walletconnectv2.core.model.vo.Key keyB) {
        return null;
    }
    
    private final kotlin.Pair<java.lang.String, java.lang.String> splitKeys(java.lang.String concatKeys) {
        return null;
    }
}