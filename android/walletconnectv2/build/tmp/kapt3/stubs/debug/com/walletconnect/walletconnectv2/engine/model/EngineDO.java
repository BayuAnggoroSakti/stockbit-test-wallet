package com.walletconnect.walletconnectv2.engine.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0017\u0003\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0017\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&\'()*+,-./0\u00a8\u00061"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "", "()V", "AppMetaData", "Blockchain", "Default", "DeletedPairing", "JsonRpc", "JsonRpcResponse", "Notification", "Notifications", "PairingUpdate", "Request", "SessionApproved", "SessionDelete", "SessionNotification", "SessionPermissions", "SessionProposal", "SessionRejected", "SessionRequest", "SessionState", "SessionUpdate", "SessionUpgrade", "SettledPairing", "SettledSession", "WalletConnectUri", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$WalletConnectUri;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionProposal;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRequest;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRequest$JSONRPCRequest;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionDelete;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$DeletedPairing;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionNotification;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledPairing;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRejected;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionApproved;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$PairingUpdate;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionUpdate;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionUpgrade;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Default;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notification;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionState;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionPermissions;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Blockchain;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpc;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Request;", "walletconnectv2_debug"})
public abstract class EngineDO {
    
    private EngineDO() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000b\b\u0000\u0018\u00002\u00020\u0001B2\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\rR\u001c\u0010\u0004\u001a\u00020\u0005\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000f\u0082\u0002\u000f\n\u0002\b\u0019\n\u0005\b\u00a1\u001e0\u0001\n\u0002\b!\u00a8\u0006\u0016"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$WalletConnectUri;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "publicKey", "Lcom/walletconnect/walletconnectv2/core/model/vo/PublicKey;", "isController", "", "relay", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "version", "", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Ljava/lang/String;ZLcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V", "()Z", "getPublicKey-oW5vskc", "()Ljava/lang/String;", "Ljava/lang/String;", "getRelay", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getVersion", "walletconnectv2_debug"})
    public static final class WalletConnectUri extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String publicKey = null;
        private final boolean isController = false;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String version = null;
        
        private WalletConnectUri(com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, java.lang.String publicKey, boolean isController, com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, java.lang.String version) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
            return null;
        }
        
        public final boolean isController() {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO getRelay() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getVersion() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b#\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B\u0091\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\b\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\b\u0012\u0010\b\u0002\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\b\u0012\u0006\u0010\r\u001a\u00020\u0004\u0012\u0006\u0010\u000e\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\b\u0012\u0006\u0010\u0014\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0015J\t\u0010&\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0010H\u00c6\u0003J\t\u0010(\u001a\u00020\u0012H\u00c6\u0003J\u000f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00040\bH\u00c6\u0003J\t\u0010*\u001a\u00020\u0004H\u00c6\u0003J\t\u0010+\u001a\u00020\u0004H\u00c6\u0003J\t\u0010,\u001a\u00020\u0004H\u00c6\u0003J\u000f\u0010-\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u00c6\u0003J\u000f\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00040\bH\u00c6\u0003J\u000f\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00040\bH\u00c6\u0003J\u0011\u00100\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\bH\u00c6\u0003J\t\u00101\u001a\u00020\u0004H\u00c6\u0003J\t\u00102\u001a\u00020\u0004H\u00c6\u0003J\u00ab\u0001\u00103\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u00042\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\b2\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\b2\u0010\b\u0002\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\b2\b\b\u0002\u0010\r\u001a\u00020\u00042\b\b\u0002\u0010\u000e\u001a\u00020\u00042\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u00122\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\b2\b\b\u0002\u0010\u0014\u001a\u00020\u0004H\u00c6\u0001J\u0013\u00104\u001a\u00020\u00102\b\u00105\u001a\u0004\u0018\u000106H\u00d6\u0003J\t\u00107\u001a\u000208H\u00d6\u0001J\t\u00109\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0017R\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0017R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u001cR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0017R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001aR\u0011\u0010\u000e\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001aR\u0011\u0010\u0014\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u001aR\u0011\u0010\r\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001aR\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0019\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001a\u00a8\u0006:"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionProposal;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "name", "", "description", "url", "icons", "", "Ljava/net/URI;", "chains", "methods", "types", "topic", "publicKey", "isController", "", "ttl", "", "accounts", "relayProtocol", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ZJLjava/util/List;Ljava/lang/String;)V", "getAccounts", "()Ljava/util/List;", "getChains", "getDescription", "()Ljava/lang/String;", "getIcons", "()Z", "getMethods", "getName", "getPublicKey", "getRelayProtocol", "getTopic", "getTtl", "()J", "getTypes", "getUrl", "component1", "component10", "component11", "component12", "component13", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SessionProposal extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String name = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String description = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String url = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.net.URI> icons = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> chains = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> methods = null;
        @org.jetbrains.annotations.Nullable()
        private final java.util.List<java.lang.String> types = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String publicKey = null;
        private final boolean isController = false;
        private final long ttl = 0L;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> accounts = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String relayProtocol = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionProposal copy(@org.jetbrains.annotations.NotNull()
        java.lang.String name, @org.jetbrains.annotations.NotNull()
        java.lang.String description, @org.jetbrains.annotations.NotNull()
        java.lang.String url, @org.jetbrains.annotations.NotNull()
        java.util.List<java.net.URI> icons, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> chains, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> methods, @org.jetbrains.annotations.Nullable()
        java.util.List<java.lang.String> types, @org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String publicKey, boolean isController, long ttl, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
        java.lang.String relayProtocol) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionProposal(@org.jetbrains.annotations.NotNull()
        java.lang.String name, @org.jetbrains.annotations.NotNull()
        java.lang.String description, @org.jetbrains.annotations.NotNull()
        java.lang.String url, @org.jetbrains.annotations.NotNull()
        java.util.List<java.net.URI> icons, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> chains, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> methods, @org.jetbrains.annotations.Nullable()
        java.util.List<java.lang.String> types, @org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String publicKey, boolean isController, long ttl, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
        java.lang.String relayProtocol) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getName() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDescription() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUrl() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.net.URI> component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.net.URI> getIcons() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component5() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getChains() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component6() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getMethods() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.util.List<java.lang.String> component7() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.util.List<java.lang.String> getTypes() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component8() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component9() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getPublicKey() {
            return null;
        }
        
        public final boolean component10() {
            return false;
        }
        
        public final boolean isController() {
            return false;
        }
        
        public final long component11() {
            return 0L;
        }
        
        public final long getTtl() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component12() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getAccounts() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component13() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getRelayProtocol() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u0019B\u001f\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0004H\u00c6\u0003J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0007H\u00c6\u0003J)\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0004H\u00d6\u0001R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\n\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRequest;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "", "chainId", "request", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRequest$JSONRPCRequest;", "(Ljava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRequest$JSONRPCRequest;)V", "getChainId", "()Ljava/lang/String;", "getRequest", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRequest$JSONRPCRequest;", "getTopic", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "JSONRPCRequest", "walletconnectv2_debug"})
    public static final class SessionRequest extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String chainId = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionRequest.JSONRPCRequest request = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionRequest copy(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.Nullable()
        java.lang.String chainId, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionRequest.JSONRPCRequest request) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionRequest(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.Nullable()
        java.lang.String chainId, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionRequest.JSONRPCRequest request) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getChainId() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionRequest.JSONRPCRequest component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionRequest.JSONRPCRequest getRequest() {
            return null;
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b\u00a8\u0006\u0018"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRequest$JSONRPCRequest;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "id", "", "method", "", "params", "(JLjava/lang/String;Ljava/lang/String;)V", "getId", "()J", "getMethod", "()Ljava/lang/String;", "getParams", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class JSONRPCRequest extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String method = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String params = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionRequest.JSONRPCRequest copy(long id, @org.jetbrains.annotations.NotNull()
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            java.lang.String params) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public JSONRPCRequest(long id, @org.jetbrains.annotations.NotNull()
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            java.lang.String params) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            public final long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMethod() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getParams() {
                return null;
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u000b\u001a\u00020\u0004H\u00c6\u0003J\u001d\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0004H\u00c6\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0004H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionDelete;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "", "reason", "(Ljava/lang/String;Ljava/lang/String;)V", "getReason", "()Ljava/lang/String;", "getTopic", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SessionDelete extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String reason = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionDelete copy(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String reason) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionDelete(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String reason) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getReason() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u000b\u001a\u00020\u0004H\u00c6\u0003J\u001d\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0004H\u00c6\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0004H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$DeletedPairing;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "", "reason", "(Ljava/lang/String;Ljava/lang/String;)V", "getReason", "()Ljava/lang/String;", "getTopic", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class DeletedPairing extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String reason = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.DeletedPairing copy(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String reason) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public DeletedPairing(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String reason) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getReason() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0004H\u00c6\u0003J\'\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u0004H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0004H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\t\u00a8\u0006\u0017"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionNotification;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "", "type", "data", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getData", "()Ljava/lang/String;", "getTopic", "getType", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SessionNotification extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String type = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String data = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionNotification copy(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        java.lang.String data) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionNotification(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        java.lang.String data) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getType() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getData() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B\u001f\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\bH\u00c6\u0003J)\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bH\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0006H\u00d6\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u001b"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledPairing;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "relay", "", "appMetaData", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;)V", "getAppMetaData", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "getRelay", "()Ljava/lang/String;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SettledPairing extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String relay = null;
        @org.jetbrains.annotations.Nullable()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData appMetaData = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledPairing copy(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        java.lang.String relay, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData appMetaData) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SettledPairing(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        java.lang.String relay, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData appMetaData) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getRelay() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData component3() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData getAppMetaData() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u000b\u001a\u00020\u0004H\u00c6\u0003J\u001d\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0004H\u00c6\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0004H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionRejected;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "", "reason", "(Ljava/lang/String;Ljava/lang/String;)V", "getReason", "()Ljava/lang/String;", "getTopic", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SessionRejected extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String reason = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionRejected copy(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String reason) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionRejected(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String reason) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getReason() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B-\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0014\u001a\u00020\u0004H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\bH\u00c6\u0003J\u000f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u00c6\u0003J9\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006 "}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionApproved;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "", "peerAppMetaData", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "permissions", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionPermissions;", "accounts", "", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionPermissions;Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "getPeerAppMetaData", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "getPermissions", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionPermissions;", "getTopic", "()Ljava/lang/String;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SessionApproved extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.Nullable()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData peerAppMetaData = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions permissions = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> accounts = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionApproved copy(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData peerAppMetaData, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions permissions, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionApproved(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData peerAppMetaData, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions permissions, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData component2() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData getPeerAppMetaData() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions getPermissions() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getAccounts() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J\u001d\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0017"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$PairingUpdate;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "metaData", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;)V", "getMetaData", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    public static final class PairingUpdate extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData metaData = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.PairingUpdate copy(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData metaData) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public PairingUpdate(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData metaData) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData getMetaData() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\t\u0010\r\u001a\u00020\u0004H\u00c6\u0003J\u000f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J#\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0007H\u00d6\u0001R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0017"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionUpdate;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "accounts", "", "", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SessionUpdate extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> accounts = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionUpdate copy(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionUpdate(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getAccounts() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002B)\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\tJ\t\u0010\u000f\u001a\u00020\u0004H\u00c6\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J\u000f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J3\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0007H\u00d6\u0001R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionUpgrade;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "chains", "", "", "methods", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Ljava/util/List;Ljava/util/List;)V", "getChains", "()Ljava/util/List;", "getMethods", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SessionUpgrade extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> chains = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> methods = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionUpgrade copy(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> chains, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> methods) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionUpgrade(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> chains, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> methods) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getChains() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getMethods() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Default;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/SequenceLifecycle;", "()V", "walletconnectv2_debug"})
    public static final class Default extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.SequenceLifecycle {
        @org.jetbrains.annotations.NotNull()
        public static final com.walletconnect.walletconnectv2.engine.model.EngineDO.Default INSTANCE = null;
        
        private Default() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notification;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "type", "", "data", "(Ljava/lang/String;Ljava/lang/String;)V", "getData", "()Ljava/lang/String;", "getType", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class Notification extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String type = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String data = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.Notification copy(@org.jetbrains.annotations.NotNull()
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        java.lang.String data) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Notification(@org.jetbrains.annotations.NotNull()
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        java.lang.String data) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getType() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getData() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0080\b\u0018\u00002\u00020\u00012\u00020\u0002:\u0001+B=\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\b\u0010\f\u001a\u0004\u0018\u00010\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001d\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\bH\u00c6\u0003J\u000f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\rH\u00c6\u0003J\t\u0010\"\u001a\u00020\u000fH\u00c6\u0003JM\u0010#\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000fH\u00c6\u0001J\u0013\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\'H\u00d6\u0003J\t\u0010(\u001a\u00020)H\u00d6\u0001J\t\u0010*\u001a\u00020\u000bH\u00d6\u0001R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0013\u0010\f\u001a\u0004\u0018\u00010\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001c\u00a8\u0006,"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "Lcom/walletconnect/walletconnectv2/core/model/type/Sequence;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "expiry", "Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "status", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "accounts", "", "", "peerAppMetaData", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "permissions", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Ljava/util/List;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions;)V", "getAccounts", "()Ljava/util/List;", "getExpiry", "()Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "getPeerAppMetaData", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "getPermissions", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions;", "getStatus", "()Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Permissions", "walletconnectv2_debug"})
    public static final class SettledSession extends com.walletconnect.walletconnectv2.engine.model.EngineDO implements com.walletconnect.walletconnectv2.core.model.type.Sequence {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> accounts = null;
        @org.jetbrains.annotations.Nullable()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData peerAppMetaData = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions permissions = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession copy(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData peerAppMetaData, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions permissions) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SettledSession(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData peerAppMetaData, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions permissions) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO getExpiry() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus getStatus() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getAccounts() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData component5() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData getPeerAppMetaData() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions component6() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions getPermissions() {
            return null;
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0080\b\u0018\u00002\u00020\u0001:\u0003\u001a\u001b\u001cB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001d"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions;", "", "blockchain", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$Blockchain;", "jsonRpc", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$JsonRpc;", "notifications", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$Notifications;", "(Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$Blockchain;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$JsonRpc;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$Notifications;)V", "getBlockchain", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$Blockchain;", "getJsonRpc", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$JsonRpc;", "getNotifications", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$Notifications;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Blockchain", "JsonRpc", "Notifications", "walletconnectv2_debug"})
        public static final class Permissions {
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Blockchain blockchain = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.JsonRpc jsonRpc = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Notifications notifications = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions copy(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Blockchain blockchain, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.JsonRpc jsonRpc, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Notifications notifications) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Permissions(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Blockchain blockchain, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.JsonRpc jsonRpc, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Notifications notifications) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Blockchain component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Blockchain getBlockchain() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.JsonRpc component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.JsonRpc getJsonRpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Notifications component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Notifications getNotifications() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$Blockchain;", "", "chains", "", "", "(Ljava/util/List;)V", "getChains", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
            public static final class Blockchain {
                @org.jetbrains.annotations.NotNull()
                private final java.util.List<java.lang.String> chains = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Blockchain copy(@org.jetbrains.annotations.NotNull()
                java.util.List<java.lang.String> chains) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public Blockchain(@org.jetbrains.annotations.NotNull()
                java.util.List<java.lang.String> chains) {
                    super();
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.util.List<java.lang.String> component1() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.util.List<java.lang.String> getChains() {
                    return null;
                }
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$JsonRpc;", "", "methods", "", "", "(Ljava/util/List;)V", "getMethods", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
            public static final class JsonRpc {
                @org.jetbrains.annotations.NotNull()
                private final java.util.List<java.lang.String> methods = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.JsonRpc copy(@org.jetbrains.annotations.NotNull()
                java.util.List<java.lang.String> methods) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public JsonRpc(@org.jetbrains.annotations.NotNull()
                java.util.List<java.lang.String> methods) {
                    super();
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.util.List<java.lang.String> component1() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.util.List<java.lang.String> getMethods() {
                    return null;
                }
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0015\u0012\u000e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005J\u0011\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u001b\u0010\t\u001a\u00020\u00002\u0010\b\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u00d6\u0001R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SettledSession$Permissions$Notifications;", "", "types", "", "", "(Ljava/util/List;)V", "getTypes", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
            public static final class Notifications {
                @org.jetbrains.annotations.Nullable()
                private final java.util.List<java.lang.String> types = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SettledSession.Permissions.Notifications copy(@org.jetbrains.annotations.Nullable()
                java.util.List<java.lang.String> types) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public Notifications(@org.jetbrains.annotations.Nullable()
                java.util.List<java.lang.String> types) {
                    super();
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.util.List<java.lang.String> component1() {
                    return null;
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.util.List<java.lang.String> getTypes() {
                    return null;
                }
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionState;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "accounts", "", "", "(Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class SessionState extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> accounts = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionState copy(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionState(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> accounts) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getAccounts() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J)\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001b"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$SessionPermissions;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "blockchain", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Blockchain;", "jsonRpc", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpc;", "notification", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notifications;", "(Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Blockchain;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpc;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notifications;)V", "getBlockchain", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Blockchain;", "getJsonRpc", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpc;", "getNotification", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notifications;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    public static final class SessionPermissions extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.Blockchain blockchain = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpc jsonRpc = null;
        @org.jetbrains.annotations.Nullable()
        private final com.walletconnect.walletconnectv2.engine.model.EngineDO.Notifications notification = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.SessionPermissions copy(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.Blockchain blockchain, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpc jsonRpc, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.Notifications notification) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionPermissions(@org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.Blockchain blockchain, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpc jsonRpc, @org.jetbrains.annotations.Nullable()
        com.walletconnect.walletconnectv2.engine.model.EngineDO.Notifications notification) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.Blockchain component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.Blockchain getBlockchain() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpc component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpc getJsonRpc() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.Notifications component3() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.Notifications getNotification() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Blockchain;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "chains", "", "", "(Ljava/util/List;)V", "getChains", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class Blockchain extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> chains = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.Blockchain copy(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> chains) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Blockchain(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> chains) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getChains() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpc;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "methods", "", "", "(Ljava/util/List;)V", "getMethods", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class JsonRpc extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> methods = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpc copy(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> methods) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public JsonRpc(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> methods) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getMethods() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Notifications;", "", "types", "", "", "(Ljava/util/List;)V", "getTypes", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class Notifications {
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> types = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.Notifications copy(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> types) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Notifications(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> types) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getTypes() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\r\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003J7\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n\u00a8\u0006\u001b"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$AppMetaData;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "name", "", "description", "url", "icons", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getDescription", "()Ljava/lang/String;", "getIcons", "()Ljava/util/List;", "getName", "getUrl", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class AppMetaData extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String name = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String description = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String url = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.List<java.lang.String> icons = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.AppMetaData copy(@org.jetbrains.annotations.NotNull()
        java.lang.String name, @org.jetbrains.annotations.NotNull()
        java.lang.String description, @org.jetbrains.annotations.NotNull()
        java.lang.String url, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> icons) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public AppMetaData(@org.jetbrains.annotations.NotNull()
        java.lang.String name, @org.jetbrains.annotations.NotNull()
        java.lang.String description, @org.jetbrains.annotations.NotNull()
        java.lang.String url, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> icons) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getName() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDescription() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUrl() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getIcons() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0002\n\u000b\u00a8\u0006\f"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "()V", "id", "", "getId", "()J", "Error", "JsonRpcError", "JsonRpcResult", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$JsonRpcResult;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$JsonRpcError;", "walletconnectv2_debug"})
    public static abstract class JsonRpcResponse extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        
        private JsonRpcResponse() {
            super();
        }
        
        public abstract long getId();
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b\u00a8\u0006\u0018"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$JsonRpcResult;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse;", "id", "", "jsonrpc", "", "result", "(JLjava/lang/String;Ljava/lang/String;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getResult", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        @com.squareup.moshi.JsonClass(generateAdapter = true)
        public static final class JsonRpcResult extends com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String result = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.JsonRpcResult copy(long id, @org.jetbrains.annotations.NotNull()
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            java.lang.String result) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public JsonRpcResult(long id, @org.jetbrains.annotations.NotNull()
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            java.lang.String result) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getResult() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$JsonRpcError;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse;", "id", "", "jsonrpc", "", "error", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$Error;", "(JLjava/lang/String;Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$Error;)V", "getError", "()Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$Error;", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        @com.squareup.moshi.JsonClass(generateAdapter = true)
        public static final class JsonRpcError extends com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.Error error = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.JsonRpcError copy(long id, @org.jetbrains.annotations.NotNull()
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.Error error) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public JsonRpcError(long id, @org.jetbrains.annotations.NotNull()
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.Error error) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.Error component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.Error getError() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0080\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$JsonRpcResponse$Error;", "", "code", "", "message", "", "(ILjava/lang/String;)V", "getCode", "()I", "getMessage", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "walletconnectv2_debug"})
        public static final class Error {
            private final int code = 0;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String message = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.engine.model.EngineDO.JsonRpcResponse.Error copy(int code, @org.jetbrains.annotations.NotNull()
            java.lang.String message) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Error(int code, @org.jetbrains.annotations.NotNull()
            java.lang.String message) {
                super();
            }
            
            public final int component1() {
                return 0;
            }
            
            public final int getCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMessage() {
                return null;
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J3\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u00a8\u0006\u0019"}, d2 = {"Lcom/walletconnect/walletconnectv2/engine/model/EngineDO$Request;", "Lcom/walletconnect/walletconnectv2/engine/model/EngineDO;", "topic", "", "method", "params", "chainId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getChainId", "()Ljava/lang/String;", "getMethod", "getParams", "getTopic", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class Request extends com.walletconnect.walletconnectv2.engine.model.EngineDO {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String method = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String params = null;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String chainId = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.engine.model.EngineDO.Request copy(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String method, @org.jetbrains.annotations.NotNull()
        java.lang.String params, @org.jetbrains.annotations.Nullable()
        java.lang.String chainId) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Request(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.lang.String method, @org.jetbrains.annotations.NotNull()
        java.lang.String params, @org.jetbrains.annotations.Nullable()
        java.lang.String chainId) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getMethod() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getParams() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component4() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getChainId() {
            return null;
        }
    }
}