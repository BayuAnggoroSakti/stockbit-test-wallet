package com.walletconnect.walletconnectv2.walletconnectv2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001&B-\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rR\u0014\u0010\u0005\u001a\u00020\u0006X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0007\u001a\u00020\bX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\t\u001a\u00020\nX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u000b\u001a\u00020\fX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0014\u0010\u0016\u001a\u00020\u0017X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0014\u0010\u001a\u001a\u00020\u001bX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0014\u0010\u001e\u001a\u00020\u001fX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0014\u0010\"\u001a\u00020#X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010%\u00a8\u0006\'"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;", "Lcom/squareup/sqldelight/TransacterImpl;", "Lcom/walletconnect/walletconnectv2/Database;", "driver", "Lcom/squareup/sqldelight/db/SqlDriver;", "JsonRpcHistoryDaoAdapter", "Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryDao$Adapter;", "MetaDataDaoAdapter", "Lcom/walletconnect/walletconnectv2/storage/data/dao/MetaDataDao$Adapter;", "PairingDaoAdapter", "Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDao$Adapter;", "SessionDaoAdapter", "Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDao$Adapter;", "(Lcom/squareup/sqldelight/db/SqlDriver;Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryDao$Adapter;Lcom/walletconnect/walletconnectv2/storage/data/dao/MetaDataDao$Adapter;Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDao$Adapter;Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDao$Adapter;)V", "getJsonRpcHistoryDaoAdapter$walletconnectv2_debug", "()Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryDao$Adapter;", "getMetaDataDaoAdapter$walletconnectv2_debug", "()Lcom/walletconnect/walletconnectv2/storage/data/dao/MetaDataDao$Adapter;", "getPairingDaoAdapter$walletconnectv2_debug", "()Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDao$Adapter;", "getSessionDaoAdapter$walletconnectv2_debug", "()Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDao$Adapter;", "jsonRpcHistoryQueries", "Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl;", "getJsonRpcHistoryQueries", "()Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl;", "metaDataDaoQueries", "Lcom/walletconnect/walletconnectv2/walletconnectv2/MetaDataDaoQueriesImpl;", "getMetaDataDaoQueries", "()Lcom/walletconnect/walletconnectv2/walletconnectv2/MetaDataDaoQueriesImpl;", "pairingDaoQueries", "Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl;", "getPairingDaoQueries", "()Lcom/walletconnect/walletconnectv2/walletconnectv2/PairingDaoQueriesImpl;", "sessionDaoQueries", "Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl;", "getSessionDaoQueries", "()Lcom/walletconnect/walletconnectv2/walletconnectv2/SessionDaoQueriesImpl;", "Schema", "walletconnectv2_debug"})
final class DatabaseImpl extends com.squareup.sqldelight.TransacterImpl implements com.walletconnect.walletconnectv2.Database {
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.storage.data.dao.JsonRpcHistoryDao.Adapter JsonRpcHistoryDaoAdapter = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.storage.data.dao.MetaDataDao.Adapter MetaDataDaoAdapter = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.storage.data.dao.PairingDao.Adapter PairingDaoAdapter = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.storage.data.dao.SessionDao.Adapter SessionDaoAdapter = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.walletconnectv2.JsonRpcHistoryQueriesImpl jsonRpcHistoryQueries = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.walletconnectv2.MetaDataDaoQueriesImpl metaDataDaoQueries = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.walletconnectv2.PairingDaoQueriesImpl pairingDaoQueries = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.walletconnectv2.SessionDaoQueriesImpl sessionDaoQueries = null;
    
    public DatabaseImpl(@org.jetbrains.annotations.NotNull()
    com.squareup.sqldelight.db.SqlDriver driver, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.JsonRpcHistoryDao.Adapter JsonRpcHistoryDaoAdapter, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.MetaDataDao.Adapter MetaDataDaoAdapter, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.PairingDao.Adapter PairingDaoAdapter, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.SessionDao.Adapter SessionDaoAdapter) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.data.dao.JsonRpcHistoryDao.Adapter getJsonRpcHistoryDaoAdapter$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.data.dao.MetaDataDao.Adapter getMetaDataDaoAdapter$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.data.dao.PairingDao.Adapter getPairingDaoAdapter$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.data.dao.SessionDao.Adapter getSessionDaoAdapter$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.walletconnect.walletconnectv2.walletconnectv2.JsonRpcHistoryQueriesImpl getJsonRpcHistoryQueries() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.walletconnect.walletconnectv2.walletconnectv2.MetaDataDaoQueriesImpl getMetaDataDaoQueries() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.walletconnect.walletconnectv2.walletconnectv2.PairingDaoQueriesImpl getPairingDaoQueries() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.walletconnect.walletconnectv2.walletconnectv2.SessionDaoQueriesImpl getSessionDaoQueries() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J \u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u0004H\u0016R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl$Schema;", "Lcom/squareup/sqldelight/db/SqlDriver$Schema;", "()V", "version", "", "getVersion", "()I", "create", "", "driver", "Lcom/squareup/sqldelight/db/SqlDriver;", "migrate", "oldVersion", "newVersion", "walletconnectv2_debug"})
    public static final class Schema implements com.squareup.sqldelight.db.SqlDriver.Schema {
        @org.jetbrains.annotations.NotNull()
        public static final com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl.Schema INSTANCE = null;
        
        private Schema() {
            super();
        }
        
        @java.lang.Override()
        public int getVersion() {
            return 0;
        }
        
        @java.lang.Override()
        public void create(@org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.db.SqlDriver driver) {
        }
        
        @java.lang.Override()
        public void migrate(@org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.db.SqlDriver driver, int oldVersion, int newVersion) {
        }
    }
}