package com.walletconnect.walletconnectv2.core.exceptions.peer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"NO_SEQUENCE_CODE", "", "UNAUTHORIZED_PEER_CODE", "walletconnectv2_debug"})
public final class PeerCodesKt {
    public static final long NO_SEQUENCE_CODE = 1302L;
    public static final long UNAUTHORIZED_PEER_CODE = 3005L;
}