package com.walletconnect.walletconnectv2.core.exceptions.peer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"PEER_IS_ALSO_CONTROLLER_MESSAGE", "", "PEER_IS_ALSO_NON_CONTROLLER_MESSAGE", "walletconnectv2_debug"})
public final class PeerMessagesKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PEER_IS_ALSO_NON_CONTROLLER_MESSAGE = "Unauthorized: peer is also non-controller";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PEER_IS_ALSO_CONTROLLER_MESSAGE = "Unauthorized: peer is also controller";
}