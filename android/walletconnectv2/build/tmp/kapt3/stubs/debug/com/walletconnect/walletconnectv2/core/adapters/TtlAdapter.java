package com.walletconnect.walletconnectv2.core.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c0\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\fB\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0017J\u001c\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u0017\u00a8\u0006\r"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/adapters/TtlAdapter;", "Lcom/squareup/moshi/JsonAdapter;", "Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "()V", "fromJson", "reader", "Lcom/squareup/moshi/JsonReader;", "toJson", "", "writer", "Lcom/squareup/moshi/JsonWriter;", "value", "Qualifier", "walletconnectv2_debug"})
public final class TtlAdapter extends com.squareup.moshi.JsonAdapter<com.walletconnect.walletconnectv2.core.model.vo.TtlVO> {
    @org.jetbrains.annotations.NotNull()
    public static final com.walletconnect.walletconnectv2.core.adapters.TtlAdapter INSTANCE = null;
    
    private TtlAdapter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0087\u0002\u0018\u00002\u00020\u0001B\u0000\u00a8\u0006\u0002"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/adapters/TtlAdapter$Qualifier;", "", "walletconnectv2_debug"})
    @java.lang.annotation.Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
    @com.squareup.moshi.JsonQualifier()
    @kotlin.annotation.Retention(value = kotlin.annotation.AnnotationRetention.RUNTIME)
    public static abstract @interface Qualifier {
    }
}