package com.walletconnect.walletconnectv2.walletconnectv2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0003+,-B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00170\n2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J&\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u001a0\n2\u0006\u0010\u0015\u001a\u00020\u00162\u000e\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00160\u001cH\u0016J\u00c2\u0001\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u001d0\n\"\b\b\u0000\u0010\u001d*\u00020\u001e2\u0006\u0010\u0015\u001a\u00020\u00162\u000e\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00160\u001c2\u008f\u0001\u0010\u001f\u001a\u008a\u0001\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(\u0018\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(\u0015\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(\u001b\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(#\u0012\u0013\u0012\u00110$\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(%\u0012\u0013\u0012\u00110&\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(\'\u0012\u0004\u0012\u0002H\u001d0 H\u0016J&\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020(0\n2\u0006\u0010\u0015\u001a\u00020\u00162\u000e\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00160\u001cH\u0016J\u00c2\u0001\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u001d0\n\"\b\b\u0000\u0010\u001d*\u00020\u001e2\u0006\u0010\u0015\u001a\u00020\u00162\u000e\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00160\u001c2\u008f\u0001\u0010\u001f\u001a\u008a\u0001\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(\u0018\u0012\u0013\u0012\u00110\u0016\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(\u0015\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(\u001b\u0012\u0015\u0012\u0013\u0018\u00010\u0016\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(#\u0012\u0013\u0012\u00110$\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(%\u0012\u0013\u0012\u00110&\u00a2\u0006\f\b!\u0012\b\b\"\u0012\u0004\b\b(\'\u0012\u0004\u0012\u0002H\u001d0 H\u0016J<\u0010)\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0015\u001a\u00020\u00162\b\u0010\u001b\u001a\u0004\u0018\u00010\u00162\b\u0010#\u001a\u0004\u0018\u00010\u00162\u0006\u0010%\u001a\u00020$2\u0006\u0010\'\u001a\u00020&H\u0016J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00190\nH\u0016J\u0018\u0010*\u001a\u00020\u00142\u0006\u0010%\u001a\u00020$2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\r\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u001e\u0010\u000f\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u001e\u0010\u0011\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\tX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\f\u00a8\u0006."}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl;", "Lcom/squareup/sqldelight/TransacterImpl;", "Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryQueries;", "database", "Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;", "driver", "Lcom/squareup/sqldelight/db/SqlDriver;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/DatabaseImpl;Lcom/squareup/sqldelight/db/SqlDriver;)V", "doesJsonRpcNotExist", "", "Lcom/squareup/sqldelight/Query;", "getDoesJsonRpcNotExist$walletconnectv2_debug", "()Ljava/util/List;", "getJsonRpcRequestsDaos", "getGetJsonRpcRequestsDaos$walletconnectv2_debug", "getJsonRpcRespondsDaos", "getGetJsonRpcRespondsDaos$walletconnectv2_debug", "selectLastInsertedRowId", "getSelectLastInsertedRowId$walletconnectv2_debug", "deleteJsonRpcHistory", "", "topic", "", "", "request_id", "", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetJsonRpcRequestsDaos;", "method", "", "T", "", "mapper", "Lkotlin/Function6;", "Lkotlin/ParameterName;", "name", "body", "Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "status", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "controller_type", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetJsonRpcRespondsDaos;", "insertJsonRpcHistory", "updateJsonRpcHistory", "DoesJsonRpcNotExistQuery", "GetJsonRpcRequestsDaosQuery", "GetJsonRpcRespondsDaosQuery", "walletconnectv2_debug"})
final class JsonRpcHistoryQueriesImpl extends com.squareup.sqldelight.TransacterImpl implements com.walletconnect.walletconnectv2.storage.data.dao.JsonRpcHistoryQueries {
    private final com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl database = null;
    private final com.squareup.sqldelight.db.SqlDriver driver = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> doesJsonRpcNotExist = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> selectLastInsertedRowId = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getJsonRpcRequestsDaos = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.squareup.sqldelight.Query<?>> getJsonRpcRespondsDaos = null;
    
    public JsonRpcHistoryQueriesImpl(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.walletconnectv2.DatabaseImpl database, @org.jetbrains.annotations.NotNull()
    com.squareup.sqldelight.db.SqlDriver driver) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getDoesJsonRpcNotExist$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getSelectLastInsertedRowId$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetJsonRpcRequestsDaos$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.squareup.sqldelight.Query<?>> getGetJsonRpcRespondsDaos$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<java.lang.Boolean> doesJsonRpcNotExist(long request_id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<java.lang.Long> selectLastInsertedRowId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getJsonRpcRequestsDaos(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.String> method, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function6<? super java.lang.Long, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? extends T> mapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetJsonRpcRequestsDaos> getJsonRpcRequestsDaos(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.String> method) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getJsonRpcRespondsDaos(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.String> method, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function6<? super java.lang.Long, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? extends T> mapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetJsonRpcRespondsDaos> getJsonRpcRespondsDaos(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.String> method) {
        return null;
    }
    
    @java.lang.Override()
    public void insertJsonRpcHistory(long request_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.lang.String method, @org.jetbrains.annotations.Nullable()
    java.lang.String body, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type) {
    }
    
    @java.lang.Override()
    public void updateJsonRpcHistory(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus status, long request_id) {
    }
    
    @java.lang.Override()
    public void deleteJsonRpcHistory(@org.jetbrains.annotations.NotNull()
    java.lang.String topic) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\f\u001a\u00020\bH\u0016J\b\u0010\r\u001a\u00020\u000eH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000f"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl$DoesJsonRpcNotExistQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "request_id", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl;JLkotlin/jvm/functions/Function1;)V", "getRequest_id", "()J", "execute", "toString", "", "walletconnectv2_debug"})
    final class DoesJsonRpcNotExistQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        private final long request_id = 0L;
        
        public DoesJsonRpcNotExistQuery(long request_id, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        public final long getRequest_id() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B1\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0007\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00028\u00000\t\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\u0010\u001a\u00020\nH\u0016J\b\u0010\u0011\u001a\u00020\u0005H\u0016R\u0019\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0012"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl$GetJsonRpcRequestsDaosQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "method", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl;Ljava/lang/String;Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)V", "getMethod", "()Ljava/util/Collection;", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class GetJsonRpcRequestsDaosQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.Collection<java.lang.String> method = null;
        
        public GetJsonRpcRequestsDaosQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.util.Collection<java.lang.String> method, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Collection<java.lang.String> getMethod() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0082\u0004\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B1\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0007\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00028\u00000\t\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\u0010\u001a\u00020\nH\u0016J\b\u0010\u0011\u001a\u00020\u0005H\u0016R\u0019\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0012"}, d2 = {"Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl$GetJsonRpcRespondsDaosQuery;", "T", "", "Lcom/squareup/sqldelight/Query;", "topic", "", "method", "", "mapper", "Lkotlin/Function1;", "Lcom/squareup/sqldelight/db/SqlCursor;", "(Lcom/walletconnect/walletconnectv2/walletconnectv2/JsonRpcHistoryQueriesImpl;Ljava/lang/String;Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)V", "getMethod", "()Ljava/util/Collection;", "getTopic", "()Ljava/lang/String;", "execute", "toString", "walletconnectv2_debug"})
    final class GetJsonRpcRespondsDaosQuery<T extends java.lang.Object> extends com.squareup.sqldelight.Query<T> {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String topic = null;
        @org.jetbrains.annotations.NotNull()
        private final java.util.Collection<java.lang.String> method = null;
        
        public GetJsonRpcRespondsDaosQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String topic, @org.jetbrains.annotations.NotNull()
        java.util.Collection<java.lang.String> method, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.squareup.sqldelight.db.SqlCursor, ? extends T> mapper) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Collection<java.lang.String> getMethod() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.squareup.sqldelight.db.SqlCursor execute() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
    }
}