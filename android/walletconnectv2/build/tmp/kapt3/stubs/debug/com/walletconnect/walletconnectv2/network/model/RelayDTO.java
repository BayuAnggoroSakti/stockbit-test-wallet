package com.walletconnect.walletconnectv2.network.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0005\u000b\f\r\u000e\u000fB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\n\u0082\u0001\u0004\u0010\u0011\u0012\u0013\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO;", "", "()V", "id", "", "getId", "()J", "jsonrpc", "", "getJsonrpc", "()Ljava/lang/String;", "Error", "Publish", "Subscribe", "Subscription", "Unsubscribe", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe;", "walletconnectv2_debug"})
public abstract class RelayDTO {
    
    private RelayDTO() {
        super();
    }
    
    public abstract long getId();
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.String getJsonrpc();
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO;", "()V", "Acknowledgement", "JsonRpcError", "Request", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Request;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Acknowledgement;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$JsonRpcError;", "walletconnectv2_debug"})
    public static abstract class Publish extends com.walletconnect.walletconnectv2.network.model.RelayDTO {
        
        private Publish() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0081\b\u0018\u00002\u00020\u0001:\u0001\u001dB-\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\bH\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00052\b\b\u0003\u0010\u0007\u001a\u00020\bH\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001e"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Request;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish;", "id", "", "jsonrpc", "", "method", "params", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Request$Params;", "(JLjava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Request$Params;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getMethod", "getParams", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Request$Params;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Params", "walletconnectv2_debug"})
        @com.squareup.moshi.JsonClass(generateAdapter = true)
        public static final class Request extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String method = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Request.Params params = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Request copy(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "method")
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "params")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Request.Params params) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Request(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "method")
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "params")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Request.Params params) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMethod() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Request.Params component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Request.Params getParams() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0013\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B/\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0007H\u00c6\u0003J\u0010\u0010\u0017\u001a\u0004\u0018\u00010\tH\u00c6\u0003\u00a2\u0006\u0002\u0010\u000eJ8\u0010\u0018\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00072\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\tH\u00c6\u0001\u00a2\u0006\u0002\u0010\u0019J\u0013\u0010\u001a\u001a\u00020\t2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0015\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\n\n\u0002\u0010\u000f\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006\u001f"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Request$Params;", "", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "message", "", "ttl", "Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "prompt", "", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;Ljava/lang/Boolean;)V", "getMessage", "()Ljava/lang/String;", "getPrompt", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getTtl", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "component1", "component2", "component3", "component4", "copy", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;Ljava/lang/Boolean;)Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Request$Params;", "equals", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
            @com.squareup.moshi.JsonClass(generateAdapter = true)
            public static final class Params {
                @org.jetbrains.annotations.NotNull()
                @com.walletconnect.walletconnectv2.core.adapters.TopicAdapter.Qualifier()
                private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
                @org.jetbrains.annotations.NotNull()
                private final java.lang.String message = null;
                @org.jetbrains.annotations.NotNull()
                @com.walletconnect.walletconnectv2.core.adapters.TtlAdapter.Qualifier()
                private final com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl = null;
                @org.jetbrains.annotations.Nullable()
                private final java.lang.Boolean prompt = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Request.Params copy(@org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "topic")
                com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "message")
                java.lang.String message, @org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "ttl")
                com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl, @org.jetbrains.annotations.Nullable()
                @com.squareup.moshi.Json(name = "prompt")
                java.lang.Boolean prompt) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public Params(@org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "topic")
                com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "message")
                java.lang.String message, @org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "ttl")
                com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl, @org.jetbrains.annotations.Nullable()
                @com.squareup.moshi.Json(name = "prompt")
                java.lang.Boolean prompt) {
                    super();
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String getMessage() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.TtlVO component3() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.TtlVO getTtl() {
                    return null;
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Boolean component4() {
                    return null;
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Boolean getPrompt() {
                    return null;
                }
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0019"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Acknowledgement;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish;", "id", "", "jsonrpc", "", "result", "", "(JLjava/lang/String;Z)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getResult", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Acknowledgement extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            private final boolean result = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Acknowledgement copy(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @com.squareup.moshi.Json(name = "result")
            boolean result) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Acknowledgement(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @com.squareup.moshi.Json(name = "result")
            boolean result) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            public final boolean component3() {
                return false;
            }
            
            public final boolean getResult() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$JsonRpcError;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish;", "jsonrpc", "", "error", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "id", "", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;J)V", "getError", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class JsonRpcError extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error = null;
            private final long id = 0L;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.JsonRpcError copy(@org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "error")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error, @com.squareup.moshi.Json(name = "id")
            long id) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public JsonRpcError(@org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "error")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error, @com.squareup.moshi.Json(name = "id")
            long id) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error getError() {
                return null;
            }
            
            public final long component3() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO;", "()V", "Acknowledgement", "JsonRpcError", "Request", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Request;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Acknowledgement;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$JsonRpcError;", "walletconnectv2_debug"})
    public static abstract class Subscribe extends com.walletconnect.walletconnectv2.network.model.RelayDTO {
        
        private Subscribe() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0081\b\u0018\u00002\u00020\u0001:\u0001\u001dB-\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\bH\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00052\b\b\u0003\u0010\u0007\u001a\u00020\bH\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001e"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Request;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe;", "id", "", "jsonrpc", "", "method", "params", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Request$Params;", "(JLjava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Request$Params;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getMethod", "getParams", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Request$Params;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Params", "walletconnectv2_debug"})
        @com.squareup.moshi.JsonClass(generateAdapter = true)
        public static final class Request extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String method = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Request.Params params = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Request copy(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "method")
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "params")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Request.Params params) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Request(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "method")
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "params")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Request.Params params) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMethod() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Request.Params component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Request.Params getParams() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0081\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Request$Params;", "", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;)V", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "walletconnectv2_debug"})
            @com.squareup.moshi.JsonClass(generateAdapter = true)
            public static final class Params {
                @org.jetbrains.annotations.NotNull()
                @com.walletconnect.walletconnectv2.core.adapters.TopicAdapter.Qualifier()
                private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Request.Params copy(@org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "topic")
                com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public Params(@org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "topic")
                com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
                    super();
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
                    return null;
                }
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Acknowledgement;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe;", "id", "", "jsonrpc", "", "result", "Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;", "(JLjava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getResult", "()Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Acknowledgement extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            @com.walletconnect.walletconnectv2.core.adapters.SubscriptionIdAdapter.Qualifier()
            private final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO result = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Acknowledgement copy(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "result")
            com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO result) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Acknowledgement(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "result")
            com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO result) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO getResult() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$JsonRpcError;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe;", "jsonrpc", "", "error", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "id", "", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;J)V", "getError", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class JsonRpcError extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error = null;
            private final long id = 0L;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.JsonRpcError copy(@org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "error")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error, @com.squareup.moshi.Json(name = "id")
            long id) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public JsonRpcError(@org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "error")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error, @com.squareup.moshi.Json(name = "id")
            long id) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error getError() {
                return null;
            }
            
            public final long component3() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO;", "()V", "Acknowledgement", "JsonRpcError", "Request", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Acknowledgement;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$JsonRpcError;", "walletconnectv2_debug"})
    public static abstract class Subscription extends com.walletconnect.walletconnectv2.network.model.RelayDTO {
        
        private Subscription() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0081\b\u0018\u00002\u00020\u0001:\u0001#B-\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\bH\u00c6\u0003J1\u0010\u001b\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00052\b\b\u0003\u0010\u0007\u001a\u00020\bH\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006$"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription;", "id", "", "jsonrpc", "", "method", "params", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request$Params;", "(JLjava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request$Params;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "message", "getMessage", "getMethod", "getParams", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request$Params;", "subscriptionTopic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getSubscriptionTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Params", "walletconnectv2_debug"})
        @com.squareup.moshi.JsonClass(generateAdapter = true)
        public static final class Request extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String method = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params params = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO subscriptionTopic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String message = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request copy(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "method")
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "params")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params params) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Request(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "method")
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "params")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params params) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMethod() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params getParams() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getSubscriptionTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMessage() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001:\u0001\u0015B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0016"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request$Params;", "", "subscriptionId", "Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;", "subscriptionData", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request$Params$SubscriptionData;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request$Params$SubscriptionData;)V", "getSubscriptionData", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request$Params$SubscriptionData;", "getSubscriptionId", "()Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "SubscriptionData", "walletconnectv2_debug"})
            @com.squareup.moshi.JsonClass(generateAdapter = true)
            public static final class Params {
                @org.jetbrains.annotations.NotNull()
                @com.walletconnect.walletconnectv2.core.adapters.SubscriptionIdAdapter.Qualifier()
                private final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO subscriptionId = null;
                @org.jetbrains.annotations.NotNull()
                private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params.SubscriptionData subscriptionData = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params copy(@org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "id")
                com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO subscriptionId, @org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "data")
                com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params.SubscriptionData subscriptionData) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public Params(@org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "id")
                com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO subscriptionId, @org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "data")
                com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params.SubscriptionData subscriptionData) {
                    super();
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO component1() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO getSubscriptionId() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params.SubscriptionData component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params.SubscriptionData getSubscriptionData() {
                    return null;
                }
                
                @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request$Params$SubscriptionData;", "", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "message", "", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
                @com.squareup.moshi.JsonClass(generateAdapter = true)
                public static final class SubscriptionData {
                    @org.jetbrains.annotations.NotNull()
                    @com.walletconnect.walletconnectv2.core.adapters.TopicAdapter.Qualifier()
                    private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
                    @org.jetbrains.annotations.NotNull()
                    private final java.lang.String message = null;
                    
                    @org.jetbrains.annotations.NotNull()
                    public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request.Params.SubscriptionData copy(@org.jetbrains.annotations.NotNull()
                    @com.squareup.moshi.Json(name = "topic")
                    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
                    @com.squareup.moshi.Json(name = "message")
                    java.lang.String message) {
                        return null;
                    }
                    
                    @java.lang.Override()
                    public boolean equals(@org.jetbrains.annotations.Nullable()
                    java.lang.Object other) {
                        return false;
                    }
                    
                    @java.lang.Override()
                    public int hashCode() {
                        return 0;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    @java.lang.Override()
                    public java.lang.String toString() {
                        return null;
                    }
                    
                    public SubscriptionData(@org.jetbrains.annotations.NotNull()
                    @com.squareup.moshi.Json(name = "topic")
                    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
                    @com.squareup.moshi.Json(name = "message")
                    java.lang.String message) {
                        super();
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
                        return null;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
                        return null;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    public final java.lang.String component2() {
                        return null;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    public final java.lang.String getMessage() {
                        return null;
                    }
                }
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0019"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Acknowledgement;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription;", "id", "", "jsonrpc", "", "result", "", "(JLjava/lang/String;Z)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getResult", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Acknowledgement extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            private final boolean result = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Acknowledgement copy(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @com.squareup.moshi.Json(name = "result")
            boolean result) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Acknowledgement(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @com.squareup.moshi.Json(name = "result")
            boolean result) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            public final boolean component3() {
                return false;
            }
            
            public final boolean getResult() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$JsonRpcError;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription;", "jsonrpc", "", "error", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "id", "", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;J)V", "getError", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class JsonRpcError extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error = null;
            private final long id = 0L;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.JsonRpcError copy(@org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "error")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error, @com.squareup.moshi.Json(name = "id")
            long id) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public JsonRpcError(@org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "error")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error, @com.squareup.moshi.Json(name = "id")
            long id) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error getError() {
                return null;
            }
            
            public final long component3() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO;", "()V", "Acknowledgement", "JsonRpcError", "Request", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Request;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Acknowledgement;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$JsonRpcError;", "walletconnectv2_debug"})
    public static abstract class Unsubscribe extends com.walletconnect.walletconnectv2.network.model.RelayDTO {
        
        private Unsubscribe() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0080\b\u0018\u00002\u00020\u0001:\u0001\u001dB-\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\bH\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00052\b\b\u0003\u0010\u0007\u001a\u00020\bH\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001e"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Request;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe;", "id", "", "jsonrpc", "", "method", "params", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Request$Params;", "(JLjava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Request$Params;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getMethod", "getParams", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Request$Params;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Params", "walletconnectv2_debug"})
        public static final class Request extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String method = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Request.Params params = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Request copy(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "method")
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "params")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Request.Params params) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Request(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "method")
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "params")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Request.Params params) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMethod() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Request.Params component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Request.Params getParams() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Request$Params;", "", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "subscriptionId", "Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;)V", "getSubscriptionId", "()Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "walletconnectv2_debug"})
            public static final class Params {
                @org.jetbrains.annotations.NotNull()
                @com.walletconnect.walletconnectv2.core.adapters.TopicAdapter.Qualifier()
                private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
                @org.jetbrains.annotations.NotNull()
                @com.walletconnect.walletconnectv2.core.adapters.SubscriptionIdAdapter.Qualifier()
                private final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO subscriptionId = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Request.Params copy(@org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "topic")
                com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "id")
                com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO subscriptionId) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public Params(@org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "topic")
                com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
                @com.squareup.moshi.Json(name = "id")
                com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO subscriptionId) {
                    super();
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO getSubscriptionId() {
                    return null;
                }
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0019"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Acknowledgement;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe;", "id", "", "jsonrpc", "", "result", "", "(JLjava/lang/String;Z)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getResult", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Acknowledgement extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe {
            private final long id = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            private final boolean result = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Acknowledgement copy(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @com.squareup.moshi.Json(name = "result")
            boolean result) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Acknowledgement(@com.squareup.moshi.Json(name = "id")
            long id, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @com.squareup.moshi.Json(name = "result")
            boolean result) {
                super();
            }
            
            public final long component1() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            public final boolean component3() {
                return false;
            }
            
            public final boolean getResult() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$JsonRpcError;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe;", "jsonrpc", "", "error", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "id", "", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;J)V", "getError", "()Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class JsonRpcError extends com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error = null;
            private final long id = 0L;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.JsonRpcError copy(@org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "error")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error, @com.squareup.moshi.Json(name = "id")
            long id) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public JsonRpcError(@org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "jsonrpc")
            java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
            @com.squareup.moshi.Json(name = "error")
            com.walletconnect.walletconnectv2.network.model.RelayDTO.Error error, @com.squareup.moshi.Json(name = "id")
            long id) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String getJsonrpc() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error getError() {
                return null;
            }
            
            public final long component3() {
                return 0L;
            }
            
            @java.lang.Override()
            public long getId() {
                return 0L;
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u000f\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b\u00a8\u0006\u0016"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Error;", "", "code", "", "message", "", "(JLjava/lang/String;)V", "getCode", "()J", "errorMessage", "getErrorMessage", "()Ljava/lang/String;", "getMessage", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class Error {
        private final long code = 0L;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String message = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String errorMessage = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.network.model.RelayDTO.Error copy(@com.squareup.moshi.Json(name = "code")
        long code, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "message")
        java.lang.String message) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Error(@com.squareup.moshi.Json(name = "code")
        long code, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "message")
        java.lang.String message) {
            super();
        }
        
        public final long component1() {
            return 0L;
        }
        
        public final long getCode() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getMessage() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getErrorMessage() {
            return null;
        }
    }
}