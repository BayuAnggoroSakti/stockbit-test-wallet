package com.walletconnect.walletconnectv2.util;

import java.lang.System;

@kotlin.jvm.JvmName(name = "Utils")
@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000.\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001H\u0002\u001a\b\u0010\u0007\u001a\u00020\bH\u0000\u001a\u0010\u0010\t\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0001H\u0002\u001a\b\u0010\n\u001a\u00020\bH\u0000\u001a\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0000\u001a\f\u0010\u000f\u001a\u00020\u0001*\u00020\fH\u0000\u001a\f\u0010\u0010\u001a\u00020\f*\u00020\u0001H\u0000\u001a\f\u0010\u0011\u001a\u00020\u0005*\u00020\u0012H\u0000\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00018@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0002\u0010\u0003\u00a8\u0006\u0013"}, d2 = {"hexToUtf8", "", "getHexToUtf8", "(Ljava/lang/String;)Ljava/lang/String;", "containsHexPrefix", "", "input", "generateId", "", "getHexPrefix", "pendingSequenceExpirySeconds", "randomBytes", "", "size", "", "bytesToHex", "hexToBytes", "isSequenceValid", "Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "walletconnectv2_debug"})
public final class Utils {
    
    private static final java.lang.String getHexPrefix(java.lang.String input) {
        return null;
    }
    
    private static final boolean containsHexPrefix(java.lang.String input) {
        return false;
    }
}