package com.walletconnect.walletconnectv2.storage.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b$\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001;Bs\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0013J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\t\u0010(\u001a\u00020\u0005H\u00c6\u0003J\u0011\u0010)\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011H\u00c6\u0003J\u0010\u0010*\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u001cJ\t\u0010+\u001a\u00020\u0005H\u00c6\u0003J\t\u0010,\u001a\u00020\u0005H\u00c6\u0003J\t\u0010-\u001a\u00020\u0003H\u00c6\u0003J\t\u0010.\u001a\u00020\tH\u00c6\u0003J\t\u0010/\u001a\u00020\u000bH\u00c6\u0003J\t\u00100\u001a\u00020\u0005H\u00c6\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u0094\u0001\u00103\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u00052\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00112\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001\u00a2\u0006\u0002\u00104J\u0013\u00105\u001a\u0002062\b\u00107\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00108\u001a\u000209H\u00d6\u0001J\b\u0010:\u001a\u00020\u0005H\u0016R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0019R\u0015\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u001d\u001a\u0004\b\u001b\u0010\u001cR\u0013\u0010\r\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0015R\u0019\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0011\u0010\u000f\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0015R\u0011\u0010\f\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0015R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0015\u00a8\u0006<"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDao;", "", "id", "", "topic", "", "uri", "expiry", "status", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "controller_type", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "self_participant", "peer_participant", "controller_key", "relay_protocol", "permissions", "", "metadata_id", "(JLjava/lang/String;Ljava/lang/String;JLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;)V", "getController_key", "()Ljava/lang/String;", "getController_type", "()Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "getExpiry", "()J", "getId", "getMetadata_id", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getPeer_participant", "getPermissions", "()Ljava/util/List;", "getRelay_protocol", "getSelf_participant", "getStatus", "()Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "getTopic", "getUri", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(JLjava/lang/String;Ljava/lang/String;JLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;)Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDao;", "equals", "", "other", "hashCode", "", "toString", "Adapter", "walletconnectv2_debug"})
public final class PairingDao {
    private final long id = 0L;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String topic = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String uri = null;
    private final long expiry = 0L;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String self_participant = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String peer_participant = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String controller_key = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String relay_protocol = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.String> permissions = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Long metadata_id = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.data.dao.PairingDao copy(long id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.lang.String uri, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    public PairingDao(long id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.lang.String uri, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id) {
        super();
    }
    
    public final long component1() {
        return 0L;
    }
    
    public final long getId() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTopic() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUri() {
        return null;
    }
    
    public final long component4() {
        return 0L;
    }
    
    public final long getExpiry() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus getStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType getController_type() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSelf_participant() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPeer_participant() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getController_key() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRelay_protocol() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getPermissions() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getMetadata_id() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0006\u0018\u00002\u00020\u0001BG\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0018\u0010\b\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\t\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0002\u0010\nR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR#\u0010\b\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\t\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f\u00a8\u0006\u000f"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/PairingDao$Adapter;", "", "statusAdapter", "Lcom/squareup/sqldelight/ColumnAdapter;", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "", "controller_typeAdapter", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "permissionsAdapter", "", "(Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;)V", "getController_typeAdapter", "()Lcom/squareup/sqldelight/ColumnAdapter;", "getPermissionsAdapter", "getStatusAdapter", "walletconnectv2_debug"})
    public static final class Adapter {
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, java.lang.String> statusAdapter = null;
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> controller_typeAdapter = null;
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> permissionsAdapter = null;
        
        public Adapter(@org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, java.lang.String> statusAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> controller_typeAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> permissionsAdapter) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, java.lang.String> getStatusAdapter() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> getController_typeAdapter() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<java.util.List<java.lang.String>, java.lang.String> getPermissionsAdapter() {
            return null;
        }
    }
}