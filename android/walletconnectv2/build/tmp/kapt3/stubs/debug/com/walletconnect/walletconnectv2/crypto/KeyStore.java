package com.walletconnect.walletconnectv2.crypto;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u001c\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J \u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH&\u00a8\u0006\f"}, d2 = {"Lcom/walletconnect/walletconnectv2/crypto/KeyStore;", "", "deleteKeys", "", "tag", "", "getKeys", "Lkotlin/Pair;", "setKey", "key1", "Lcom/walletconnect/walletconnectv2/core/model/vo/Key;", "key2", "walletconnectv2_debug"})
public abstract interface KeyStore {
    
    public abstract void setKey(@org.jetbrains.annotations.NotNull()
    java.lang.String tag, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.Key key1, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.Key key2);
    
    @org.jetbrains.annotations.NotNull()
    public abstract kotlin.Pair<java.lang.String, java.lang.String> getKeys(@org.jetbrains.annotations.NotNull()
    java.lang.String tag);
    
    public abstract void deleteKeys(@org.jetbrains.annotations.NotNull()
    java.lang.String tag);
}