package com.walletconnect.walletconnectv2.crypto.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u000b\b\u0000\u0018\u0000 +2\u00020\u0001:\u0001+B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0006H\u0016\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002J1\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\n0\u000e2\u0006\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0006H\u0016\u00f8\u0001\u0000\u00f8\u0001\u0002\u00a2\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00060\u000e2\u0006\u0010\u0015\u001a\u00020\nH\u0016\u00f8\u0001\u0000J$\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00170\u000e2\u0006\u0010\u0018\u001a\u00020\u0019H\u0000\u00f8\u0001\u0000\u00a2\u0006\u0002\b\u001aJ%\u0010\u001b\u001a\u00020\f2\u0006\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\u0006H\u0000\u00f8\u0001\u0000\u00f8\u0001\u0002\u00a2\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\fH\u0016J-\u0010#\u001a\u00020!2\u0006\u0010\u000b\u001a\u00020\u000f2\u0006\u0010$\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\nH\u0016\u00f8\u0001\u0000\u00f8\u0001\u0002\u00a2\u0006\u0004\b%\u0010&J%\u0010\'\u001a\u00020!2\u0006\u0010$\u001a\u00020\u00062\u0006\u0010(\u001a\u00020\u0017H\u0000\u00f8\u0001\u0000\u00f8\u0001\u0002\u00a2\u0006\u0004\b)\u0010*R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b\u00a1\u001e0\u0001\u00a8\u0006,"}, d2 = {"Lcom/walletconnect/walletconnectv2/crypto/data/repository/BouncyCastleCryptoRepository;", "Lcom/walletconnect/walletconnectv2/crypto/CryptoRepository;", "keyChain", "Lcom/walletconnect/walletconnectv2/crypto/KeyStore;", "(Lcom/walletconnect/walletconnectv2/crypto/KeyStore;)V", "generateKeyPair", "Lcom/walletconnect/walletconnectv2/core/model/vo/PublicKey;", "generateKeyPair-oW5vskc", "()Ljava/lang/String;", "generateTopic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "sharedKey", "", "generateTopicAndSharedKey", "Lkotlin/Pair;", "Lcom/walletconnect/walletconnectv2/core/model/vo/SharedKey;", "self", "peer", "generateTopicAndSharedKey-xakSQ8w", "(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Pair;", "getKeyAgreement", "topic", "getKeyPair", "Lcom/walletconnect/walletconnectv2/core/model/vo/PrivateKey;", "wcKey", "Lcom/walletconnect/walletconnectv2/core/model/vo/Key;", "getKeyPair$walletconnectv2_debug", "getSharedKey", "selfPrivate", "peerPublic", "getSharedKey-SJ5hUIo$walletconnectv2_debug", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", "removeKeys", "", "tag", "setEncryptionKeys", "publicKey", "setEncryptionKeys-SxomFNM", "(Ljava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;)V", "setKeyPair", "privateKey", "setKeyPair-NcClpbk$walletconnectv2_debug", "(Ljava/lang/String;Ljava/lang/String;)V", "Companion", "walletconnectv2_debug"})
public final class BouncyCastleCryptoRepository implements com.walletconnect.walletconnectv2.crypto.CryptoRepository {
    private final com.walletconnect.walletconnectv2.crypto.KeyStore keyChain = null;
    @org.jetbrains.annotations.NotNull()
    private static final com.walletconnect.walletconnectv2.crypto.data.repository.BouncyCastleCryptoRepository.Companion Companion = null;
    @java.lang.Deprecated()
    private static final int KEY_SIZE = 32;
    @java.lang.Deprecated()
    private static final java.lang.String SHA_256 = "SHA-256";
    
    public BouncyCastleCryptoRepository(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.crypto.KeyStore keyChain) {
        super();
    }
    
    @java.lang.Override()
    public void removeKeys(@org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kotlin.Pair<com.walletconnect.walletconnectv2.core.model.vo.SharedKey, com.walletconnect.walletconnectv2.core.model.vo.PublicKey> getKeyAgreement(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.Pair<com.walletconnect.walletconnectv2.core.model.vo.PublicKey, com.walletconnect.walletconnectv2.core.model.vo.PrivateKey> getKeyPair$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.Key wcKey) {
        return null;
    }
    
    private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO generateTopic(java.lang.String sharedKey) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/crypto/data/repository/BouncyCastleCryptoRepository$Companion;", "", "()V", "KEY_SIZE", "", "SHA_256", "", "walletconnectv2_debug"})
    static final class Companion {
        
        private Companion() {
            super();
        }
    }
}