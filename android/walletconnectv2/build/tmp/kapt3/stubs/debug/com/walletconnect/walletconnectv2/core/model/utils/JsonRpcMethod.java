package com.walletconnect.walletconnectv2.core.model.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0010\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u00020\u00048FX\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/utils/JsonRpcMethod;", "", "()V", "WC_PAIRING_APPROVE", "", "WC_PAIRING_DELETE", "WC_PAIRING_NOTIFICATION", "WC_PAIRING_PAYLOAD", "WC_PAIRING_PING", "WC_PAIRING_REJECT", "WC_PAIRING_UPDATE", "WC_SESSION_APPROVE", "WC_SESSION_DELETE", "WC_SESSION_NOTIFICATION", "WC_SESSION_PAYLOAD", "WC_SESSION_PING", "WC_SESSION_PROPOSE", "WC_SESSION_REJECT", "WC_SESSION_UPDATE", "WC_SESSION_UPGRADE", "walletconnectv2_debug"})
public final class JsonRpcMethod {
    @org.jetbrains.annotations.NotNull()
    public static final com.walletconnect.walletconnectv2.core.model.utils.JsonRpcMethod INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_PAIRING_PAYLOAD = "wc_pairingPayload";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_PAIRING_APPROVE = "wc_pairingApprove";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_PAIRING_REJECT = "wc_pairingReject";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_PAIRING_UPDATE = "wc_pairingUpdate";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_PAIRING_PING = "wc_pairingPing";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_PAIRING_DELETE = "wc_pairingDelete";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_PAIRING_NOTIFICATION = "wc_pairingNotification";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_PAYLOAD = "wc_sessionPayload";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_PROPOSE = "wc_sessionPropose";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_APPROVE = "wc_sessionApprove";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_UPDATE = "wc_sessionUpdate";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_UPGRADE = "wc_sessionUpgrade";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_REJECT = "wc_sessionReject";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_DELETE = "wc_sessionDelete";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_PING = "wc_sessionPing";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WC_SESSION_NOTIFICATION = "wc_sessionNotification";
    
    private JsonRpcMethod() {
        super();
    }
}