package com.walletconnect.walletconnectv2.core.model.type;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b`\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/type/Sequence;", "", "expiry", "Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "getExpiry", "()Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "status", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "getStatus", "()Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "walletconnectv2_debug"})
public abstract interface Sequence {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO getExpiry();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus getStatus();
}