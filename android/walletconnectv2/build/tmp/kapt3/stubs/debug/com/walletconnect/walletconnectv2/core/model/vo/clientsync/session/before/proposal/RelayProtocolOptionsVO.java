package com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u000f"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "", "protocol", "", "(Ljava/lang/String;)V", "getProtocol", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class RelayProtocolOptionsVO {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String protocol = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO copy(@org.jetbrains.annotations.NotNull()
    java.lang.String protocol) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public RelayProtocolOptionsVO() {
        super();
    }
    
    public RelayProtocolOptionsVO(@org.jetbrains.annotations.NotNull()
    java.lang.String protocol) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getProtocol() {
        return null;
    }
}