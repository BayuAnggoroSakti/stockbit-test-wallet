package com.walletconnect.walletconnectv2.core.model.vo.clientsync.session;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\t\u0003\u0004\u0005\u0006\u0007\b\t\n\u000bB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\t\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "Lcom/walletconnect/walletconnectv2/core/model/type/ClientParams;", "()V", "ApprovalParams", "DeleteParams", "NotificationParams", "PingParams", "ProposalParams", "RejectParams", "SessionPayloadParams", "UpdateParams", "UpgradeParams", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$ProposalParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$ApprovalParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$RejectParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$SessionPayloadParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$DeleteParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$UpdateParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$UpgradeParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$PingParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$NotificationParams;", "walletconnectv2_debug"})
public abstract class SessionParamsVO implements com.walletconnect.walletconnectv2.core.model.type.ClientParams {
    
    private SessionParamsVO() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0081\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0001\u0010\b\u001a\u00020\t\u0012\b\b\u0001\u0010\n\u001a\u00020\u000b\u0012\b\b\u0001\u0010\f\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u000bH\u00c6\u0003J\t\u0010 \u001a\u00020\rH\u00c6\u0003JE\u0010!\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00072\b\b\u0003\u0010\b\u001a\u00020\t2\b\b\u0003\u0010\n\u001a\u00020\u000b2\b\b\u0003\u0010\f\u001a\u00020\rH\u00c6\u0001J\u0013\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%H\u00d6\u0003J\t\u0010&\u001a\u00020\'H\u00d6\u0001J\t\u0010(\u001a\u00020)H\u00d6\u0001R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0016\u0010\f\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001a\u00a8\u0006*"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$ProposalParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "relay", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "proposer", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionProposerVO;", "signal", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionSignalVO;", "permissions", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionProposedPermissionsVO;", "ttl", "Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionProposerVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionSignalVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionProposedPermissionsVO;Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;)V", "getPermissions", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionProposedPermissionsVO;", "getProposer", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionProposerVO;", "getRelay", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "getSignal", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/SessionSignalVO;", "getTopic", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getTtl", "()Lcom/walletconnect/walletconnectv2/core/model/vo/TtlVO;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class ProposalParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        @org.jetbrains.annotations.NotNull()
        @com.walletconnect.walletconnectv2.core.adapters.TopicAdapter.Qualifier()
        private final com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposerVO proposer = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO signal = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVO permissions = null;
        @org.jetbrains.annotations.NotNull()
        @com.walletconnect.walletconnectv2.core.adapters.TtlAdapter.Qualifier()
        private final com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.ProposalParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "topic")
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "relay")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "proposer")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposerVO proposer, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "signal")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO signal, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "permissions")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVO permissions, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "ttl")
        com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public ProposalParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "topic")
        com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "relay")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "proposer")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposerVO proposer, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "signal")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO signal, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "permissions")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVO permissions, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "ttl")
        com.walletconnect.walletconnectv2.core.model.vo.TtlVO ttl) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TopicVO getTopic() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO getRelay() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposerVO component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposerVO getProposer() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO getSignal() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVO component5() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionProposedPermissionsVO getPermissions() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TtlVO component6() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.TtlVO getTtl() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0081\b\u0018\u00002\u00020\u0001B-\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0001\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00072\b\b\u0003\u0010\b\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006 "}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$ApprovalParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "relay", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "responder", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/success/SessionParticipantVO;", "expiry", "Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "state", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/common/SessionStateVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/success/SessionParticipantVO;Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/common/SessionStateVO;)V", "getExpiry", "()Lcom/walletconnect/walletconnectv2/core/model/vo/ExpiryVO;", "getRelay", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/proposal/RelayProtocolOptionsVO;", "getResponder", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/before/success/SessionParticipantVO;", "getState", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/common/SessionStateVO;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class ApprovalParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO responder = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO state = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.ApprovalParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "relay")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "responder")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO responder, @org.jetbrains.annotations.NotNull()
        @com.walletconnect.walletconnectv2.core.adapters.ExpiryAdapter.Qualifier()
        @com.squareup.moshi.Json(name = "expiry")
        com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "state")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO state) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public ApprovalParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "relay")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO relay, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "responder")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO responder, @org.jetbrains.annotations.NotNull()
        @com.walletconnect.walletconnectv2.core.adapters.ExpiryAdapter.Qualifier()
        @com.squareup.moshi.Json(name = "expiry")
        com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO expiry, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "state")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO state) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO getRelay() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO getResponder() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO getExpiry() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO getState() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0001\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$RejectParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "reason", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;)V", "getReason", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class RejectParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO reason = null;
        
        public RejectParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "reason")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO reason) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO getReason() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0005H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$SessionPayloadParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "request", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/SessionRequestVO;", "chainId", "", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/SessionRequestVO;Ljava/lang/String;)V", "getChainId", "()Ljava/lang/String;", "getRequest", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/SessionRequestVO;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class SessionPayloadParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVO request = null;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String chainId = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.SessionPayloadParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "request")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVO request, @org.jetbrains.annotations.Nullable()
        @com.squareup.moshi.Json(name = "chainId")
        java.lang.String chainId) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SessionPayloadParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "request")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVO request, @org.jetbrains.annotations.Nullable()
        @com.squareup.moshi.Json(name = "chainId")
        java.lang.String chainId) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVO getRequest() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getChainId() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0001\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$DeleteParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "reason", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;)V", "getReason", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/ReasonVO;", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class DeleteParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO reason = null;
        
        public DeleteParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "reason")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO reason) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.ReasonVO getReason() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$UpdateParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "state", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/common/SessionStateVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/common/SessionStateVO;)V", "getState", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/common/SessionStateVO;", "walletconnectv2_debug"})
    public static final class UpdateParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO state = null;
        
        public UpdateParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "state")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO state) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.common.SessionStateVO getState() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$UpgradeParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "permissions", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/SessionPermissionsVO;", "(Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/SessionPermissionsVO;)V", "getPermissions", "()Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/after/params/SessionPermissionsVO;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
    public static final class UpgradeParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVO permissions = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.UpgradeParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "permissions")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVO permissions) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public UpgradeParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "permissions")
        com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVO permissions) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVO component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionPermissionsVO getPermissions() {
            return null;
        }
    }
    
    @kotlin.Suppress(names = {"CanSealedSubClassBeObject"})
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$PingParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "()V", "walletconnectv2_debug"})
    public static final class PingParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        
        public PingParams() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO$NotificationParams;", "Lcom/walletconnect/walletconnectv2/core/model/vo/clientsync/session/SessionParamsVO;", "type", "", "data", "", "(Ljava/lang/String;Ljava/lang/Object;)V", "getData", "()Ljava/lang/Object;", "getType", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
    public static final class NotificationParams extends com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String type = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.Object data = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO.NotificationParams copy(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "type")
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "data")
        java.lang.Object data) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public NotificationParams(@org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "type")
        java.lang.String type, @org.jetbrains.annotations.NotNull()
        @com.squareup.moshi.Json(name = "data")
        java.lang.Object data) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getType() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object getData() {
            return null;
        }
    }
}