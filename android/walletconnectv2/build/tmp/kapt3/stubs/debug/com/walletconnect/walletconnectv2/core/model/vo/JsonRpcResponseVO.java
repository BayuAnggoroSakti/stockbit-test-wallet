package com.walletconnect.walletconnectv2.core.model.vo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b0\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0002\n\u000b\u00a8\u0006\f"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO;", "", "()V", "id", "", "getId", "()J", "Error", "JsonRpcError", "JsonRpcResult", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$JsonRpcResult;", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$JsonRpcError;", "walletconnectv2_debug"})
public abstract class JsonRpcResponseVO {
    
    private JsonRpcResponseVO() {
        super();
    }
    
    public abstract long getId();
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b\u00a8\u0006\u0018"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$JsonRpcResult;", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO;", "id", "", "jsonrpc", "", "result", "(JLjava/lang/String;Ljava/lang/String;)V", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "getResult", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class JsonRpcResult extends com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO {
        private final long id = 0L;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String jsonrpc = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String result = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.JsonRpcResult copy(long id, @org.jetbrains.annotations.NotNull()
        java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
        java.lang.String result) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public JsonRpcResult(long id, @org.jetbrains.annotations.NotNull()
        java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
        java.lang.String result) {
            super();
        }
        
        public final long component1() {
            return 0L;
        }
        
        @java.lang.Override()
        public long getId() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getJsonrpc() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getResult() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0081\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$JsonRpcError;", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO;", "id", "", "jsonrpc", "", "error", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$Error;", "(JLjava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$Error;)V", "getError", "()Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$Error;", "getId", "()J", "getJsonrpc", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
    @com.squareup.moshi.JsonClass(generateAdapter = true)
    public static final class JsonRpcError extends com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO {
        private final long id = 0L;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String jsonrpc = null;
        @org.jetbrains.annotations.NotNull()
        private final com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.Error error = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.JsonRpcError copy(long id, @org.jetbrains.annotations.NotNull()
        java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.Error error) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public JsonRpcError(long id, @org.jetbrains.annotations.NotNull()
        java.lang.String jsonrpc, @org.jetbrains.annotations.NotNull()
        com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.Error error) {
            super();
        }
        
        public final long component1() {
            return 0L;
        }
        
        @java.lang.Override()
        public long getId() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getJsonrpc() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.Error component3() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.Error getError() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0080\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO$Error;", "", "code", "", "message", "", "(ILjava/lang/String;)V", "getCode", "()I", "getMessage", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "walletconnectv2_debug"})
    public static final class Error {
        private final int code = 0;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String message = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO.Error copy(int code, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Error(int code, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
            super();
        }
        
        public final int component1() {
            return 0;
        }
        
        public final int getCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getMessage() {
            return null;
        }
    }
}