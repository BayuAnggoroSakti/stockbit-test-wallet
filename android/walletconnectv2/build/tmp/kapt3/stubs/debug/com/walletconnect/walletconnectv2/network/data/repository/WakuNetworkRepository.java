package com.walletconnect.walletconnectv2.network.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 -2\u00020\u0001:\u0001-B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001c\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00100\u0012H\u0002J\u001c\u0010\u0014\u001a\u00020\u00102\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00100\u0012H\u0002J\u001c\u0010\u0017\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00100\u0012H\u0002J\u001c\u0010\u0019\u001a\u00020\u00102\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00100\u0012H\u0002J\u001c\u0010\u001a\u001a\u00020\u00102\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00100\u0012H\u0002J\u001c\u0010\u001d\u001a\u00020\u00102\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00100\u0012H\u0002J=\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0018\u0010\u0011\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130%\u0012\u0004\u0012\u00020\u00100\u0012H\u0016\u00f8\u0001\u0000J\u0010\u0010&\u001a\u00020\u00102\u0006\u0010\'\u001a\u00020(H\u0002J-\u0010)\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\u0018\u0010\u0011\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180%\u0012\u0004\u0012\u00020\u00100\u0012H\u0016\u00f8\u0001\u0000J5\u0010*\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010+\u001a\u00020,2\u0018\u0010\u0011\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001c0%\u0012\u0004\u0012\u00020\u00100\u0012H\u0016\u00f8\u0001\u0000R\u001a\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006."}, d2 = {"Lcom/walletconnect/walletconnectv2/network/data/repository/WakuNetworkRepository;", "Lcom/walletconnect/walletconnectv2/network/NetworkRepository;", "relay", "Lcom/walletconnect/walletconnectv2/network/data/service/RelayService;", "(Lcom/walletconnect/walletconnectv2/network/data/service/RelayService;)V", "eventsFlow", "Lkotlinx/coroutines/flow/SharedFlow;", "Lcom/tinder/scarlet/WebSocket$Event;", "getEventsFlow", "()Lkotlinx/coroutines/flow/SharedFlow;", "subscriptionRequest", "Lkotlinx/coroutines/flow/Flow;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request;", "getSubscriptionRequest", "()Lkotlinx/coroutines/flow/Flow;", "observePublishAcknowledgement", "", "onResult", "Lkotlin/Function1;", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Acknowledgement;", "observePublishError", "onFailure", "", "observeSubscribeAcknowledgement", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Acknowledgement;", "observeSubscribeError", "observeUnSubscribeAcknowledgement", "onSuccess", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Acknowledgement;", "observeUnSubscribeError", "publish", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "message", "", "prompt", "", "Lkotlin/Result;", "publishSubscriptionAcknowledgement", "id", "", "subscribe", "unsubscribe", "subscriptionId", "Lcom/walletconnect/walletconnectv2/core/model/vo/SubscriptionIdVO;", "Companion", "walletconnectv2_debug"})
public final class WakuNetworkRepository implements com.walletconnect.walletconnectv2.network.NetworkRepository {
    private final com.walletconnect.walletconnectv2.network.data.service.RelayService relay = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.SharedFlow<com.tinder.scarlet.WebSocket.Event> eventsFlow = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request> subscriptionRequest = null;
    @org.jetbrains.annotations.NotNull()
    private static final com.walletconnect.walletconnectv2.network.data.repository.WakuNetworkRepository.Companion Companion = null;
    @java.lang.Deprecated()
    private static final int REPLAY = 1;
    
    public WakuNetworkRepository(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.network.data.service.RelayService relay) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kotlinx.coroutines.flow.SharedFlow<com.tinder.scarlet.WebSocket.Event> getEventsFlow() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request> getSubscriptionRequest() {
        return null;
    }
    
    @java.lang.Override()
    public void publish(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean prompt, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Acknowledgement>, kotlin.Unit> onResult) {
    }
    
    @java.lang.Override()
    public void subscribe(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Acknowledgement>, kotlin.Unit> onResult) {
    }
    
    @java.lang.Override()
    public void unsubscribe(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.SubscriptionIdVO subscriptionId, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Acknowledgement>, kotlin.Unit> onResult) {
    }
    
    private final void publishSubscriptionAcknowledgement(long id) {
    }
    
    private final void observePublishAcknowledgement(kotlin.jvm.functions.Function1<? super com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Acknowledgement, kotlin.Unit> onResult) {
    }
    
    private final void observePublishError(kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    private final void observeSubscribeAcknowledgement(kotlin.jvm.functions.Function1<? super com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Acknowledgement, kotlin.Unit> onResult) {
    }
    
    private final void observeSubscribeError(kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    private final void observeUnSubscribeAcknowledgement(kotlin.jvm.functions.Function1<? super com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Acknowledgement, kotlin.Unit> onSuccess) {
    }
    
    private final void observeUnSubscribeError(kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/data/repository/WakuNetworkRepository$Companion;", "", "()V", "REPLAY", "", "walletconnectv2_debug"})
    static final class Companion {
        
        private Companion() {
            super();
        }
    }
}