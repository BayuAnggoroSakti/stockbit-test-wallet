package com.walletconnect.walletconnectv2.storage.history;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ)\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u000eH\u0000\u00a2\u0006\u0002\b\u0012J)\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u000eH\u0000\u00a2\u0006\u0002\b\u0014J<\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u000b\u001a\u00020\u00112\b\u0010\u0018\u001a\u0004\u0018\u00010\u00112\b\u0010\u0019\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J(\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u001e\u001a\u00020\u0011J\u0010\u0010\u001f\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0016\u0010 \u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/history/JsonRpcHistory;", "", "controllerType", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "sharedPreferences", "Landroid/content/SharedPreferences;", "jsonRpcHistoryQueries", "Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryQueries;", "(Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Landroid/content/SharedPreferences;Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryQueries;)V", "deleteRequests", "", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getRequests", "", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcHistoryVO;", "listOfMethodsForRequests", "", "getRequests$walletconnectv2_debug", "getResponses", "getResponses$walletconnectv2_debug", "mapToJsonRpc", "requestId", "", "method", "body", "jsonRpcStatus", "Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "setRequest", "", "payload", "tryMigrationToDB", "updateRequestStatus", "walletconnectv2_debug"})
public final class JsonRpcHistory {
    private final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType = null;
    private final android.content.SharedPreferences sharedPreferences = null;
    private final com.walletconnect.walletconnectv2.storage.data.dao.JsonRpcHistoryQueries jsonRpcHistoryQueries = null;
    
    public JsonRpcHistory(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType, @org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences sharedPreferences, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.data.dao.JsonRpcHistoryQueries jsonRpcHistoryQueries) {
        super();
    }
    
    public final boolean setRequest(long requestId, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.Nullable()
    java.lang.String method, @org.jetbrains.annotations.NotNull()
    java.lang.String payload) {
        return false;
    }
    
    public final void updateRequestStatus(long requestId, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus jsonRpcStatus) {
    }
    
    public final void deleteRequests(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcHistoryVO> getRequests$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> listOfMethodsForRequests) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcHistoryVO> getResponses$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> listOfMethodsForRequests) {
        return null;
    }
    
    private final void tryMigrationToDB(long requestId) {
    }
    
    private final com.walletconnect.walletconnectv2.core.model.vo.JsonRpcHistoryVO mapToJsonRpc(long requestId, java.lang.String topic, java.lang.String method, java.lang.String body, com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus jsonRpcStatus, com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType) {
        return null;
    }
}