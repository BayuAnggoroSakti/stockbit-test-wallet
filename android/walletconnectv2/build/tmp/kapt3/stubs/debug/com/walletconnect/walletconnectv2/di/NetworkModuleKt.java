package com.walletconnect.walletconnectv2.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0000\u00a8\u0006\u0004"}, d2 = {"networkModule", "Lorg/koin/core/module/Module;", "serverUrl", "", "walletconnectv2_debug"})
public final class NetworkModuleKt {
}