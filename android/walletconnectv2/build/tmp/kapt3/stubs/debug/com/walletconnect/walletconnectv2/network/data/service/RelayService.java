package com.walletconnect.walletconnectv2.network.data.service;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b`\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003H\'J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003H\'J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0003H\'J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0003H\'J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0003H\'J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0003H\'J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u0003H\'J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0013\u001a\u00020\u0015H\'J\u0010\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0013\u001a\u00020\u0017H\'J\u0010\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0019H\'J\u0010\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\'\u00a8\u0006\u001c"}, d2 = {"Lcom/walletconnect/walletconnectv2/network/data/service/RelayService;", "", "eventsFlow", "Lkotlinx/coroutines/flow/Flow;", "Lcom/tinder/scarlet/WebSocket$Event;", "observePublishAcknowledgement", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Acknowledgement;", "observePublishError", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$JsonRpcError;", "observeSubscribeAcknowledgement", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Acknowledgement;", "observeSubscribeError", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$JsonRpcError;", "observeSubscriptionRequest", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Request;", "observeUnsubscribeAcknowledgement", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Acknowledgement;", "observeUnsubscribeError", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$JsonRpcError;", "publishRequest", "", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Publish$Request;", "publishSubscriptionAcknowledgement", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscription$Acknowledgement;", "subscribeRequest", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Subscribe$Request;", "unsubscribeRequest", "Lcom/walletconnect/walletconnectv2/network/model/RelayDTO$Unsubscribe$Request;", "walletconnectv2_debug"})
public abstract interface RelayService {
    
    @org.jetbrains.annotations.NotNull()
    @com.tinder.scarlet.ws.Receive()
    public abstract kotlinx.coroutines.flow.Flow<com.tinder.scarlet.WebSocket.Event> eventsFlow();
    
    @com.tinder.scarlet.ws.Send()
    public abstract void publishRequest(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Request publishRequest);
    
    @org.jetbrains.annotations.NotNull()
    @com.tinder.scarlet.ws.Receive()
    public abstract kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.Acknowledgement> observePublishAcknowledgement();
    
    @org.jetbrains.annotations.NotNull()
    @com.tinder.scarlet.ws.Receive()
    public abstract kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Publish.JsonRpcError> observePublishError();
    
    @com.tinder.scarlet.ws.Send()
    public abstract void subscribeRequest(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Request subscribeRequest);
    
    @org.jetbrains.annotations.NotNull()
    @com.tinder.scarlet.ws.Receive()
    public abstract kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.Acknowledgement> observeSubscribeAcknowledgement();
    
    @org.jetbrains.annotations.NotNull()
    @com.tinder.scarlet.ws.Receive()
    public abstract kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscribe.JsonRpcError> observeSubscribeError();
    
    @org.jetbrains.annotations.NotNull()
    @com.tinder.scarlet.ws.Receive()
    public abstract kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Request> observeSubscriptionRequest();
    
    @com.tinder.scarlet.ws.Send()
    public abstract void publishSubscriptionAcknowledgement(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.network.model.RelayDTO.Subscription.Acknowledgement publishRequest);
    
    @com.tinder.scarlet.ws.Send()
    public abstract void unsubscribeRequest(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Request unsubscribeRequest);
    
    @org.jetbrains.annotations.NotNull()
    @com.tinder.scarlet.ws.Receive()
    public abstract kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.Acknowledgement> observeUnsubscribeAcknowledgement();
    
    @org.jetbrains.annotations.NotNull()
    @com.tinder.scarlet.ws.Receive()
    public abstract kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.network.model.RelayDTO.Unsubscribe.JsonRpcError> observeUnsubscribeError();
}