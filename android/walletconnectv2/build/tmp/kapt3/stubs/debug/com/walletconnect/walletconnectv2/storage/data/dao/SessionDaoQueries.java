package com.walletconnect.walletconnectv2.storage.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0007H&J\u00bb\u0003\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0007\"\b\b\u0000\u0010\u000b*\u00020\f2\u00a0\u0003\u0010\r\u001a\u009b\u0003\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0004\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0012\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0013\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0015\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0016\u0012\u0013\u0012\u00110\b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0017\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0019\u0012\u0013\u0012\u00110\u001a\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001b\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001c\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001d\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001e\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001f\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b( \u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(!\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\"\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(#\u0012\u0004\u0012\u0002H\u000b0\u000eH&J\u0016\u0010$\u001a\b\u0012\u0004\u0012\u00020%0\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&Jd\u0010$\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0007\"\b\b\u0000\u0010\u000b*\u00020\f2\u0006\u0010\u0004\u001a\u00020\u00052B\u0010\r\u001a>\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0012\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u0002H\u000b0&H&J\u0016\u0010\'\u001a\b\u0012\u0004\u0012\u00020(0\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J\u00c3\u0003\u0010\'\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0007\"\b\b\u0000\u0010\u000b*\u00020\f2\u0006\u0010\u0004\u001a\u00020\u00052\u00a0\u0003\u0010\r\u001a\u009b\u0003\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0004\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0012\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0013\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0015\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0016\u0012\u0013\u0012\u00110\b\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0017\u0012\u0013\u0012\u00110\u0018\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0019\u0012\u0013\u0012\u00110\u001a\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001b\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001c\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001d\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001e\u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0011\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u001f\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b( \u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(!\u0012\u0015\u0012\u0013\u0018\u00010\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\"\u0012\u0013\u0012\u00110\u0005\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(#\u0012\u0004\u0012\u0002H\u000b0\u000eH&J\u0016\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00050\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J{\u0010*\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00050\u00112\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\u00112\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00112\u0006\u0010 \u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u001a2\b\u0010+\u001a\u0004\u0018\u00010\b2\u0006\u0010#\u001a\u00020\u0005H&\u00a2\u0006\u0002\u0010,J\u0018\u0010-\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0004\u001a\u00020\u0005H&J\u009f\u0001\u0010.\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00112\u0006\u0010\u0017\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u00052\b\u0010\"\u001a\u0004\u0018\u00010\u00052\b\u0010!\u001a\u0004\u0018\u00010\u00052\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00050\u00112\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\u00112\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00112\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010#\u001a\u00020\u00052\b\u0010+\u001a\u0004\u0018\u00010\b2\u0006\u0010/\u001a\u00020\u0005H&\u00a2\u0006\u0002\u00100J\u0018\u00101\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0088\u0001\u00102\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00112\u0006\u0010\u0017\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u00052\b\u0010\"\u001a\u0004\u0018\u00010\u00052\b\u0010!\u001a\u0004\u0018\u00010\u00052\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00050\u00112\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\u00112\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00112\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010/\u001a\u00020\u0005H&J \u00103\u001a\u00020\u00032\u000e\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00112\u0006\u0010\u0004\u001a\u00020\u0005H&J,\u00104\u001a\u00020\u00032\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00050\u00112\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\u00112\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u00065"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/SessionDaoQueries;", "Lcom/squareup/sqldelight/Transacter;", "deleteSession", "", "topic", "", "getExpiry", "Lcom/squareup/sqldelight/Query;", "", "getListOfSessionDaos", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetListOfSessionDaos;", "T", "", "mapper", "Lkotlin/Function17;", "Lkotlin/ParameterName;", "name", "", "permissions_chains", "permissions_methods", "permissions_types", "ttl_seconds", "accounts", "expiry", "Lcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;", "status", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "controller_type", "_name", "description", "url", "icons", "self_participant", "peer_participant", "controller_key", "relay_protocol", "getPermissionsByTopic", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetPermissionsByTopic;", "Lkotlin/Function2;", "getSessionByTopic", "Lcom/walletconnect/walletconnectv2/storage/data/dao/GetSessionByTopic;", "hasTopic", "insertSession", "metadata_id", "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;JJLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;Ljava/lang/Long;Ljava/lang/String;)V", "updatePreSettledSessionToAcknowledged", "updateProposedSessionToAcknowledged", "topic_", "(Ljava/lang/String;Ljava/util/List;JLcom/walletconnect/walletconnectv2/storage/sequence/SequenceStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V", "updateProposedSessionToResponded", "updateRespondedSessionToPresettled", "updateSessionWithAccounts", "updateSessionWithPermissions", "walletconnectv2_debug"})
public abstract interface SessionDaoQueries extends com.squareup.sqldelight.Transacter {
    
    @org.jetbrains.annotations.NotNull()
    public abstract <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getListOfSessionDaos(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function17<? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? super java.lang.Long, ? super java.util.List<java.lang.String>, ? super java.lang.Long, ? super com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? extends T> mapper);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetListOfSessionDaos> getListOfSessionDaos();
    
    @org.jetbrains.annotations.NotNull()
    public abstract <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getPermissionsByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? extends T> mapper);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetPermissionsByTopic> getPermissionsByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    @org.jetbrains.annotations.NotNull()
    public abstract <T extends java.lang.Object>com.squareup.sqldelight.Query<T> getSessionByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function17<? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? super java.util.List<java.lang.String>, ? super java.lang.Long, ? super java.util.List<java.lang.String>, ? super java.lang.Long, ? super com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus, ? super com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.util.List<java.lang.String>, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? super java.lang.String, ? extends T> mapper);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<com.walletconnect.walletconnectv2.storage.data.dao.GetSessionByTopic> getSessionByTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<java.lang.String> hasTopic(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.squareup.sqldelight.Query<java.lang.Long> getExpiry(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    public abstract void insertSession(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions_types, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, long ttl_seconds, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol);
    
    public abstract void updateProposedSessionToResponded(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    public abstract void updateRespondedSessionToPresettled(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> accounts, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions_types, long ttl_seconds, @org.jetbrains.annotations.NotNull()
    java.lang.String topic_);
    
    public abstract void updatePreSettledSessionToAcknowledged(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    public abstract void updateProposedSessionToAcknowledged(@org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> accounts, long expiry, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.sequence.SequenceStatus status, @org.jetbrains.annotations.NotNull()
    java.lang.String self_participant, @org.jetbrains.annotations.Nullable()
    java.lang.String controller_key, @org.jetbrains.annotations.Nullable()
    java.lang.String peer_participant, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> permissions_types, long ttl_seconds, @org.jetbrains.annotations.NotNull()
    java.lang.String relay_protocol, @org.jetbrains.annotations.Nullable()
    java.lang.Long metadata_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic_);
    
    public abstract void updateSessionWithPermissions(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_chains, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> permissions_methods, @org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    public abstract void updateSessionWithAccounts(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    java.lang.String topic);
    
    public abstract void deleteSession(@org.jetbrains.annotations.NotNull()
    java.lang.String topic);
}