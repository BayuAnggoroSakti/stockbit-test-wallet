package com.walletconnect.walletconnectv2.core.model.type;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\b\b`\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003R\u0012\u0010\u0004\u001a\u00020\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0012\u0010\b\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0012\u0010\f\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000bR\u0012\u0010\u000e\u001a\u00028\u0000X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/core/model/type/SettlementSequence;", "T", "Lcom/walletconnect/walletconnectv2/core/model/type/ClientParams;", "Lcom/walletconnect/walletconnectv2/core/model/type/SerializableJsonRpc;", "id", "", "getId", "()J", "jsonrpc", "", "getJsonrpc", "()Ljava/lang/String;", "method", "getMethod", "params", "getParams", "()Lcom/walletconnect/walletconnectv2/core/model/type/ClientParams;", "walletconnectv2_debug"})
public abstract interface SettlementSequence<T extends com.walletconnect.walletconnectv2.core.model.type.ClientParams> extends com.walletconnect.walletconnectv2.core.model.type.SerializableJsonRpc {
    
    public abstract long getId();
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.String getMethod();
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.String getJsonrpc();
    
    @org.jetbrains.annotations.NotNull()
    public abstract T getParams();
}