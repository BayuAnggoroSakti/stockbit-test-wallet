package com.walletconnect.walletconnectv2.relay.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00b6\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0000\u0018\u0000 Q2\u00020\u0001:\u0001QB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ-\u0010(\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020+0*\u0012\n\u0012\b\u0012\u0004\u0012\u00020+0*0)2\u0006\u0010,\u001a\u00020-H\u0000\u00a2\u0006\u0002\b.J\u0019\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020#H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00102J!\u00103\u001a\u0002002\u0006\u00101\u001a\u00020#2\u0006\u0010,\u001a\u00020-H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00104J\b\u00105\u001a\u000200H\u0002JM\u00106\u001a\u0002002\u0006\u0010,\u001a\u00020-2\n\u00107\u001a\u0006\u0012\u0002\b\u0003082\b\b\u0002\u00109\u001a\u00020\u000e2\f\u0010:\u001a\b\u0012\u0004\u0012\u0002000;2\u0012\u0010<\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u0002000=H\u0000\u00a2\u0006\u0002\b>JC\u0010?\u001a\u0002002\u0006\u0010,\u001a\u00020-2\u0006\u0010@\u001a\u00020\u00102\u000e\b\u0002\u0010:\u001a\b\u0012\u0004\u0012\u0002000;2\u0014\b\u0002\u0010<\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u0002000=H\u0000\u00a2\u0006\u0002\bAJ\u001d\u0010B\u001a\u0002002\u0006\u0010C\u001a\u00020D2\u0006\u0010E\u001a\u00020FH\u0000\u00a2\u0006\u0002\bGJ\u0015\u0010H\u001a\u0002002\u0006\u0010C\u001a\u00020DH\u0000\u00a2\u0006\u0002\bIJ\u0010\u0010J\u001a\u0002002\u0006\u0010K\u001a\u00020LH\u0002J\u0015\u0010M\u001a\u0002002\u0006\u0010,\u001a\u00020-H\u0000\u00a2\u0006\u0002\bNJ\u0015\u0010O\u001a\u0002002\u0006\u0010,\u001a\u00020-H\u0000\u00a2\u0006\u0002\bPR\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0012X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u00188F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u000e0\u001d\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0014R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010!\u001a\u000e\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020#0\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010$\u001a\u00020\u0019*\u00020%8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b&\u0010\'\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006R"}, d2 = {"Lcom/walletconnect/walletconnectv2/relay/domain/WalletConnectRelayer;", "", "networkRepository", "Lcom/walletconnect/walletconnectv2/network/NetworkRepository;", "serializer", "Lcom/walletconnect/walletconnectv2/relay/data/serializer/JsonRpcSerializer;", "jsonRpcHistory", "Lcom/walletconnect/walletconnectv2/storage/history/JsonRpcHistory;", "(Lcom/walletconnect/walletconnectv2/network/NetworkRepository;Lcom/walletconnect/walletconnectv2/relay/data/serializer/JsonRpcSerializer;Lcom/walletconnect/walletconnectv2/storage/history/JsonRpcHistory;)V", "_clientSyncJsonRpc", "Lkotlinx/coroutines/flow/MutableSharedFlow;", "Lcom/walletconnect/walletconnectv2/core/model/vo/RequestSubscriptionPayloadVO;", "_isConnectionOpened", "Lkotlinx/coroutines/flow/MutableStateFlow;", "", "_peerResponse", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcResponseVO;", "clientSyncJsonRpc", "Lkotlinx/coroutines/flow/SharedFlow;", "getClientSyncJsonRpc$walletconnectv2_debug", "()Lkotlinx/coroutines/flow/SharedFlow;", "exceptionHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "initializationErrorsFlow", "Lkotlinx/coroutines/flow/Flow;", "Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "getInitializationErrorsFlow", "()Lkotlinx/coroutines/flow/Flow;", "isConnectionOpened", "Lkotlinx/coroutines/flow/StateFlow;", "()Lkotlinx/coroutines/flow/StateFlow;", "peerResponse", "getPeerResponse", "subscriptions", "", "", "toWalletConnectException", "", "getToWalletConnectException", "(Ljava/lang/Throwable;)Lcom/walletconnect/walletconnectv2/core/exceptions/client/WalletConnectException;", "getJsonRpcHistory", "Lkotlin/Pair;", "", "Lcom/walletconnect/walletconnectv2/core/model/vo/JsonRpcHistoryVO;", "topic", "Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;", "getJsonRpcHistory$walletconnectv2_debug", "handleJsonRpcResponse", "", "decryptedMessage", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "handleSessionRequest", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/core/model/vo/TopicVO;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "manageSubscriptions", "publishJsonRpcRequests", "payload", "Lcom/walletconnect/walletconnectv2/core/model/type/SettlementSequence;", "prompt", "onSuccess", "Lkotlin/Function0;", "onFailure", "Lkotlin/Function1;", "publishJsonRpcRequests$walletconnectv2_debug", "publishJsonRpcResponse", "response", "publishJsonRpcResponse$walletconnectv2_debug", "respondWithError", "request", "Lcom/walletconnect/walletconnectv2/core/model/vo/WCRequestVO;", "error", "Lcom/walletconnect/walletconnectv2/core/exceptions/peer/PeerError;", "respondWithError$walletconnectv2_debug", "respondWithSuccess", "respondWithSuccess$walletconnectv2_debug", "setOnConnectionOpen", "event", "Lcom/tinder/scarlet/WebSocket$Event;", "subscribe", "subscribe$walletconnectv2_debug", "unsubscribe", "unsubscribe$walletconnectv2_debug", "Companion", "walletconnectv2_debug"})
public final class WalletConnectRelayer {
    private final com.walletconnect.walletconnectv2.network.NetworkRepository networkRepository = null;
    private final com.walletconnect.walletconnectv2.relay.data.serializer.JsonRpcSerializer serializer = null;
    private final com.walletconnect.walletconnectv2.storage.history.JsonRpcHistory jsonRpcHistory = null;
    private final kotlinx.coroutines.flow.MutableSharedFlow<com.walletconnect.walletconnectv2.core.model.vo.RequestSubscriptionPayloadVO> _clientSyncJsonRpc = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.SharedFlow<com.walletconnect.walletconnectv2.core.model.vo.RequestSubscriptionPayloadVO> clientSyncJsonRpc = null;
    private final kotlinx.coroutines.flow.MutableSharedFlow<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO> _peerResponse = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.SharedFlow<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO> peerResponse = null;
    private final java.util.Map<java.lang.String, java.lang.String> subscriptions = null;
    private final kotlinx.coroutines.flow.MutableStateFlow<java.lang.Boolean> _isConnectionOpened = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.StateFlow<java.lang.Boolean> isConnectionOpened = null;
    private final kotlinx.coroutines.CoroutineExceptionHandler exceptionHandler = null;
    @org.jetbrains.annotations.NotNull()
    private static final com.walletconnect.walletconnectv2.relay.domain.WalletConnectRelayer.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    @java.lang.Deprecated()
    private static final java.util.List<java.lang.String> listOfMethodsForRequests = null;
    
    public WalletConnectRelayer(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.network.NetworkRepository networkRepository, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.relay.data.serializer.JsonRpcSerializer serializer, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.history.JsonRpcHistory jsonRpcHistory) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.SharedFlow<com.walletconnect.walletconnectv2.core.model.vo.RequestSubscriptionPayloadVO> getClientSyncJsonRpc$walletconnectv2_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.SharedFlow<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO> getPeerResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.StateFlow<java.lang.Boolean> isConnectionOpened() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.walletconnect.walletconnectv2.core.exceptions.client.WalletConnectException> getInitializationErrorsFlow() {
        return null;
    }
    
    public final void publishJsonRpcRequests$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.SettlementSequence<?> payload, boolean prompt, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void publishJsonRpcResponse$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO response, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onSuccess, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Throwable, kotlin.Unit> onFailure) {
    }
    
    public final void subscribe$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
    }
    
    public final void unsubscribe$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.Pair<java.util.List<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcHistoryVO>, java.util.List<com.walletconnect.walletconnectv2.core.model.vo.JsonRpcHistoryVO>> getJsonRpcHistory$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic) {
        return null;
    }
    
    public final void respondWithError$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.exceptions.peer.PeerError error) {
    }
    
    public final void respondWithSuccess$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.vo.WCRequestVO request) {
    }
    
    private final void manageSubscriptions() {
    }
    
    private final java.lang.Object handleSessionRequest(java.lang.String decryptedMessage, com.walletconnect.walletconnectv2.core.model.vo.TopicVO topic, kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    private final java.lang.Object handleJsonRpcResponse(java.lang.String decryptedMessage, kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    private final void setOnConnectionOpen(com.tinder.scarlet.WebSocket.Event event) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/walletconnect/walletconnectv2/relay/domain/WalletConnectRelayer$Companion;", "", "()V", "listOfMethodsForRequests", "", "", "getListOfMethodsForRequests", "()Ljava/util/List;", "walletconnectv2_debug"})
    static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getListOfMethodsForRequests() {
            return null;
        }
    }
}