package com.walletconnect.walletconnectv2.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\b\u001a\u0004\u0018\u00010\tJ\u0010\u0010\n\u001a\u00020\u00062\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\n\u001a\u00020\u00062\b\u0010\b\u001a\u0004\u0018\u00010\tR\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/walletconnect/walletconnectv2/util/Logger;", "", "()V", "TAG", "", "error", "", "errorMsg", "throwable", "", "log", "logMsg", "walletconnectv2_debug"})
public final class Logger {
    @org.jetbrains.annotations.NotNull()
    public static final com.walletconnect.walletconnectv2.util.Logger INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TAG = "WalletConnectV2";
    
    private Logger() {
        super();
    }
    
    public final void log(@org.jetbrains.annotations.Nullable()
    java.lang.String logMsg) {
    }
    
    public final void log(@org.jetbrains.annotations.Nullable()
    java.lang.Throwable throwable) {
    }
    
    public final void error(@org.jetbrains.annotations.Nullable()
    java.lang.String errorMsg) {
    }
    
    public final void error(@org.jetbrains.annotations.Nullable()
    java.lang.Throwable throwable) {
    }
}