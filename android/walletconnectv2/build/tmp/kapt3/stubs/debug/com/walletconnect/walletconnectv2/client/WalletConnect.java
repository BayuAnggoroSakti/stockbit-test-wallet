package com.walletconnect.walletconnectv2.client;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect;", "", "()V", "Listeners", "Model", "Params", "walletconnectv2_debug"})
public final class WalletConnect {
    @org.jetbrains.annotations.NotNull()
    public static final com.walletconnect.walletconnectv2.client.WalletConnect INSTANCE = null;
    
    private WalletConnect() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bv\u0018\u00002\u00020\u0001:\n\u0006\u0007\b\t\n\u000b\f\r\u000e\u000fJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u0082\u0001\n\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "", "onError", "", "error", "", "Notification", "Pairing", "SessionApprove", "SessionDelete", "SessionPayload", "SessionPing", "SessionReject", "SessionRequest", "SessionUpdate", "SessionUpgrade", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$Pairing;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionReject;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionDelete;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionApprove;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionPayload;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionUpdate;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionUpgrade;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionPing;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$Notification;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionRequest;", "walletconnectv2_debug"})
    public static abstract interface Listeners {
        
        public abstract void onError(@org.jetbrains.annotations.NotNull()
        java.lang.Throwable error);
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$Pairing;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "settledPairing", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledPairing;", "walletconnectv2_debug"})
        public static abstract interface Pairing extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledPairing settledPairing);
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionReject;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "rejectedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$RejectedSession;", "walletconnectv2_debug"})
        public static abstract interface SessionReject extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.RejectedSession rejectedSession);
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionDelete;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "deletedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$DeletedSession;", "walletconnectv2_debug"})
        public static abstract interface SessionDelete extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.DeletedSession deletedSession);
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionApprove;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "settledSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession;", "walletconnectv2_debug"})
        public static abstract interface SessionApprove extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession settledSession);
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionPayload;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "walletconnectv2_debug"})
        public static abstract interface SessionPayload extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionUpdate;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "updatedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$UpdatedSession;", "walletconnectv2_debug"})
        public static abstract interface SessionUpdate extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.UpdatedSession updatedSession);
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionUpgrade;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "upgradedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$UpgradedSession;", "walletconnectv2_debug"})
        public static abstract interface SessionUpgrade extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.UpgradedSession upgradedSession);
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionPing;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "topic", "", "walletconnectv2_debug"})
        public static abstract interface SessionPing extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            java.lang.String topic);
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$Notification;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "topic", "", "walletconnectv2_debug"})
        public static abstract interface Notification extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            java.lang.String topic);
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners$SessionRequest;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Listeners;", "onSuccess", "", "response", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse;", "walletconnectv2_debug"})
        public static abstract interface SessionRequest extends com.walletconnect.walletconnectv2.client.WalletConnect.Listeners {
            
            public abstract void onSuccess(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse response);
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0013\u0003\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0013\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&\'(\u00a8\u0006)"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "", "()V", "AppMetaData", "ApprovedSession", "Blockchain", "DeletedSession", "JsonRpcHistory", "JsonRpcResponse", "Jsonrpc", "Notification", "Notifications", "RejectedSession", "SessionNotification", "SessionPermissions", "SessionProposal", "SessionRequest", "SessionState", "SettledPairing", "SettledSession", "UpdatedSession", "UpgradedSession", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionProposal;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionRequest;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionRequest$JSONRPCRequest;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionState;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledPairing;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$RejectedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$ApprovedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$DeletedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$UpgradedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Blockchain;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Jsonrpc;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$UpdatedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionNotification;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notification;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcHistory;", "walletconnectv2_debug"})
    public static abstract class Model {
        
        private Model() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b#\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u008f\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u0012\u0010\b\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0007\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u0012\u0006\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0014J\t\u0010%\u001a\u00020\u0003H\u00c6\u0003J\t\u0010&\u001a\u00020\u000fH\u00c6\u0003J\t\u0010\'\u001a\u00020\u0011H\u00c6\u0003J\u000f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\u0003H\u00c6\u0003J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010,\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u00c6\u0003J\u000f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003J\u000f\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003J\u0011\u0010/\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0007H\u00c6\u0003J\t\u00100\u001a\u00020\u0003H\u00c6\u0003J\t\u00101\u001a\u00020\u0003H\u00c6\u0003J\u00ab\u0001\u00102\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00072\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00072\u0010\b\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00072\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00112\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u00072\b\b\u0002\u0010\u0013\u001a\u00020\u0003H\u00c6\u0001J\u0013\u00103\u001a\u00020\u000f2\b\u00104\u001a\u0004\u0018\u000105H\u00d6\u0003J\t\u00106\u001a\u000207H\u00d6\u0001J\t\u00108\u001a\u00020\u0003H\u00d6\u0001R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0016R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u001bR\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0019R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0019R\u0011\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0019R\u0011\u0010\f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0019R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0019\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0016R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0019\u00a8\u00069"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionProposal;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "name", "", "description", "url", "icons", "", "Ljava/net/URI;", "chains", "methods", "types", "topic", "proposerPublicKey", "isController", "", "ttl", "", "accounts", "relayProtocol", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ZJLjava/util/List;Ljava/lang/String;)V", "getAccounts", "()Ljava/util/List;", "getChains", "getDescription", "()Ljava/lang/String;", "getIcons", "()Z", "getMethods", "getName", "getProposerPublicKey", "getRelayProtocol", "getTopic", "getTtl", "()J", "getTypes", "getUrl", "component1", "component10", "component11", "component12", "component13", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class SessionProposal extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String name = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String description = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String url = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.net.URI> icons = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> chains = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> methods = null;
            @org.jetbrains.annotations.Nullable()
            private final java.util.List<java.lang.String> types = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String proposerPublicKey = null;
            private final boolean isController = false;
            private final long ttl = 0L;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> accounts = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String relayProtocol = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionProposal copy(@org.jetbrains.annotations.NotNull()
            java.lang.String name, @org.jetbrains.annotations.NotNull()
            java.lang.String description, @org.jetbrains.annotations.NotNull()
            java.lang.String url, @org.jetbrains.annotations.NotNull()
            java.util.List<java.net.URI> icons, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> chains, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> methods, @org.jetbrains.annotations.Nullable()
            java.util.List<java.lang.String> types, @org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.lang.String proposerPublicKey, boolean isController, long ttl, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
            java.lang.String relayProtocol) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SessionProposal(@org.jetbrains.annotations.NotNull()
            java.lang.String name, @org.jetbrains.annotations.NotNull()
            java.lang.String description, @org.jetbrains.annotations.NotNull()
            java.lang.String url, @org.jetbrains.annotations.NotNull()
            java.util.List<java.net.URI> icons, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> chains, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> methods, @org.jetbrains.annotations.Nullable()
            java.util.List<java.lang.String> types, @org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.lang.String proposerPublicKey, boolean isController, long ttl, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
            java.lang.String relayProtocol) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getName() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getDescription() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getUrl() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.net.URI> component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.net.URI> getIcons() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component5() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getChains() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component6() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getMethods() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.util.List<java.lang.String> component7() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.util.List<java.lang.String> getTypes() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component8() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component9() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getProposerPublicKey() {
                return null;
            }
            
            public final boolean component10() {
                return false;
            }
            
            public final boolean isController() {
                return false;
            }
            
            public final long component11() {
                return 0L;
            }
            
            public final long getTtl() {
                return 0L;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component12() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getAccounts() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component13() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getRelayProtocol() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0018B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J)\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u00a8\u0006\u0019"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionRequest;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "chainId", "request", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionRequest$JSONRPCRequest;", "(Ljava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionRequest$JSONRPCRequest;)V", "getChainId", "()Ljava/lang/String;", "getRequest", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionRequest$JSONRPCRequest;", "getTopic", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "JSONRPCRequest", "walletconnectv2_debug"})
        public static final class SessionRequest extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.Nullable()
            private final java.lang.String chainId = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionRequest.JSONRPCRequest request = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionRequest copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.Nullable()
            java.lang.String chainId, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionRequest.JSONRPCRequest request) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SessionRequest(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.Nullable()
            java.lang.String chainId, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionRequest.JSONRPCRequest request) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getChainId() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionRequest.JSONRPCRequest component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionRequest.JSONRPCRequest getRequest() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b\u00a8\u0006\u0018"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionRequest$JSONRPCRequest;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "id", "", "method", "", "params", "(JLjava/lang/String;Ljava/lang/String;)V", "getId", "()J", "getMethod", "()Ljava/lang/String;", "getParams", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
            public static final class JSONRPCRequest extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
                private final long id = 0L;
                @org.jetbrains.annotations.NotNull()
                private final java.lang.String method = null;
                @org.jetbrains.annotations.NotNull()
                private final java.lang.String params = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionRequest.JSONRPCRequest copy(long id, @org.jetbrains.annotations.NotNull()
                java.lang.String method, @org.jetbrains.annotations.NotNull()
                java.lang.String params) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public JSONRPCRequest(long id, @org.jetbrains.annotations.NotNull()
                java.lang.String method, @org.jetbrains.annotations.NotNull()
                java.lang.String params) {
                    super();
                }
                
                public final long component1() {
                    return 0L;
                }
                
                public final long getId() {
                    return 0L;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String getMethod() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String component3() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String getParams() {
                    return null;
                }
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001fB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J9\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\b\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0003H\u00d6\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006 "}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "accounts", "", "peerAppMetaData", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "permissions", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions;", "(Ljava/lang/String;Ljava/util/List;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions;)V", "getAccounts", "()Ljava/util/List;", "getPeerAppMetaData", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "getPermissions", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions;", "getTopic", "()Ljava/lang/String;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Permissions", "walletconnectv2_debug"})
        public static final class SettledSession extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> accounts = null;
            @org.jetbrains.annotations.Nullable()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData peerAppMetaData = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions permissions = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.Nullable()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData peerAppMetaData, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions permissions) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SettledSession(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.Nullable()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData peerAppMetaData, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions permissions) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getAccounts() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData component3() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData getPeerAppMetaData() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions getPermissions() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001:\u0003\u001a\u001b\u001cB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001d"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions;", "", "blockchain", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$Blockchain;", "jsonRpc", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$JsonRpc;", "notifications", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$Notifications;", "(Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$Blockchain;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$JsonRpc;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$Notifications;)V", "getBlockchain", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$Blockchain;", "getJsonRpc", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$JsonRpc;", "getNotifications", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$Notifications;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Blockchain", "JsonRpc", "Notifications", "walletconnectv2_debug"})
            public static final class Permissions {
                @org.jetbrains.annotations.NotNull()
                private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Blockchain blockchain = null;
                @org.jetbrains.annotations.NotNull()
                private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.JsonRpc jsonRpc = null;
                @org.jetbrains.annotations.NotNull()
                private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Notifications notifications = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions copy(@org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Blockchain blockchain, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.JsonRpc jsonRpc, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Notifications notifications) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public Permissions(@org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Blockchain blockchain, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.JsonRpc jsonRpc, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Notifications notifications) {
                    super();
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Blockchain component1() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Blockchain getBlockchain() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.JsonRpc component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.JsonRpc getJsonRpc() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Notifications component3() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Notifications getNotifications() {
                    return null;
                }
                
                @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$Blockchain;", "", "chains", "", "", "(Ljava/util/List;)V", "getChains", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
                public static final class Blockchain {
                    @org.jetbrains.annotations.NotNull()
                    private final java.util.List<java.lang.String> chains = null;
                    
                    @org.jetbrains.annotations.NotNull()
                    public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Blockchain copy(@org.jetbrains.annotations.NotNull()
                    java.util.List<java.lang.String> chains) {
                        return null;
                    }
                    
                    @java.lang.Override()
                    public boolean equals(@org.jetbrains.annotations.Nullable()
                    java.lang.Object other) {
                        return false;
                    }
                    
                    @java.lang.Override()
                    public int hashCode() {
                        return 0;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    @java.lang.Override()
                    public java.lang.String toString() {
                        return null;
                    }
                    
                    public Blockchain(@org.jetbrains.annotations.NotNull()
                    java.util.List<java.lang.String> chains) {
                        super();
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    public final java.util.List<java.lang.String> component1() {
                        return null;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    public final java.util.List<java.lang.String> getChains() {
                        return null;
                    }
                }
                
                @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$JsonRpc;", "", "methods", "", "", "(Ljava/util/List;)V", "getMethods", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
                public static final class JsonRpc {
                    @org.jetbrains.annotations.NotNull()
                    private final java.util.List<java.lang.String> methods = null;
                    
                    @org.jetbrains.annotations.NotNull()
                    public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.JsonRpc copy(@org.jetbrains.annotations.NotNull()
                    java.util.List<java.lang.String> methods) {
                        return null;
                    }
                    
                    @java.lang.Override()
                    public boolean equals(@org.jetbrains.annotations.Nullable()
                    java.lang.Object other) {
                        return false;
                    }
                    
                    @java.lang.Override()
                    public int hashCode() {
                        return 0;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    @java.lang.Override()
                    public java.lang.String toString() {
                        return null;
                    }
                    
                    public JsonRpc(@org.jetbrains.annotations.NotNull()
                    java.util.List<java.lang.String> methods) {
                        super();
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    public final java.util.List<java.lang.String> component1() {
                        return null;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    public final java.util.List<java.lang.String> getMethods() {
                        return null;
                    }
                }
                
                @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u000e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005J\u0011\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u001b\u0010\t\u001a\u00020\u00002\u0010\b\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u00d6\u0001R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledSession$Permissions$Notifications;", "", "types", "", "", "(Ljava/util/List;)V", "getTypes", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
                public static final class Notifications {
                    @org.jetbrains.annotations.Nullable()
                    private final java.util.List<java.lang.String> types = null;
                    
                    @org.jetbrains.annotations.NotNull()
                    public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledSession.Permissions.Notifications copy(@org.jetbrains.annotations.Nullable()
                    java.util.List<java.lang.String> types) {
                        return null;
                    }
                    
                    @java.lang.Override()
                    public boolean equals(@org.jetbrains.annotations.Nullable()
                    java.lang.Object other) {
                        return false;
                    }
                    
                    @java.lang.Override()
                    public int hashCode() {
                        return 0;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    @java.lang.Override()
                    public java.lang.String toString() {
                        return null;
                    }
                    
                    public Notifications(@org.jetbrains.annotations.Nullable()
                    java.util.List<java.lang.String> types) {
                        super();
                    }
                    
                    @org.jetbrains.annotations.Nullable()
                    public final java.util.List<java.lang.String> component1() {
                        return null;
                    }
                    
                    @org.jetbrains.annotations.Nullable()
                    public final java.util.List<java.lang.String> getTypes() {
                        return null;
                    }
                }
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionState;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "accounts", "", "", "(Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class SessionState extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> accounts = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionState copy(@org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SessionState(@org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getAccounts() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SettledPairing;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "metaData", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;)V", "getMetaData", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "getTopic", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class SettledPairing extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.Nullable()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metaData = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SettledPairing copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.Nullable()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metaData) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SettledPairing(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.Nullable()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metaData) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData component2() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData getMetaData() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$RejectedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "reason", "(Ljava/lang/String;Ljava/lang/String;)V", "getReason", "()Ljava/lang/String;", "getTopic", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class RejectedSession extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String reason = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.RejectedSession copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.lang.String reason) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public RejectedSession(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.lang.String reason) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getReason() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00030\tH\u00c6\u0003J9\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0003H\u00d6\u0001R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006\u001f"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$ApprovedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "metaData", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "permissions", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "accounts", "", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "getMetaData", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "getPermissions", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "getTopic", "()Ljava/lang/String;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class ApprovedSession extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.Nullable()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metaData = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> accounts = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.ApprovedSession copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.Nullable()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metaData, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public ApprovedSession(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.Nullable()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metaData, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData component2() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData getMetaData() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions getPermissions() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getAccounts() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$DeletedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "reason", "(Ljava/lang/String;Ljava/lang/String;)V", "getReason", "()Ljava/lang/String;", "getTopic", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class DeletedSession extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String reason = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.DeletedSession copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.lang.String reason) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public DeletedSession(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.lang.String reason) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getReason() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$UpgradedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "permissions", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;)V", "getPermissions", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "getTopic", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class UpgradedSession extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.UpgradedSession copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public UpgradedSession(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions getPermissions() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J)\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001b"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "blockchain", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Blockchain;", "jsonRpc", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Jsonrpc;", "notification", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notifications;", "(Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Blockchain;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Jsonrpc;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notifications;)V", "getBlockchain", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Blockchain;", "getJsonRpc", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Jsonrpc;", "getNotification", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notifications;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
        public static final class SessionPermissions extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Blockchain blockchain = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Jsonrpc jsonRpc = null;
            @org.jetbrains.annotations.Nullable()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notifications notification = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions copy(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.Blockchain blockchain, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.Jsonrpc jsonRpc, @org.jetbrains.annotations.Nullable()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notifications notification) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SessionPermissions(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.Blockchain blockchain, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.Jsonrpc jsonRpc, @org.jetbrains.annotations.Nullable()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notifications notification) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Blockchain component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Blockchain getBlockchain() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Jsonrpc component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Jsonrpc getJsonRpc() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notifications component3() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notifications getNotification() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Blockchain;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "chains", "", "", "(Ljava/util/List;)V", "getChains", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Blockchain extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> chains = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Blockchain copy(@org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> chains) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Blockchain(@org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> chains) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getChains() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Jsonrpc;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "methods", "", "", "(Ljava/util/List;)V", "getMethods", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Jsonrpc extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> methods = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Jsonrpc copy(@org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> methods) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Jsonrpc(@org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> methods) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getMethods() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notifications;", "", "types", "", "", "(Ljava/util/List;)V", "getTypes", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Notifications {
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> types = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notifications copy(@org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> types) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Notifications(@org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> types) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getTypes() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J#\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$UpdatedSession;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "accounts", "", "(Ljava/lang/String;Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "getTopic", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class UpdatedSession extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> accounts = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.UpdatedSession copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public UpdatedSession(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getAccounts() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b\u00a8\u0006\u0016"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionNotification;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "type", "data", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getData", "()Ljava/lang/String;", "getTopic", "getType", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class SessionNotification extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String type = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String data = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionNotification copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.lang.String type, @org.jetbrains.annotations.NotNull()
            java.lang.String data) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SessionNotification(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.lang.String type, @org.jetbrains.annotations.NotNull()
            java.lang.String data) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getType() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getData() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notification;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "type", "", "data", "(Ljava/lang/String;Ljava/lang/String;)V", "getData", "()Ljava/lang/String;", "getType", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Notification extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String type = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String data = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notification copy(@org.jetbrains.annotations.NotNull()
            java.lang.String type, @org.jetbrains.annotations.NotNull()
            java.lang.String data) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Notification(@org.jetbrains.annotations.NotNull()
            java.lang.String type, @org.jetbrains.annotations.NotNull()
            java.lang.String data) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getType() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getData() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u000b\f\rB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u0082\u0001\u0002\u000e\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "()V", "id", "", "getId", "()J", "jsonrpc", "", "getJsonrpc", "()Ljava/lang/String;", "Error", "JsonRpcError", "JsonRpcResult", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse$JsonRpcResult;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse$JsonRpcError;", "walletconnectv2_debug"})
        public static abstract class JsonRpcResponse extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String jsonrpc = "2.0";
            
            private JsonRpcResponse() {
                super();
            }
            
            public abstract long getId();
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getJsonrpc() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0005H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse$JsonRpcResult;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse;", "id", "", "result", "", "(JLjava/lang/String;)V", "getId", "()J", "getResult", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
            public static final class JsonRpcResult extends com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse {
                private final long id = 0L;
                @org.jetbrains.annotations.NotNull()
                private final java.lang.String result = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse.JsonRpcResult copy(long id, @org.jetbrains.annotations.NotNull()
                java.lang.String result) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public JsonRpcResult(long id, @org.jetbrains.annotations.NotNull()
                java.lang.String result) {
                    super();
                }
                
                public final long component1() {
                    return 0L;
                }
                
                @java.lang.Override()
                public long getId() {
                    return 0L;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String getResult() {
                    return null;
                }
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0016"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse$JsonRpcError;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse;", "id", "", "error", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse$Error;", "(JLcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse$Error;)V", "getError", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse$Error;", "getId", "()J", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "walletconnectv2_debug"})
            public static final class JsonRpcError extends com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse {
                private final long id = 0L;
                @org.jetbrains.annotations.NotNull()
                private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse.Error error = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse.JsonRpcError copy(long id, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse.Error error) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public JsonRpcError(long id, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse.Error error) {
                    super();
                }
                
                public final long component1() {
                    return 0L;
                }
                
                @java.lang.Override()
                public long getId() {
                    return 0L;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse.Error component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse.Error getError() {
                    return null;
                }
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse$Error;", "", "code", "", "message", "", "(ILjava/lang/String;)V", "getCode", "()I", "getMessage", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "walletconnectv2_debug"})
            public static final class Error {
                private final int code = 0;
                @org.jetbrains.annotations.NotNull()
                private final java.lang.String message = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse.Error copy(int code, @org.jetbrains.annotations.NotNull()
                java.lang.String message) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public Error(int code, @org.jetbrains.annotations.NotNull()
                java.lang.String message) {
                    super();
                }
                
                public final int component1() {
                    return 0;
                }
                
                public final int getCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String getMessage() {
                    return null;
                }
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\r\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003J7\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n\u00a8\u0006\u001b"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "name", "", "description", "url", "icons", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getDescription", "()Ljava/lang/String;", "getIcons", "()Ljava/util/List;", "getName", "getUrl", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class AppMetaData extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String name = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String description = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String url = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> icons = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData copy(@org.jetbrains.annotations.NotNull()
            java.lang.String name, @org.jetbrains.annotations.NotNull()
            java.lang.String description, @org.jetbrains.annotations.NotNull()
            java.lang.String url, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> icons) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public AppMetaData(@org.jetbrains.annotations.NotNull()
            java.lang.String name, @org.jetbrains.annotations.NotNull()
            java.lang.String description, @org.jetbrains.annotations.NotNull()
            java.lang.String url, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> icons) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getName() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getDescription() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getUrl() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component4() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getIcons() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0019B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J3\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r\u00a8\u0006\u001a"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcHistory;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model;", "topic", "", "listOfRequests", "", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcHistory$HistoryEntry;", "listOfResponses", "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V", "getListOfRequests", "()Ljava/util/List;", "getListOfResponses", "getTopic", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "HistoryEntry", "walletconnectv2_debug"})
        public static final class JsonRpcHistory extends com.walletconnect.walletconnectv2.client.WalletConnect.Model {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> listOfRequests = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> listOfResponses = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> listOfRequests, @org.jetbrains.annotations.NotNull()
            java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> listOfResponses) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public JsonRpcHistory(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> listOfRequests, @org.jetbrains.annotations.NotNull()
            java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> listOfResponses) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> getListOfRequests() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry> getListOfResponses() {
                return null;
            }
            
            @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u000bH\u00c6\u0003JI\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010!\u001a\u00020\"H\u00d6\u0001J\t\u0010#\u001a\u00020\u0005H\u00d6\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u000e\u00a8\u0006$"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcHistory$HistoryEntry;", "", "requestId", "", "topic", "", "method", "body", "jsonRpcStatus", "Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "controllerType", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;)V", "getBody", "()Ljava/lang/String;", "getControllerType", "()Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "getJsonRpcStatus", "()Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "getMethod", "getRequestId", "()J", "getTopic", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "walletconnectv2_debug"})
            public static final class HistoryEntry {
                private final long requestId = 0L;
                @org.jetbrains.annotations.NotNull()
                private final java.lang.String topic = null;
                @org.jetbrains.annotations.Nullable()
                private final java.lang.String method = null;
                @org.jetbrains.annotations.Nullable()
                private final java.lang.String body = null;
                @org.jetbrains.annotations.NotNull()
                private final com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus jsonRpcStatus = null;
                @org.jetbrains.annotations.NotNull()
                private final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType = null;
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcHistory.HistoryEntry copy(long requestId, @org.jetbrains.annotations.NotNull()
                java.lang.String topic, @org.jetbrains.annotations.Nullable()
                java.lang.String method, @org.jetbrains.annotations.Nullable()
                java.lang.String body, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus jsonRpcStatus, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType) {
                    return null;
                }
                
                @java.lang.Override()
                public boolean equals(@org.jetbrains.annotations.Nullable()
                java.lang.Object other) {
                    return false;
                }
                
                @java.lang.Override()
                public int hashCode() {
                    return 0;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public java.lang.String toString() {
                    return null;
                }
                
                public HistoryEntry(long requestId, @org.jetbrains.annotations.NotNull()
                java.lang.String topic, @org.jetbrains.annotations.Nullable()
                java.lang.String method, @org.jetbrains.annotations.Nullable()
                java.lang.String body, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus jsonRpcStatus, @org.jetbrains.annotations.NotNull()
                com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controllerType) {
                    super();
                }
                
                public final long component1() {
                    return 0L;
                }
                
                public final long getRequestId() {
                    return 0L;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String component2() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final java.lang.String getTopic() {
                    return null;
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String component3() {
                    return null;
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getMethod() {
                    return null;
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String component4() {
                    return null;
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getBody() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus component5() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus getJsonRpcStatus() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType component6() {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType getControllerType() {
                    return null;
                }
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\f\u0003\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000eB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\f\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u00a8\u0006\u001b"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "", "()V", "Approve", "Connect", "Disconnect", "Init", "Notify", "Pair", "Ping", "Reject", "Request", "Response", "Update", "Upgrade", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Init;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Connect;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Pair;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Approve;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Reject;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Disconnect;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Response;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Request;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Update;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Upgrade;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Ping;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Notify;", "walletconnectv2_debug"})
    public static abstract class Params {
        
        private Params() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B7\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fB\'\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000eB\u001f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u000bH\u00c6\u0003J\'\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00052\b\b\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u00052\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\"H\u00d6\u0001J\t\u0010#\u001a\u00020\u0007H\u00d6\u0001J\f\u0010$\u001a\u00020\u0005*\u00020\u0007H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0012R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0007X\u0080.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019\u00a8\u0006%"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Init;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "application", "Landroid/app/Application;", "useTls", "", "hostName", "", "projectId", "isController", "metadata", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "(Landroid/app/Application;ZLjava/lang/String;Ljava/lang/String;ZLcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;)V", "relayServerUrl", "(Landroid/app/Application;Ljava/lang/String;ZLcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;)V", "(Landroid/app/Application;ZLcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;)V", "getApplication", "()Landroid/app/Application;", "()Z", "getMetadata", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$AppMetaData;", "serverUrl", "getServerUrl$walletconnectv2_debug", "()Ljava/lang/String;", "setServerUrl$walletconnectv2_debug", "(Ljava/lang/String;)V", "component1", "component2", "component3", "copy", "equals", "other", "", "hashCode", "", "toString", "isValidRelayServerUrl", "walletconnectv2_debug"})
        public static final class Init extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final android.app.Application application = null;
            private final boolean isController = false;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metadata = null;
            public java.lang.String serverUrl;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Init copy(@org.jetbrains.annotations.NotNull()
            android.app.Application application, boolean isController, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metadata) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Init(@org.jetbrains.annotations.NotNull()
            android.app.Application application, boolean isController, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metadata) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.app.Application component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.app.Application getApplication() {
                return null;
            }
            
            public final boolean component2() {
                return false;
            }
            
            public final boolean isController() {
                return false;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData getMetadata() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getServerUrl$walletconnectv2_debug() {
                return null;
            }
            
            public final void setServerUrl$walletconnectv2_debug(@org.jetbrains.annotations.NotNull()
            java.lang.String p0) {
            }
            
            public Init(@org.jetbrains.annotations.NotNull()
            android.app.Application application, boolean useTls, @org.jetbrains.annotations.NotNull()
            java.lang.String hostName, @org.jetbrains.annotations.NotNull()
            java.lang.String projectId, boolean isController, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metadata) {
                super();
            }
            
            public Init(@org.jetbrains.annotations.NotNull()
            android.app.Application application, @org.jetbrains.annotations.NotNull()
            java.lang.String relayServerUrl, boolean isController, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.AppMetaData metadata) {
                super();
            }
            
            private final boolean isValidRelayServerUrl(java.lang.String $this$isValidRelayServerUrl) {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0005H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Connect;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "permissions", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "pairingTopic", "", "(Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;Ljava/lang/String;)V", "getPairingTopic", "()Ljava/lang/String;", "getPermissions", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Connect extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions = null;
            @org.jetbrains.annotations.Nullable()
            private final java.lang.String pairingTopic = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Connect copy(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions, @org.jetbrains.annotations.Nullable()
            java.lang.String pairingTopic) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Connect(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions, @org.jetbrains.annotations.Nullable()
            java.lang.String pairingTopic) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions getPermissions() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getPairingTopic() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Pair;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "uri", "", "(Ljava/lang/String;)V", "getUri", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Pair extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String uri = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Pair copy(@org.jetbrains.annotations.NotNull()
            java.lang.String uri) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Pair(@org.jetbrains.annotations.NotNull()
            java.lang.String uri) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getUri() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0006H\u00d6\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0016"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Approve;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "proposal", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionProposal;", "accounts", "", "", "(Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionProposal;Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "getProposal", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionProposal;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Approve extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionProposal proposal = null;
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<java.lang.String> accounts = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Approve copy(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionProposal proposal, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Approve(@org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionProposal proposal, @org.jetbrains.annotations.NotNull()
            java.util.List<java.lang.String> accounts) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionProposal component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionProposal getProposal() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<java.lang.String> getAccounts() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Reject;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "rejectionReason", "", "proposalTopic", "(Ljava/lang/String;Ljava/lang/String;)V", "getProposalTopic", "()Ljava/lang/String;", "getRejectionReason", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Reject extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String rejectionReason = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String proposalTopic = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Reject copy(@org.jetbrains.annotations.NotNull()
            java.lang.String rejectionReason, @org.jetbrains.annotations.NotNull()
            java.lang.String proposalTopic) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Reject(@org.jetbrains.annotations.NotNull()
            java.lang.String rejectionReason, @org.jetbrains.annotations.NotNull()
            java.lang.String proposalTopic) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getRejectionReason() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getProposalTopic() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u00a8\u0006\u0017"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Disconnect;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "sessionTopic", "", "reason", "reasonCode", "", "(Ljava/lang/String;Ljava/lang/String;I)V", "getReason", "()Ljava/lang/String;", "getReasonCode", "()I", "getSessionTopic", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "toString", "walletconnectv2_debug"})
        public static final class Disconnect extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String sessionTopic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String reason = null;
            private final int reasonCode = 0;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Disconnect copy(@org.jetbrains.annotations.NotNull()
            java.lang.String sessionTopic, @org.jetbrains.annotations.NotNull()
            java.lang.String reason, int reasonCode) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Disconnect(@org.jetbrains.annotations.NotNull()
            java.lang.String sessionTopic, @org.jetbrains.annotations.NotNull()
            java.lang.String reason, int reasonCode) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getSessionTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getReason() {
                return null;
            }
            
            public final int component3() {
                return 0;
            }
            
            public final int getReasonCode() {
                return 0;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Response;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "sessionTopic", "", "jsonRpcResponse", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse;", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse;)V", "getJsonRpcResponse", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$JsonRpcResponse;", "getSessionTopic", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Response extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String sessionTopic = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse jsonRpcResponse = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Response copy(@org.jetbrains.annotations.NotNull()
            java.lang.String sessionTopic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse jsonRpcResponse) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Response(@org.jetbrains.annotations.NotNull()
            java.lang.String sessionTopic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse jsonRpcResponse) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getSessionTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.JsonRpcResponse getJsonRpcResponse() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J3\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u00a8\u0006\u0019"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Request;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "sessionTopic", "", "method", "params", "chainId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getChainId", "()Ljava/lang/String;", "getMethod", "getParams", "getSessionTopic", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Request extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String sessionTopic = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String method = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String params = null;
            @org.jetbrains.annotations.Nullable()
            private final java.lang.String chainId = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Request copy(@org.jetbrains.annotations.NotNull()
            java.lang.String sessionTopic, @org.jetbrains.annotations.NotNull()
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            java.lang.String params, @org.jetbrains.annotations.Nullable()
            java.lang.String chainId) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Request(@org.jetbrains.annotations.NotNull()
            java.lang.String sessionTopic, @org.jetbrains.annotations.NotNull()
            java.lang.String method, @org.jetbrains.annotations.NotNull()
            java.lang.String params, @org.jetbrains.annotations.Nullable()
            java.lang.String chainId) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getSessionTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getMethod() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component3() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getParams() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String component4() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getChainId() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Update;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "sessionTopic", "", "sessionState", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionState;", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionState;)V", "getSessionState", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionState;", "getSessionTopic", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Update extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String sessionTopic = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionState sessionState = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Update copy(@org.jetbrains.annotations.NotNull()
            java.lang.String sessionTopic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionState sessionState) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Update(@org.jetbrains.annotations.NotNull()
            java.lang.String sessionTopic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionState sessionState) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getSessionTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionState component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionState getSessionState() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Upgrade;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "topic", "", "permissions", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;)V", "getPermissions", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$SessionPermissions;", "getTopic", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Upgrade extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Upgrade copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Upgrade(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions permissions) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.SessionPermissions getPermissions() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Ping;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "topic", "", "(Ljava/lang/String;)V", "getTopic", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Ping extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Ping copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Ping(@org.jetbrains.annotations.NotNull()
            java.lang.String topic) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params$Notify;", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Params;", "topic", "", "notification", "Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notification;", "(Ljava/lang/String;Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notification;)V", "getNotification", "()Lcom/walletconnect/walletconnectv2/client/WalletConnect$Model$Notification;", "getTopic", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "walletconnectv2_debug"})
        public static final class Notify extends com.walletconnect.walletconnectv2.client.WalletConnect.Params {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String topic = null;
            @org.jetbrains.annotations.NotNull()
            private final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notification notification = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Params.Notify copy(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notification notification) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Notify(@org.jetbrains.annotations.NotNull()
            java.lang.String topic, @org.jetbrains.annotations.NotNull()
            com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notification notification) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getTopic() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notification component2() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.walletconnect.walletconnectv2.client.WalletConnect.Model.Notification getNotification() {
                return null;
            }
        }
    }
}