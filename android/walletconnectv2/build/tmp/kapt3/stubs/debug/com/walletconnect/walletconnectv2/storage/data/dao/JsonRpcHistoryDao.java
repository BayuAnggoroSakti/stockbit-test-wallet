package com.walletconnect.walletconnectv2.storage.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\'BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\nH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\fH\u00c6\u0003JS\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\fH\u00c6\u0001J\u0013\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010$\u001a\u00020%H\u00d6\u0001J\b\u0010&\u001a\u00020\u0006H\u0016R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0013R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u000f\u00a8\u0006("}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryDao;", "", "id", "", "request_id", "topic", "", "method", "body", "status", "Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "controller_type", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;)V", "getBody", "()Ljava/lang/String;", "getController_type", "()Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "getId", "()J", "getMethod", "getRequest_id", "getStatus", "()Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "getTopic", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "", "other", "hashCode", "", "toString", "Adapter", "walletconnectv2_debug"})
public final class JsonRpcHistoryDao {
    private final long id = 0L;
    private final long request_id = 0L;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String topic = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String method = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String body = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus status = null;
    @org.jetbrains.annotations.NotNull()
    private final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.data.dao.JsonRpcHistoryDao copy(long id, long request_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.lang.String method, @org.jetbrains.annotations.Nullable()
    java.lang.String body, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    public JsonRpcHistoryDao(long id, long request_id, @org.jetbrains.annotations.NotNull()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.lang.String method, @org.jetbrains.annotations.Nullable()
    java.lang.String body, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus status, @org.jetbrains.annotations.NotNull()
    com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType controller_type) {
        super();
    }
    
    public final long component1() {
        return 0L;
    }
    
    public final long getId() {
        return 0L;
    }
    
    public final long component2() {
        return 0L;
    }
    
    public final long getRequest_id() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTopic() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMethod() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBody() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus getStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType getController_type() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B-\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0002\u0010\bR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\n\u00a8\u0006\f"}, d2 = {"Lcom/walletconnect/walletconnectv2/storage/data/dao/JsonRpcHistoryDao$Adapter;", "", "statusAdapter", "Lcom/squareup/sqldelight/ColumnAdapter;", "Lcom/walletconnect/walletconnectv2/storage/history/model/JsonRpcStatus;", "", "controller_typeAdapter", "Lcom/walletconnect/walletconnectv2/core/model/type/enums/ControllerType;", "(Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;)V", "getController_typeAdapter", "()Lcom/squareup/sqldelight/ColumnAdapter;", "getStatusAdapter", "walletconnectv2_debug"})
    public static final class Adapter {
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus, java.lang.String> statusAdapter = null;
        @org.jetbrains.annotations.NotNull()
        private final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> controller_typeAdapter = null;
        
        public Adapter(@org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus, java.lang.String> statusAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> controller_typeAdapter) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.storage.history.model.JsonRpcStatus, java.lang.String> getStatusAdapter() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.sqldelight.ColumnAdapter<com.walletconnect.walletconnectv2.core.model.type.enums.ControllerType, java.lang.String> getController_typeAdapter() {
            return null;
        }
    }
}