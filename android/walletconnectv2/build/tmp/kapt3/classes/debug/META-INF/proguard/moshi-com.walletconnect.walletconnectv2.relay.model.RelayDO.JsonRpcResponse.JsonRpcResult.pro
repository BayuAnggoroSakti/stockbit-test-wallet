-if class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcResult
-keepnames class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcResult
-if class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcResult
-keep class com.walletconnect.walletconnectv2.relay.model.RelayDO_JsonRpcResponse_JsonRpcResultJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcResult
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcResult
-keepclassmembers class com.walletconnect.walletconnectv2.relay.model.RelayDO$JsonRpcResponse$JsonRpcResult {
    public synthetic <init>(long,java.lang.String,java.lang.Object,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
