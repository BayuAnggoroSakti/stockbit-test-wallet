-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.params.SessionRequestVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
