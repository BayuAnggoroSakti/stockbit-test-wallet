-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO$ApproveParams
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO$ApproveParams
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO$ApproveParams
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO_ApproveParamsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
    private com.squareup.moshi.JsonAdapter expiryVOAtQualifierAdapter;
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO$ApproveParams
-keep @interface com.walletconnect.walletconnectv2.core.adapters.ExpiryAdapter$Qualifier
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO$ApproveParams
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO$ApproveParams
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO$ApproveParams {
    public synthetic <init>(com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.RelayProtocolOptionsVO,com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO,com.walletconnect.walletconnectv2.core.model.vo.ExpiryVO,com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
