-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ApprovalParams
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ApprovalParams
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ApprovalParams
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO_ApprovalParamsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
    private com.squareup.moshi.JsonAdapter expiryVOAtQualifierAdapter;
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ApprovalParams
-keep @interface com.walletconnect.walletconnectv2.core.adapters.ExpiryAdapter$Qualifier
