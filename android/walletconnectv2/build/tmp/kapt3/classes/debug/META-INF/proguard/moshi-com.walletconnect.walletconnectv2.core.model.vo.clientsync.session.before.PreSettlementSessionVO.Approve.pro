-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Approve
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Approve
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Approve
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO_ApproveJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Approve
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Approve
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Approve {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ApprovalParams,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
