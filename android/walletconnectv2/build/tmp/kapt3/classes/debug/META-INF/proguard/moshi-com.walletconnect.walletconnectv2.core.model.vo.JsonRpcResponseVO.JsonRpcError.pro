-if class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcError
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcError
-if class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcError
-keep class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO_JsonRpcErrorJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcError
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcError
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcError {
    public synthetic <init>(long,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$Error,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
