-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params
-keepnames class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params
-keep class com.walletconnect.walletconnectv2.network.model.RelayDTO_Publish_Request_ParamsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
    private com.squareup.moshi.JsonAdapter topicVOAtQualifierAdapter;
    private com.squareup.moshi.JsonAdapter ttlVOAtQualifierAdapter;
}
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params
-keep @interface com.walletconnect.walletconnectv2.core.adapters.TopicAdapter$Qualifier
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params
-keep @interface com.walletconnect.walletconnectv2.core.adapters.TtlAdapter$Qualifier
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params
-keepclassmembers class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params {
    public synthetic <init>(com.walletconnect.walletconnectv2.core.model.vo.TopicVO,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.TtlVO,java.lang.Boolean,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
