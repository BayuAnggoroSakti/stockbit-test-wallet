-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.PostSettlementPairingVO$SessionDelete
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.PostSettlementPairingVO$SessionDelete
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.PostSettlementPairingVO$SessionDelete
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.PostSettlementPairingVO_SessionDeleteJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.PostSettlementPairingVO$SessionDelete
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.PostSettlementPairingVO$SessionDelete
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.after.PostSettlementPairingVO$SessionDelete {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.PairingParamsVO$DeleteParams,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
