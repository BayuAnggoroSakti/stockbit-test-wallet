-if class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcResult
-keepnames class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcResult
-if class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcResult
-keep class com.walletconnect.walletconnectv2.engine.model.EngineDO_JsonRpcResponse_JsonRpcResultJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcResult
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcResult
-keepclassmembers class com.walletconnect.walletconnectv2.engine.model.EngineDO$JsonRpcResponse$JsonRpcResult {
    public synthetic <init>(long,java.lang.String,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
