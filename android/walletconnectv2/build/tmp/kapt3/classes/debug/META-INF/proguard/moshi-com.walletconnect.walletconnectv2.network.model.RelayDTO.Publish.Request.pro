-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request
-keepnames class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request
-keep class com.walletconnect.walletconnectv2.network.model.RelayDTO_Publish_RequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request
-keepclassmembers class com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.network.model.RelayDTO$Publish$Request$Params,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
