-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPing
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPing
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPing
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO_SessionPingJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPing
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPing
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPing {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$PingParams,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
