-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Proposal
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Proposal
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Proposal
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO_ProposalJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Proposal
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Proposal
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.PreSettlementSessionVO$Proposal {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ProposalParams,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
