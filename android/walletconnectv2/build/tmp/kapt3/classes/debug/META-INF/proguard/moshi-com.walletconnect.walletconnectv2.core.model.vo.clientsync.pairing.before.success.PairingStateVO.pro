-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingStateVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
