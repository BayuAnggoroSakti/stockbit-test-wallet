-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionNotification
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionNotification
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionNotification
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO_SessionNotificationJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionNotification
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionNotification
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionNotification {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$NotificationParams,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
