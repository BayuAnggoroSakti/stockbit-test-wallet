-if class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcResult
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcResult
-if class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcResult
-keep class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO_JsonRpcResultJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcResult
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcResult
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.JsonRpcResponseVO$JsonRpcResult {
    public synthetic <init>(long,java.lang.String,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
