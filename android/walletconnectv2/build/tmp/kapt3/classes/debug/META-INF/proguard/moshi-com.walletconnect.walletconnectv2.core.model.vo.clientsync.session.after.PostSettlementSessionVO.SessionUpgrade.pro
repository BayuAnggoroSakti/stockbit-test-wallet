-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionUpgrade
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionUpgrade
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionUpgrade
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO_SessionUpgradeJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionUpgrade
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionUpgrade
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionUpgrade {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$UpgradeParams,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
