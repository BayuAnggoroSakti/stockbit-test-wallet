-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.success.SessionParticipantVO {
    public synthetic <init>(java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.AppMetaDataVO,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
