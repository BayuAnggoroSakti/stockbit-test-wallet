-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPayload
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPayload
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPayload
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO_SessionPayloadJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPayload
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPayload
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.after.PostSettlementSessionVO$SessionPayload {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$SessionPayloadParams,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
