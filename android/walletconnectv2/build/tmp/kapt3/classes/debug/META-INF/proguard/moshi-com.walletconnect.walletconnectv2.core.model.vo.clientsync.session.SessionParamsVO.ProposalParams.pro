-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ProposalParams
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ProposalParams
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ProposalParams
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO_ProposalParamsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
    private com.squareup.moshi.JsonAdapter topicVOAtQualifierAdapter;
    private com.squareup.moshi.JsonAdapter ttlVOAtQualifierAdapter;
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ProposalParams
-keep @interface com.walletconnect.walletconnectv2.core.adapters.TopicAdapter$Qualifier
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.SessionParamsVO$ProposalParams
-keep @interface com.walletconnect.walletconnectv2.core.adapters.TtlAdapter$Qualifier
