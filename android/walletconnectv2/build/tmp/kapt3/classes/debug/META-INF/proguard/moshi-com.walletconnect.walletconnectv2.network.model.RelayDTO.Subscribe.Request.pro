-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request
-keepnames class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request
-keep class com.walletconnect.walletconnectv2.network.model.RelayDTO_Subscribe_RequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request
-keepclassmembers class com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request {
    public synthetic <init>(long,java.lang.String,java.lang.String,com.walletconnect.walletconnectv2.network.model.RelayDTO$Subscribe$Request$Params,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
