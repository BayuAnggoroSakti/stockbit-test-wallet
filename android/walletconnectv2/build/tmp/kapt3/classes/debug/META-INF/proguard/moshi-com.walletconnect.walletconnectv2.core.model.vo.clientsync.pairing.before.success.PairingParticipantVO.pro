-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.pairing.before.success.PairingParticipantVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
