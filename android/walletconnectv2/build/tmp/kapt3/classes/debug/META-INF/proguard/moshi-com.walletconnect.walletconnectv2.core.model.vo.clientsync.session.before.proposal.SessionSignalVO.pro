-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO
-keepnames class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO
-keep class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVOJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO
-keepclassmembers class com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO {
    public synthetic <init>(java.lang.String,com.walletconnect.walletconnectv2.core.model.vo.clientsync.session.before.proposal.SessionSignalVO$Params,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
