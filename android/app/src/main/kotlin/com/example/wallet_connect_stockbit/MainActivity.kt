package com.example.wallet_connect_stockbit

import android.util.Log
import androidx.annotation.NonNull
import com.walletconnect.walletconnectv2.client.WalletConnect
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel


class MainActivity: FlutterActivity() {
    private val CHANNEL = "com.stockbit.wallet/wallet"
    private val viewModel = WalletModel()
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler {
            // Note: this method is invoked on the main thread.
                call, result ->
            if (call.method == "approve") {
                val response: String? = approve()
                result.success(response)
            }else if (call.method == "reject") {
                val response: String? = reject()
                result.success(response)
            }else if(call.method == "getBarcode"){
                val code = call.argument<String>("code")
                if (code != null) {
                    val response: String? = pair(code)
                    result.success(response)
                    }
            }else if(call.method == "disconnect"){
                val topic = call.argument<String>("topic")
                if (topic != null) {
                    val response: String? = disconnect(topic)
                    result.success(response)
                }
            }else if(call.method == "listSession"){
                val response: List<WalletConnect.Model.SettledSession> = listSession()
                if (response.size > 0) {
                   
                    result.success(hashMapOf(
                        "topic" to response.get(0)!!.topic!!,
                        "name" to response.get(0).peerAppMetaData!!.name,
                        "icons" to response.get(0).peerAppMetaData!!.icons.get(0),
                        "accounts" to response.get(0).accounts!!,
                    ))
                }

            }else {
                result.notImplemented()
            }
        }
    }

    private fun pair(code: String): String? {
        viewModel.pair(code)
        Log.d("pairingBro", "pair: "+viewModel.pair(code).toString())
        return  ("ShowSessionProposalDialog")
    }

    private fun disconnect(topic: String): String? {
        viewModel.disconnect(topic)
        return  ("success")
    }

    private fun listSession(): List<WalletConnect.Model.SettledSession> {
        var result: List<WalletConnect.Model.SettledSession> = listOf()
        viewModel.eventFlow.observe(this) { event ->
            when (event) {
                is InitSessionsList -> {
                    Log.d("listsession", "InitSessionsList: "+event.sessions.toString())
                    result = (event.sessions)
                }

                is ShowSessionRequestDialog -> {
                    Log.d("listsession", "ShowSessionRequestDialog: ")
                }
                is UpdateActiveSessions -> {
                    Log.d("listsession", "UpdateActiveSessions: "+event.sessions.toString())
                        result = (event.sessions)
                }
                is RejectSession ->  Log.d("listsession", "RejectSession: ")
                is PingSuccess ->  Log.d("listsession", "PingSuccess: ")
            }
        }
        return result
    }

    private fun approve(): String? {
        var result = ""
        viewModel.eventFlow.observe(this) { event ->
            when (event) {
                is ShowSessionProposalDialog -> {
                    viewModel.approve()
                    result = "approve"
                }
            }
        }
        return result
    }

    private fun reject(): String? {
        var result = ""
        viewModel.eventFlow.observe(this) { event ->
            when (event) {
                is ShowSessionProposalDialog -> {
                    viewModel.reject()
                    result = "reject"
                }
            }
        }
        return result
    }


}