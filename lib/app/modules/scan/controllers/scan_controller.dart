import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:get/get.dart';
import 'package:wallet_connect_stockbit/app/controllers/page_index_controller.dart';
import 'package:wallet_connect_stockbit/app/routes/app_pages.dart';
import 'package:wallet_connect_stockbit/app/widgets/dialog/custom_alert_dialog.dart';

class ScanController extends GetxController {
  static const platform = MethodChannel('com.stockbit.wallet/wallet');
  final pageIndexController = Get.find<PageIndexController>();
  String session = '';

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> scanQR() async {
    String session;
    try {
      session = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "cancel", true, ScanMode.QR);
      try {
        final String result =
            await platform.invokeMethod('getBarcode', {'code': session});
        if (result == "ShowSessionProposalDialog") {
          CustomAlertDialog.showAlert(
              confirm: "Approve",
              cancel: "Reject",
              title: 'Proposal confirmation',
              message:
                  'you need to confirm that you are an approve this proposal',
              onCancel: () {
                reject();
              },
              onConfirm: () async {
                approve();
              });
        }
      } on PlatformException catch (e) {
        Get.snackbar(
          "Sorry!",
          e.message.toString(),
          icon: Icon(Icons.dangerous, color: Colors.white),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.red,
          borderRadius: 20,
          margin: EdgeInsets.all(15),
          colorText: Colors.white,
          duration: Duration(seconds: 4),
          isDismissible: true,
          forwardAnimationCurve: Curves.easeOutBack,
        );
      }
    } on PlatformException {}
  }

  Future<void> approve() async {
    String session;
    try {
      final String result = await platform.invokeMethod('approve');
      if (result == "approve") {
        Get.offAllNamed(Routes.HOME);
        pageIndexController.changePage(0);
      }
    } on PlatformException catch (e) {
      Get.snackbar(
        "Sorry!",
        e.message.toString(),
        icon: Icon(Icons.dangerous, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }
  }

  Future<void> reject() async {
    try {
      final String result = await platform.invokeMethod('reject');
      Get.back();
    } on PlatformException catch (e) {
      Get.snackbar(
        "Sorry!",
        e.message.toString(),
        icon: Icon(Icons.dangerous, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }
  }
}
