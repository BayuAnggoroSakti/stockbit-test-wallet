import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:wallet_connect_stockbit/app/controllers/page_index_controller.dart';
import 'package:wallet_connect_stockbit/app/routes/app_pages.dart';

class HomeController extends GetxController {
  static const platform = MethodChannel('com.stockbit.wallet/wallet');
  final pageIndexController = Get.find<PageIndexController>();
  final session = [].obs;

  @override
  void onInit() {
    super.onInit();
    listSession();
  }

  Future<void> listSession() async {
    try {
      final result = await platform.invokeMethod('listSession');
      Map<String, dynamic> myObject = {
        'topic': result['topic'],
        'name': result['name'],
        'icons': result['icons'],
        'accounts': result['accounts']
      };
      List<Map<String, dynamic>> send = [];

      send.add(myObject);
      session.value = send;
    } on PlatformException catch (e) {
      Get.snackbar(
        "Sorry!",
        e.message.toString(),
        icon: Icon(Icons.dangerous, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }
  }

  Future<void> disconnect(String topic) async {
    try {
      final String result =
          await platform.invokeMethod('disconnect', {'topic': topic});
      if (result == "success") {
        Get.offAllNamed(Routes.HOME);
        pageIndexController.changePage(0);
      }
    } on PlatformException catch (e) {
      Get.snackbar(
        "Sorry!",
        e.message.toString(),
        icon: Icon(Icons.dangerous, color: Colors.white),
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        borderRadius: 20,
        margin: EdgeInsets.all(15),
        colorText: Colors.white,
        duration: Duration(seconds: 4),
        isDismissible: true,
        forwardAnimationCurve: Curves.easeOutBack,
      );
    }
  }
}
