import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wallet_connect_stockbit/app/controllers/page_index_controller.dart';
import 'package:wallet_connect_stockbit/app/data/model/session.dart';

import 'package:wallet_connect_stockbit/app/routes/app_pages.dart';
import 'package:wallet_connect_stockbit/app/style/app_color.dart';
import 'package:wallet_connect_stockbit/app/widgets/custom_bottom_navigation_bar.dart';
import 'package:wallet_connect_stockbit/app/widgets/dialog/custom_alert_dialog.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final pageIndexController = Get.find<PageIndexController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CustomBottomNavigationBar(),
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          elevation: 0.0,
          title: new Center(
            child: new Text(
              'List Wallet',
              style: TextStyle(
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          actions: [
            PopupMenuButton<int>(
              itemBuilder: (ctx) => [
                PopupMenuItem(
                  child: Row(
                    children: [
                      Icon(Icons.refresh),
                      SizedBox(width: 8.0),
                      Text('Refresh'),
                    ],
                  ),
                  value: 0,
                ),
              ],
              onSelected: (value) {
                switch (value) {
                  case 0:
                    Get.offAllNamed(Routes.HOME);
                    pageIndexController.changePage(0);
                    break;
                }
              },
            ),
          ],
          leadingWidth: Get.width * 0.25,
        ),
        extendBody: true,
        body: Obx(() {
          final sessionState = controller.session.value;
          log(sessionState.toString());
          if (sessionState.length > 0) {
            return ListView(
              children: [
                GestureDetector(
                  onTap: () => CustomAlertDialog.showAlert(
                      confirm: "Disconnect",
                      cancel: "Cancel",
                      title: 'Disconnect Session',
                      message:
                          'you need to confirm that you are an disconnect session',
                      onCancel: () {
                        Get.back();
                      },
                      onConfirm: () async {
                        controller.disconnect(sessionState[0]["topic"]);
                        Get.back();
                      }),
                  child: Card(
                    child: ListTile(
                        title: Text(sessionState[0]["name"]),
                        leading: CircleAvatar(
                            backgroundImage:
                                NetworkImage(sessionState[0]["icons"])),
                        trailing: Icon(Icons.arrow_right_alt)),
                  ),
                ),
              ],
              padding: EdgeInsets.all(10),
            );
          } else {
            return Text("");
          }
        }));
  }
}
